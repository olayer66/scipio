# Scipio
Scipio es una aplicación basada en la creación de una aplicación para la corrección de practicas de programación por parte de los profesores y entre los propios alumnos (inter-pares), así como la generación de informes de dichas correcciones para el feedback.

## Requisitos

- Linux ubuntu 18.04 o superior
- Angular CLI en su version 8.0.1 o superior
- pm2 process manager version 3.5.0 o superior
- MySQL Community Server Ver 14.14 Distrib 5.7.27

No se puede asegurar el funcionamiento en versiones inferiores a las anteriormente listadas.
## Instalación
A continuación se describe el proceso de instalación de la aplicación.

### Descarga del proyecto
Para poder descargar el proyecto es necesario tener instalado Git.

    sudo apt install git
Para comprobar la instalación podemos ejecutar el siguiente comando.

    git --version

A continuación procedemos a descargar el proyecto
     
    git clone https://bitbucket.org/olayer66/scipio.git

Una ver descargado procederemos a la instalación de la aplicación.

### Instalación de la aplicación

Para la instalación nos vamos al directorio de la aplicación y ejecutamos

    npm install
    npm install --only="dev"

Una vez termine podremos a instalar los elementos externos a la aplicación necesarios (pm2 y MySQL server)


#### MySQL server
Para instalar el servidor de MySQL se recomienda seguir el tutorial siguiente.

    https://www.digitalocean.com/community/tutorials/como-instalar-mysql-en-ubuntu-18-04-es 
Una vez instalado podremos crear la base de datos y las tablas usaremos el script de gestión de la aplicación, que se explica en la sección de Gestión.
PAra gestionar la base de datos si disponemos de interfaz gráfica podemos usar mysql workbench, phpmyadmin o cualquier gestor que permita bases de datos mysql. 
Si no disponemos de interfaz gráfica podemos usar el comando.

    mysql -u root -p

Nos permitirá entran en el modo consola de mysql.


#### pm2

Para instalar el gestor de procesos lanzamos el siguiente comando en la consola
    
    npm install pm2@latest -g
    
#### angular

Para instalar angular en el sistema lanzamos el siguiente comando en la consola.
    
    npm install -g @angular/cli

#### nodemon

Para instalar el servidor backend de desarrollo lanzamos el siguiente comando.
    
    npm install nodemon -g

## Configuración
### Base de datos
Para configurar las base de datos de nuestra aplicación debemos de abrir el fichero de configuración "knex.js"  que esta situado en la carpeta "conf" de la raiz del 
proyecto. En este fichero encontraremos dos configuraciones iguales una para desarrollo (development) y otra para producción (production). Ambas contienen la misma 
estructura.
       
       {
            client: 'mysql',
            connection: {
                  host : '127.0.0.1',
                  user : 'root',
                  password : 'Tfg2019!',
                  database : 'scipioDB'
            },
            useNullAsDefault: true
        }

En esta estructura los campos que se han de modificar son.

- host: La dirección IP del servidor MySql.
- user: El nombre del usuario de la base de datos con el que vamos a acceder, este se ha configurado en el paso "Instalación de MySql server".
- password: La contraseña asociada al usuario anterior.

Se recomienda cambiar el usuario y la contraseña un uno propio, y que sean distintos para caso, en desarrollo se puede usar el usuario de root pero en el caso de 
producción se recomienda el uso de un usuario con permisos mas limitados (lectura y escritura) para evitar accesos no autorizados.

###Comando para el gestor
Esta parte es opcional, pero mejora la gestión a no tener que estar llamando al script de gestión del servidor de forma manual.

El primer paso es localizar el fichero '.bashrc' en el directorio raiz de nuestro usuario, para ello usamos el siguiente comando

    ls -la
    
 Nos mostrara los ficheros del directorio entre ellos el que necesitamos, Ahora lo abrimos para editar para ello lo mas sencillo desde la propia consola lanzamos
 
    vi .bashrc
Nos abrirá el editor de vi con el fichero, si no se sabe usar se puede abrir con cualquier otro editor tanto de linea de comandos como gráfico.

Ahora tenemos que insertar la siguiente linea al final del fichero.

    alias scipio='[path]/scipio/scripts/server.sh'
Siendo [path] la ruta absoluta al directorio donde este la aplicación. Guardamos y cerramos, para que surta efecto debemos cerrar y abrir la consola. Ya dispondríamos 
del comando scipio desde cualquier parte del sistema

En este fichero se puede insertar también la variable de entorno ENV mediante la linea

    export ENV="prod"

Esta linea nos permite que en caso de reinicio del servidor ya este configurado.

###Configuramos el reboot automatico con un servicio
- Nos vamos a la carpeta que contiene los servicios

        cd /lib/systemd/system

- creamos un nuevo servicio llamado vps.service

        vi scipio.service

- copiamos y pegamos el siguiente código

        [Unit]
        Description=SCIPIO server start
        After=network.target
        After=systemd-user-sessions.service
        After=network-online.target
    
        [Service]
        User=root
        Type=forking
        ExecStart=[path]/scipio/scripts/server.sh
        ExecStop=[path]/scipio/scripts/server.sh
        TimeoutSec=30
        Restart=on-failure
        RestartSec=30
        StartLimitInterval=350
        StartLimitBurst=10
        
        [Install]
        WantedBy=multi-user.target

    Siendo [path] la ruta absoluta al directorio donde este la aplicación.
- guardamos y cerramos la edición

- lanzamos la siguiente secuencia de comandos (si el vps ya esta arrancado saldrá un fallo al realizar el start
    
        sudo systemctl start scipio.service
        sudo systemctl stop scipio.service
        sudo systemctl enable scipio.service
    
- Comprobamos que el servicio se ha creado bien

        systemctl list-units --type service --all
    
    Saldrán todos los servicios buscamos el nuestro y veremos algo similar esto:
    
        scipio.service                           loaded    active   running SCIPIO server start

- Realizamos un reboot del servidor para comprobar el funcionamiento del servicio

BONUS: https://askubuntu.com/questions/919054/how-do-i-run-a-single-command-at-startup-using-systemd

## Gestión de la aplicación
Lo primero que tenemos que hacer es definir el entorno de ejecución de la aplicación para ello disponemos de la variable de sistema ENV que puede tener dos modos.
- Modo desarrollo

        export ENV="dev"
    
- Modo producción

        export ENV="prod"

En caso de no iniciarla o iniciarla con otros valores la aplicación entenderá que se ejecuta en modo de desarrollo.
Con esto podemos ejecutar la aplicación en sus distintos modos.

Para la gestión de Scipio disponemos de un script que permite realizar diferentes acciones sobre la aplicación. Este esta situado en la carpeta scripts situada en la raiz del proyecto.


Para ejecutarlo primero le tenemos que dar permisos de ejecución
    
    chmod +x scripts/server.sh
    
Ya podemos ejecutarlo, para para lanzarlo nos situamos en el directorio donde esta la carpeta scipio.
 
    ./[ruta]/scipio/server.sh

 Una vez los ejecutemos nos mostrar las siguientes opciones.
1) Arrancar servidor
2) Reiniciar servidor
3) Parar servidor
4) Iniciar angular
5) Iniciar express
6) Actualizar repositorio
7) Monitor	
8) Info SCIPIO_BACK
9) Info SCIPIO_FRONT
10) Gestor DB
11) Salir
#### 1) Arrancar servidor
Permite arrancar el servidor con el modo indicado por la variable de entorno ENV, esto arranca tanto el servidor de Angular como el servidor del backend.
#### 2) Reiniciar el servidor
Permite reiniciar el servidor con el modo indicado por la variable de entorno ENV.
#### 3) Parar servidor
Para los servidores.
#### 4)Iniciar Angular
Inicia el servidor de angular en modo desarrollo, esto deja la ventana bloqueada para pararlo usar ctrl+c para parar la ejecución.
#### 5) Iniciar express
Inicia el servidor de Node js con express, esto deja la ventana bloqueada para pararlo usar ctrl+c para parar la ejecución.
#### 6) Actualizar repositorio
Descarga la ultima version de la aplicación del repositorio y actualiza.
#### 7) Monitor
Abre el monitor del servidor, muestra el estado del mismo y las entradas de log.
#### 8) info SCIPIO_BACK
Muestra la información del servidor pm2 con respecto al Backend.
#### 9) info SCIPIO_FRONT
Muestra la información del servidor pm2 con respecto al frontend de angular.
#### 10) Gestor DB
Abre el gestor de la base de datos, que dispone de la siguientes opciones.

1) Crear DB
2) Reiniciar tablas
3) Salir
##### 1) Crear BD
Permite crear la base de datos y las tablas necesarias para el uso de la aplicación, además de introducir los datos básicos de funcionamiento. Para crear la base de datos requiere 
de usuario y clave de MySQL server con permisos para ello.
##### 2) Reiniciar tablas
Reinicia las tablas de la base de datos al estado inicial.
##### 3) Salir
Cierra el gestor de la base de datos.
#### 11) Salir
Cierra el gestor de la aplicación.

##Pruebas
Para poder probar la aplicación se ha creado una serie de usuarios para facilitar esta tarea, a continuación se muestra un listado con los mismos.

- test.admin
- test.profesor
- test.profesor2
- test.alumno
- test.alumno2
- test.alumno3
- test.alumno4
- test.alumno5
- test.alumno6
- test.alumno7

Todos ellos usan la contraseña Tfg2019! para acceder.
