\externaldocument{Disenyo}

\chapter{Introduction}
\label{cap:introduccionEn}

\chapterquote{Computers are like Old Testament gods; lots of rules and no mercy.}{Joseph John Campbell (1904-1987)}

\section{Motivation}
\app\ is motivated by the need to improve communication between students and teachers when correcting programming exercises. It 
provides tools to facilitate code review by teachers, which can then not only mark errors, but also explain what is wrong and include a corrected version. Once reviews are finished, students can receive a report that allows them to see all corrections in the context of their surrounding code.

With current tools, mainly the campus-wide installation of Moodle, there are difficulties in communicating corrections of programming practices between teachers and students: neither  can open or review code easily. For example, if a teacher complains that \emph{line 10 of foo.c is wrong}, the student would have to fire up en external editor just to check what it said on that specific line. 
Moodle's tools also fail to provide any tool to summarize in one document all comments and corrections; if the teacher wanted to do this, much time would be wasted copying and pasting code. Instead, teachers would probably just explain general problems affecting many students, instead of the specifics of what is good or bad with each student's individual answer.

As a student at the School of Computer Science (\emph{Facultad de Informática}) of the UCM, in subjects that have a strong programming component such as Fundamentals of Programming, Programming Technology or in higher courses such as Web Applications, Data Structures and Algorithms or Algorithmic Techniques in Software Engineering, I have noticed that communication between teachers and students, or even between students, is both complex and time-consuming. Because of the tight schedule, in general the necessary time is never allocated.

Finally, a great motivation on my part was to improve my knowledge of new methodologies, languages and technologies currently in vogue in real business environments.

With the above mentioned motivations \app\ is born, with the spirit of creating a platform that meets these needs and improves as well as streamlines this important task for those learning to program.


\section{Goals}

\subsection{Main goals}

The objectives we intend to meet are detailed below.

\subsubsection*{Programming exercise correction}
Teachers are one of the main actors of our application, and one of our goals is to allow them to review and correct code submitted by students.
Therefore, through the use of the application, teachers will be able to correct the code of exercises turned in by students. This includs being able to make comments and corrections directly on the code, which, after a generated report, the student will be able access. These comments and corrections will be summarized into a single document when so requested. The teacher can also activate the peer-review correction function described in the advanced objectives.

\subsubsection*{Integration with Moodle}
Integration with the virtual campus allows teachers greater agility when using \app\ to review code.

Teachers can download the exercises delivered in Moodle \citep{Moodle}, and upload them into \app\ quickly and easily.
Internally they are distributed among available teams (one or more students) through an internally-unique identifier in the file name, using the identifier of the group to which the delivery belongs.

\subsubsection*{Students can see their code-reviews}
Feedback between teachers and students is a basic part of our application.
Therefore, students must be able to see the comments and corrections made by the teacher, in addition to the report generated. With this system created interaction between teacher and students, improving communication between them.

\subsubsection*{Teachers can manage their classes and assignments}

Teachers will be able to manage their subjects based on a common base subject and be able to manage their student teams, tasks and deliveries in a simple way.

\subsubsection*{Administrators can manage the platform}
The administrator may perform maintenance and updating of the elements that make up the platform. Its main tasks will be to maintain the common base of subjects and the management of teacher profiles.

\subsection{Advanced goals}

Once the previous goals have been achieved, we also propose the following advanced objectives.

\subsubsection*{Peer review between students}

Students, through a randomized system, may make corrections on the exercises of other classmates. This provides another learning tool, allowing students to see the work of their peers and learn from their successes and mistakes.

\subsubsection*{Grading of peer-reviews}

Students may issue their assessment of the work of other students. When doing so, they will follow identical steps as teachers, including the generation of a summary pdf report with comments and possible corrections made in the code, visible only to the teacher and the authors of the reviewed code. 

\section{Recapitulation of goals}
\begin{description}
\item[O1] Programming exercise correction. Design and discussion in section \ref{subsec:O1}.
\item[O2] Integration with Moodle. Design and discussion in the section \ref{subsec:O2}
\item[O3] Students can see their code-reviews. Design and discussion in section \ref{subsec:O3}
\item[O4] Teachers can manage their classes and assignments. Design and discussion in section \ref{subsec:O4}
\item[O5] Administrators can manage the platform. Design and discussion in section \ref{subsec:O5}
\item[O6] “Peer review” between students. Design and discussion in the section \ref{subsec:A1}
\item[O7] Grading of peer-reviews. Design and discussion in section \ref{subsec:A2}
\end{description}

\section{Layout of this paper}

The state of the art describes various ways to address the problem identified in motivation, and possible approaches to meet our goals.

In the design section, possible technologies that can be applied, as well as architectures, are described; including why \texttt{angular2} was chosen over \texttt{nodejs}, with \texttt{codemirror} as the main component. This section also describes the process by which the applied work methodology was chosen, and what it consists of.

The implementation section describes how the implementation of the \app\ system was carried out, and also the fundamental milestones of its development. It also provides a brief description of its internal architecture (for more details, see appendices and above all, the code in its official repository%
\footnote{https://bitbucket.org/olayer66/scipio}%
).

Finally, the results section describes the main contributions made in this work: the system delivered, as well as the resources provided for its deployment and management; and recapitulates the design and engineering that has been used for it.

The chapter on conclusions and future work relates, first, the objectives to the results; and later, future lines are indicated by which \app\ could be improved.


