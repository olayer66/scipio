#!/bin/bash

SCRIPT_FOLDER="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
APP_FOLDER="$SCRIPT_FOLDER/../"
OLD_WD="$( pwd )"
FRONT_PORT=8080
echo "Bienvenido al sistema de gestion de SCIPIO"
PS3='Selecciona una opcion: '
options=("Arrancar servidor" "Reiniciar servidor" "Parar servidor" "Iniciar angular" "Iniciar express" "Actualizar repositorio" "Monitor" "Info SCIPIO_BACK" "Info SCIPIO_FRONT" "Gestor DB" "Salir")

cd "$APP_FOLDER"

select opt in "${options[@]}"
do
    case $opt in
        "Arrancar servidor")
			pm2 describe SCIPIO_BACK > /dev/null
			if test $? -ne 0 ; then
				if [ "$ENV" == "prod" ] ; then
					echo "Iniciando modo de produccion"
					pm2 start $APP_FOLDER/ecosystem.config.js
					sh -c "cd $APP_FOLDER && ng build --prod --aot --output-hashing=none"
					pm2 serve $APP_FOLDER/dist/scipio $FRONT_PORT --name SCIPIO_FRONT
					echo "Scipio online on http://localhost:8080"
				else
					echo "Iniciando modo de desarrollo; Ctrl+C para salir"
					sh -c "cd $APP_FOLDER && ng serve -o > /dev/null &"
					nodemon $APP_FOLDER/server/app.js
				fi
			else
				echo "El servidor ya esta en funcionamiento (localhost:4200)"
				pm2 list
			fi
			break
            ;;
        "Reiniciar servidor")
			pm2 describe SCIPIO_BACK > /dev/null
			if test $? -eq 0 ; then
				pm2 restart SCIPIO_BACK
				pm2 restart SCIPIO_FRONT
			else
				echo "El servidor no esta iniciado"
				if [ "$ENV" == "prod" ] ; then
					echo "Iniciando modo de produccion"
					pm2 start $APP_FOLDER/ecosystem.config.js
					sh -c "cd $APP_FOLDER && ng build --prod --aot --output-hashing=none"
					pm2 serve $APP_FOLDER/dist/scipio $FRONT_PORT --name SCIPIO_FRONT
				else
					echo "Iniciando modo de desarrollo"
					pm2 start $APP_FOLDER/ecosystem.config.js
					sh -c "cd $APP_FOLDER && ng serve -o"
					nodemon $APP_FOLDER/server/app.js
				fi
			fi
			break
            ;;
        "Parar servidor")
			if test $? -eq 0 ; then
				pm2 stop SCIPIO_BACK
				pm2 delete SCIPIO_BACK
				pm2 stop SCIPIO_FRONT
				pm2 delete SCIPIO_FRONT
				pkill "ng build"
			else
				echo "El servidor ya esta detenido"
			fi
			break
            ;;
		"Iniciar angular")
				echo "Iniciando angular en modo desarrollo"
				sh -c "cd $APP_FOLDER && ng serve -o"
			break
			;;
	  "Iniciar express")
				echo "Iniciando express en modo desarrollo"
				nodemon $APP_FOLDER/server/app.js
			break
			;;
		"Actualizar repositorio")
			sh -c "cd $APP_FOLDER && git pull && npm install"
			pm2 restart SCIPIO_BACK
			pm2 restart SCIPIO_FRONT
			break
			;;
		"Monitor")
			pm2 monit
			break
			;;
		"Info SCIPIO_BACK")
			pm2 show SCIPIO_BACK
			break
			;;
	  "Info SCIPIO_FRONT")
			pm2 show SCIPIO_FRONT
			break
			;;
		"Gestor DB")
			$APP_FOLDER/scripts/db.sh
			break
			;;
        "Salir")
			echo "Adios USUARIO, que tenga un buen ciclo"
            break
            ;;
        *) echo "Opcion incorrecta $REPLY";;
	esac
done

cd "$OLD_WD"
