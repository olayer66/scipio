#!/bin/bash

SCRIPT_FOLDER="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
APP_FOLDER="$SCRIPT_FOLDER/../"

MYSQL=`which mysql`

echo "Bienvenido al sistema de gestion de SCIPIO"
PS3='Selecciona una opcion: '
options=("Crear DB" "Reiniciar tablas" "Salir")
select opt in "${options[@]}"
do
    case $opt in
        "Crear DB")
            read -r -p "¿Estas seguro? [y/N] " response
            case "$response" in
                [yY][eE][sS]|[yY])
                      Q1="CREATE DATABASE IF NOT EXISTS scipioDB;"
                      Q2="GRANT ALL ON *.* TO 'tfg'@'localhost' IDENTIFIED BY 'Tfg2019!';"
                      Q3="FLUSH PRIVILEGES;"
                      SQL="${Q1}${Q2}${Q3}"
                      read -r -p "User: " USER
                      $MYSQL --silent -u "$USER" -p -e "$SQL"
                      if [ $? -ne "0" ]; then
                       echo "Adios USUARIO, que tenga un buen ciclo"
                      else
                        node /$APP_FOLDER/server/DDBB/DBManager.js create
                      fi
                    ;;
                *)
                   echo "Adios USUARIO, que tenga un buen ciclo"
                    ;;
            esac
            break
            ;;
          "Reiniciar tablas")
            read -r -p "¿Estas seguro? [y/N] " response
            case "$response" in
                [yY][eE][sS]|[yY])
                    node /$APP_FOLDER/server/DDBB/DBManager.js create
                    ;;
                *)
                   echo "Adios USUARIO, que tenga un buen ciclo"
                    ;;
            esac
            break
            ;;
        "Salir")
			      echo "Adios USUARIO, que tenga un buen ciclo"
            break
            ;;
        *) echo "Opcion incorrecta $REPLY";;
    esac
done
