module.exports = {
  apps : [{
    name: 'SCIPIO_BACK',
    script: 'app.js',
    cwd:'./server',
    output:'./logs/out.log',
    error:'./logs/error.log',
    merge_logs: true,
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    exp_backoff_restart_delay: 1000,
    instances: 1,
    exec_mode: "fork",
    env: {
      NODE_ENV: 'development'
    },
    env_production: {
      NODE_ENV: 'production'
    }
  }]
};
