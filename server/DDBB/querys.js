var logger = require('../../conf/winston');

/*-------------------------------------------CRUD--------------------------------------------*/
function insert(knex,table,data){
    return knex(table).insert(data);
}
function update(knex,table,id,data){
    return knex(table).update(data).where(id.field,'=',id.value);
}
function logicalRemove(knex,table,id){
    return knex(table).update({active:false}).where(id.field,'=',id.value);
}
function logicalActivate(knex,table,id){
    return knex(table).update({active:true}).where(id.field,'=',id.value);
}
function remove(knex,table,id){
    return knex(table).where(id.field,'=',id.value).del();
}
/*-------------------------------------------PUBLIC-------------------------------------------*/
function loginUser(knex, userName, password) {
    return knex.select('user_id')
        .from('users')
        .where('user_name', 'like', userName)
        .andWhere('password', 'like', password);
}
/*--------------------------------------------SECURE------------------------------------------*/
function fileTypes(knex){
    return knex('fileTypes');
}
function isProfAssoc(knex,profId,subjectId){
    return knex('profassoc')
        .where('subject_id','=', subjectId)
        .andWhere('prof_id','=', profId);
}
function submissionOwner(knex,submissionId){
    return knex('submissions')
        .where('submission_id','=',submissionId);
}
function isTeamMember(knex,studentId,teamId){
    return knex('teamMembers')
        .where('team_id','=',teamId)
        .andWhere('student_id','=',studentId);
}
function getCorrectionsBySubmissionId(knex,subId){
    return knex('corrections').where('submission_id','=',subId);
}
function getCorrection(knex, corrId){
    return knex('corrections').where('correction_id','=',corrId);
}
function getFileCommentsByFileName(knex,correctionId,fileName){
    return knex('commentLines').where('correction_id','=',correctionId).andWhere('file_name','=',fileName);
}
function getSubmissions(knex,assignId){
    return knex('submissions AS su').where('assignment_id','=',assignId);
}
function getSubmissionByTeamId(knex,assignId,teamId){
    return knex('submissions').where('assignment_id','=',assignId).andWhere('team_id','=',teamId);
}
function getSubmissionReviewsByTeamId(knex,assignId,teamId){
    return knex('submissions').where('assignment_id','=',assignId).andWhere('peer_review_team_id','=',teamId);
}
function getSubmissionByAssignmentId(knex,assignmentId){
    return knex('submissions').where('assignment_id','=',assignmentId);
}
function getSubjectsProfAssoc(knex,userId){
    return knex('profassoc AS pa').select('sj.subject_id','sj.name AS sj_name','co.name AS co_name','co.abbr_code','sj.active')
                                .leftJoin('subjects AS sj','sj.subject_id','pa.subject_id')
                                .leftJoin('codes AS co','co.code_id','sj.code_id')
                                .where('pa.prof_id','=',userId)
                                .andWhere('sj.active','=',true);
}
function getSubjectsStudent(knex,userId){
    return knex('teamMembers AS tm').select('sj.subject_id','sj.name AS sj_name','sj.edition','sj.code_id','co.name AS co_name','co.abbr_code','sj.active','te.team_id')
                                    .leftJoin('teams AS te','te.team_id','tm.team_id')
                                    .leftJoin('subjects AS sj','te.subject_id','sj.subject_id')
                                    .leftJoin('codes AS co','co.code_id','sj.code_id')
                                    .where('tm.student_id','=',userId)
                                    .andWhere('sj.active','=',true);
}
function getAssignmentsBySubject(knex,subjectId) {
    return knex('assignments').where('subject_id','=',subjectId);
}
function getActiveAssignmentsBySubject(knex,subjectId) {
    return knex('assignments').where('subject_id','=',subjectId).andWhere('active','=',true);
}

function getTeamMembersInfo(knex,teamId){
    return knex('teamMembers AS tm').select('us.user_id','us.name','us.surnames','us.email','us.tel_number')
                                    .leftJoin('users AS us','us.user_id','tm.student_id')
                                    .where('team_id','=',teamId);
}
function getCorrectionsBySubmission(knex,submissionId){
    return knex('corrections').where('submission_id','=',submissionId);
}
function getCorrectionsBySubmissionAndOwner(knex,submissionId,userId){
    return knex('corrections').where('submission_id','=',submissionId).andWhere('reviewer_id','=',userId);
}
function getCorrectionsBySubmissionAndTeam(knex,submissionId,teamId){
    return knex('corrections').where('submission_id','=',submissionId).andWhere('team_id','=',teamId);
}
function countCommentedLinesByFile(knex,correctionId) {
    return knex('commentLines').select('file_name').count('correctionLine_id AS num_comments').where('correction_id','=',correctionId).andWhere('deleted','=',false).groupBy('file_name');
}

function getCorrectionComments(knex,correctionId) {
    return knex('commentLines').where('correction_id','=',correctionId).orderBy('file_name');
}
function CorrectionReviewer(knex,userId){
    return knex('corrections')
        .where('reviewer_id','=',userId);
}
function getSubjectCodes(knex){
    return knex('codes').then(codesData=>{
        let codes=[];
        codesData.forEach(code=>{
            codes[code.code_id]=code;
        });
        return codes;
    });
}
function getCodes(knex){
    return knex('codes');
}
function getEditions(knex){
    return knex('editions').orderBy('edition','desc');
}

function getSubjectsByProfessor(knex,userId){
    return knex('profassoc AS pa').select('sj.subject_id','sj.code_id','sj.name','sj.period','sj.edition','sj.active','sj.created_at')
        .leftJoin('subjects AS sj','sj.subject_id','pa.subject_id')
        .where('pa.prof_id','=',userId);
}
function getTeamsBySubjectId(knex,subjectId){
    return knex('teams').select('team_id').where('subject_id','=',subjectId);
}
function getProfessorsBySubjectId(knex,subjectId){
    return knex('profassoc AS pa').select('us.user_id','us.name','us.surnames','us.email').leftJoin('users AS us','us.user_id','pa.prof_id').where('subject_id','=',subjectId);
}
function getStudentsList(knex){
    return knex('users').select('user_id','name','surnames','user_name','email','active','user_type').where('user_type','=',2);
}
function getProfessorsList(knex){
    return knex('users').select('user_id','name','surnames','user_name','email','active','user_type').where('user_type','=',1);
}
function getAdminsList(knex){
    return knex('users').select('user_id','name','surnames','user_name','email','active','user_type').where('user_type','=',0);
}
function getTeamBySubject(knex,subjectId){
    return knex('teams').select('team_id').where('subject_id','=',subjectId);
}
function getSubject(knex,subjectId) {
    return knex('subjects').where('subject_id','=',subjectId);
}
function getUserByNick(knex,userName){
    return knex('users').where('user_name','=',userName);
}
function getUser(knex,userId){
    return knex('users').where('user_id','=',userId);
}
function getEdition(knex,editionId){
    return knex('editions').where('edition_id','=',editionId);
}
function checkEmailExist(knex, email) {
    return knex('users').where('email','=',email);
}
function checkUsernameExist(knex, username) {
    return knex('users').where('user_name','=',username);
}
function checkUser(knex,userId,type){
    return knex('users').where('user_id','=',userId).andWhere('user_type','=',type);
}
function checkCodGeaExist(knex, codGea) {
    return knex('codes').where('cod_gea','=',codGea);
}
function checkEditionExist(knex, edition ) {
    return knex('editions').where('edition','=',edition);
}
/*-------------------------------------------------------------------------------------------------------------------*/
function getActDate(){
    return new Date().toMysqlFormat();
}
function transformDate(date){
    return new Date(date).toMysqlFormat();
}
function twoDigits(d) {
    if(0 <= d && d < 10) return "0" + d.toString();
    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
    return d.toString();
}
Date.prototype.toMysqlFormat = function() {
    return this.getUTCFullYear() + "-" + twoDigits(1 + this.getUTCMonth()) + "-" + twoDigits(this.getUTCDate()) + " " + twoDigits(this.getUTCHours()) + ":" + twoDigits(this.getUTCMinutes()) + ":" + twoDigits(this.getUTCSeconds());
};
/*-------------------------------------------------------------------------------------------------------------------*/
module.exports = {
    getActDate,
    transformDate,
    insert,
    update,
    remove,
    logicalRemove,
    logicalActivate,
    loginUser,
    isProfAssoc,
    submissionOwner,
    isTeamMember,
    fileTypes,
    getFileCommentsByFileName,
    getCorrectionsBySubmissionId,
    getCorrection,
    getSubmissions,
    getSubmissionByTeamId,
    getSubmissionReviewsByTeamId,
    getSubjectsProfAssoc,
    getSubjectsStudent,
    getAssignmentsBySubject,
    getActiveAssignmentsBySubject,
    getTeamMembersInfo,
    getCorrectionsBySubmission,
    getCorrectionsBySubmissionAndTeam,
    getSubmissionByAssignmentId,
    countCommentedLinesByFile,
    getCorrectionComments,
    CorrectionReviewer,
    getSubjectCodes,
    getEditions,
    getSubjectsByProfessor,
    getTeamsBySubjectId,
    getProfessorsBySubjectId,
    getStudentsList,
    getProfessorsList,
    getAdminsList,
    getTeamBySubject,
    getSubject,
    getUserByNick,
    checkEmailExist,
    checkUsernameExist,
    checkUser,
    getCodes,
    checkCodGeaExist,
    checkEditionExist,
    getUser,
    getEdition
};
