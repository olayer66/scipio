'use strict';
/* DDBB schema*/

//Imports
const knex=require('./knex');
const querys =require('./querys');
const data=require('./data');
const logger=require('../../conf/winston');
//Create all tables
function createDDBB() {
  return new Promise((resolve, reject) => {
    knex.raw('select 1+1 as result').then(() => {
      logger.info('DDBB already exits');
      knex.schema.hasTable('users').then(() => {
        logger.info('DDBB tables already exits');
        deleteTables().then(()=>{
          createTables().then(() => {
            insertData().then(() => {
              logger.info('DDBB info inserted');
              resolve();
            }).catch(err => reject(err));
          }).catch(err => reject(err));
        });
      }).catch(() => {
          logger.info('DDBB creation process starts');

          createTables().then(() => {
            insertData().then(() => {
              resolve();
            }).catch(err => reject(err));
          }).catch(err => reject(err));
      });
    }).catch(() => {
      logger.info('DDBB creation process starts');
        createTables().then(() => {
            insertData().then(() => {
                resolve();
            }).catch(err => reject(err));
        }).catch(err => reject(err));
    });
  });
}
function createTables(){
  return new Promise((resolve,reject)=>{
    createUsersTable().then(()=>{
      createCodesTable().then(()=>{
        createSubjectsTable().then(()=>{
          createProfAssocTable().then(()=>{
            createTeamsTable().then(()=>{
              createTeamMembersTable().then(()=>{
                createAssignmentsTable().then(()=>{
                  createSubmissionsTable().then(()=>{
                    createCorrectionsTable().then(()=>{
                      createCommentLinesTable().then(()=>{
                          createFileTypesTable().then(()=>{
                              createEditionsTable().then(()=>{
                                  logger.info('DDBB Created');
                                  resolve();
                              }).catch(err=>reject(err));
                          }).catch(err=>reject(err));
                        }).catch(err=>reject(err));
                      }).catch(err=>reject(err));
                    }).catch(err=>reject(err));
                  }).catch(err=>reject(err));
                }).catch(err=>reject(err));
              }).catch(err=>reject(err));
            }).catch(err=>reject(err));
          }).catch(err=>reject(err));
        }).catch(err=>reject(err));
      }).catch(err=>reject(err));
    });
}
function insertData(){
  let promises=[];
  for(let table in data){
    if (data.hasOwnProperty(table))
      promises.push(querys.insert(knex, table, data[table]));
  }
  return promises.reduce((promiseChain, currentTask) => {
    return promiseChain.then(chainResults =>
      currentTask.then(currentResult =>
        [ ...chainResults, currentResult ]));
  }, Promise.resolve([])).then(() => {
    logger.info('All info inserted');
  });
}
function deleteTables(){
  let promises=[];
  for(let table in data){
    if (data.hasOwnProperty(table))
      promises.unshift(knex.schema.dropTableIfExists(table));
  }
  return promises.reduce((promiseChain, currentTask) => {
    return promiseChain.then(chainResults =>
      currentTask.then(currentResult =>
        [ ...chainResults, currentResult ]));
  }, Promise.resolve([])).then(() => {
    logger.info('All tables dropped');
  });
}
/*-------------------------------------------------------TABLES-------------------------------------------------------*/
function createUsersTable(){
  return knex.schema.createTable('users',function(t){
        t.increments('user_id').primary();
        t.integer('user_type');//0=admin 1=professor 2=student
        //User login data
        t.string('user_name',20).unique().index();
        t.string('password',255).nullable();
        //User generic data
        t.string('name',20).nullable();
        t.string('surnames', 50).nullable();
        t.string('email', 100).nullable();
        t.integer('tel_number').nullable();
        //Other
        t.boolean('active').nullable();//logical erasing
        t.timestamp('created_at').defaultTo(knex.fn.now());
      });
}
function createCodesTable() {
     return knex.schema.createTable('codes', function (t) {
        t.increments('code_id').primary();
        t.integer('cod_gea').unique().index();//  subject code in the university
        t.string('abbr_code').unique().index();// subject abbreviation code example: IW,EDA,TAIS,TP,etc
        t.string('name', 100).nullable();//Name of the subject example: Ingenieria web
        t.timestamp('created_at').defaultTo(knex.fn.now());
        t.unique(['cod_gea','abbr_code']);
      });
}
function createEditionsTable() {
    return knex.schema.createTable('editions', function (t) {
        t.increments('edition_id').primary();
        t.string('edition').unique().index();// subject abbreviation code example: IW,EDA,TAIS,TP,etc
        t.boolean('actual').defaultTo(false);// subject abbreviation code example: IW,EDA,TAIS,TP,etc
        t.timestamp('created_at').defaultTo(knex.fn.now());
    });
}
function createSubjectsTable(){
     return knex.schema.createTable('subjects',function(t){
        t.increments('subject_id').primary();
        t.integer('code_id').unsigned();// Code of the subject example: IW
        t.foreign('code_id').references('codes.code_id');
        t.string('name',100).notNullable();
        t.string('period',30).notNullable();// anual,primer cuatri, segundo cuatri
        //Other
       t.string('edition',50).notNullable();
        t.boolean('active').nullable().defaultTo(true);//logical erasing
        t.timestamp('created_at').defaultTo(knex.fn.now());
        t.unique(['code_id','edition','name']);
      });
}
function createProfAssocTable() {
    return knex.schema.createTable('profassoc', function (t) {
        t.increments('profassoc_id').primary();
        t.integer('subject_id').unsigned();// Id of the subject
        t.foreign('subject_id').references('subjects.subject_id');
        t.integer('prof_id').unsigned();//professor
        t.foreign('prof_id').references('users.user_id');
        t.timestamp('created_at').defaultTo(knex.fn.now());
        t.unique(['subject_id','prof_id']);
    });
}
function createTeamsTable(){
        return knex.schema.createTable('teams',function(t){
            t.increments('team_id');
            t.integer('subject_id').unsigned();//subject Id
            t.foreign('subject_id').references('subjects.subject_id');
            t.timestamp('created_at').defaultTo(knex.fn.now());
            t.unique(['team_id','subject_id']);
        });
}
function createTeamMembersTable() {
    return knex.schema.createTable('teamMembers', function (t) {
      t.increments('teamMember_id');
      t.integer('team_id').unsigned();//subject Id
      t.foreign('team_id').references('teams.team_id');
      t.integer('student_id').unsigned();//student(user) id
      t.foreign('student_id').references('users.user_id');
      t.timestamp('created_at').defaultTo(knex.fn.now());
      t.unique(['team_id','student_id']);
    });
}
function createAssignmentsTable(){
        return knex.schema.createTable('assignments',function(t){
            t.increments('assignment_id').primary();
            t.integer('subject_id').unsigned().nullable().index();//subject Id
            t.foreign('subject_id').references('subjects.subject_id');
            t.string('name',75).nullable();//Job name
            t.text('description','longtext');//Description
            t.date('end_date').nullable();// Job limit date
            t.boolean('peer_review').notNullable().defaultTo(false);
            t.integer('have_submission').notNullable().defaultTo(0);//0 no 1 yes active 2 yes inactive
            t.boolean('active').nullable().defaultTo(true);//logical erasing
            t.timestamp('created_at').defaultTo(knex.fn.now());
        });
}
function createSubmissionsTable(){
        return knex.schema.createTable('submissions',function(t){
            t.increments('submission_id').primary();
            t.integer('assignment_id').unsigned().nullable().index();//Job Id
            t.foreign('assignment_id').references('assignments.assignment_id');
            t.integer('team_id').unsigned().nullable().index();//group id
            t.foreign('team_id').references('teams.team_id');
            t.integer('peer_review_team_id').unsigned().nullable();//peer review group id
            t.foreign('peer_review_team_id').references('teams.team_id');
            t.string('path',100).nullable();//path to files
            t.timestamp('created_at').defaultTo(knex.fn.now());
        });
}
function createCorrectionsTable(){
        return knex.schema.createTable('corrections',function(t){
            t.increments('correction_id').primary();
            t.integer('submission_id').unsigned();//delivery Id
            t.foreign('submission_id').references('submissions.submission_id');
            t.integer('reviewer_id').unsigned();//professor(user) id
            t.foreign('reviewer_id').references('users.user_id');//User ID reviewing correction
            t.integer('team_id').unsigned().defaultTo(0);//if peer review student teamId (0 if Teacher correction)
            t.integer('state').nullable().defaultTo(0).index();//0=active 1=corrected 2=time limit (job ends)
            t.string('doc_path',50).nullable();//path to correction document if state 1 or 2
            t.timestamp('created_at').defaultTo(knex.fn.now());
            t.unique(['submission_id','reviewer_id','team_id']);
        });
}
function createCommentLinesTable() {
    return knex.schema.createTable('commentLines', function (t) {
          t.increments('correctionLine_id').primary();
          t.text('message').nullable();//Message text
          t.integer('correction_id').unsigned().index().nullable();//Correction id that owns
          t.string('file_name',250).notNullable().index();//file name that owns
          t.foreign('correction_id').references('corrections.correction_id');
          t.integer('line_start').nullable();//Start line
          t.integer('line_end').nullable();//end line
          t.text('original_code').nullable();//Original code line/s
          t.text('modified_code').nullable();//Modified code line/s
          t.boolean('deleted').notNullable().defaultTo(false);//Logical erase
          t.boolean('code_modified').notNullable().defaultTo(false);//Code modified or only comment
          t.timestamp('created_at').defaultTo(knex.fn.now());
          t.unique(['correction_id','line_start','line_end','file_name'])
    });
}
function createFileTypesTable() {
    return knex.schema.createTable('fileTypes', function (t) {
        t.increments('fileType_id').primary();
        t.string('extension',10).notNullable();//File extension
        t.string('type',250).nullable();//Codemmirror highlighting mode
        t.timestamp('created_at').defaultTo(knex.fn.now());
        t.unique(['extension', 'type'])
    });
}
module.exports= {
    createDDBB
};
