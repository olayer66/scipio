//Express
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
let app = express();
const SERVER_PORT=3000;
//Knex
const logger = require('../conf/winston');
//Routes
const publicRoutes= require('./routes/publicRoutes');
const secureRoutes= require('./routes/secureRoutes');
//Secure
const auth=require('./secure/auth');

app.use(cors());
app.options('*', cors()); // include before other routes
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.use('/public',publicRoutes);
app.use('/secure',auth.ensureAuthenticated,secureRoutes);
//app.use('/secure',secureRoutes);
app.listen(SERVER_PORT, function() {
  logger.info('Server started at port '+SERVER_PORT);
});
