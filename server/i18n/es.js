module.exports=(crDetails)=>({
    "main_title":`Informe de corrección`,
    "date":`Fecha`,
    "team_id":`Equipo ${crDetails.teamId}`,
    "file_header":"Fichero ",
    "comment":"Comentario linea ",
    "correction":"Corrección linea " ,
    "prep_to":" a ",
    "or_code":"Código original",
    "mod_code":"Código modificado",
    "summary":"Resumen",
    "com_cor":"Comentarios/Correcciones",
    "com_cor_single":"Comentario/Corrección",
    "assigment":"Tarea",
    "footer":"Facultad de Informatica Universidad complutense"
});
