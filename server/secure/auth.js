const knex= require('../DDBB/knex');
const knexQuerys = require('../DDBB/querys');
//Winston logger
const logger = require("../../conf/winston");

const path = require('path');
const appDir = path.dirname(require.main.filename);

//Encryption
const jwt = require('jsonwebtoken');
const fs= require('fs');
const publicKEY  = fs.readFileSync(appDir+'/secure/keys/public.key', 'utf8');

exports.ensureAuthenticated = function(req, res, next) {
    if(!req.headers.token) {
        logger.error("Authorization header missing");
        return res.status(403).send({'code':'ERR-13','error': "Authorization header missing"});
    }
    const token = req.headers.token;
    jwt.verify(token,publicKEY,{algorithms: ['RS256']},(err,payload)=>{
        if(err){
            logger.error(err.message);
            return res.status(403).send({'code':'ERR-14','error': "Invalid token"});
        }else{
            console.log(payload)
            if(payload.userId>0 && payload.type>=0 && payload.type<3){
                knexQuerys.checkUser(knex,payload.userId,payload.type).then(users=>{
                    if (users && users[0]) {
                        logger.info("user "+payload.userId+" type "+payload.type+" authorized");
                        next();
                    } else {
                        logger.error("User not found");
                        return res.status(403).send({'code':'ERR-16','error': "User not found"});
                    }
                }).catch(err=>{
                    logger.error("DB query error checkUser: "+err.message);
                    return res.status(403).send({'code':'ERR-06','error': "DB query error"});
                });
            }else{
                logger.error("Invalid userId or type data");
                return res.status(403).send({'code':'ERR-15','error': "Invalid userId or type data"});
            }
        }
    });
};

