/*
* Controlador de la tareas
*/
//Import Modules
const fs = require('fs');
const path = require('path');
const appDir = path.dirname(require.main.filename);
const dirTree = require("directory-tree");
const logger = require("../../conf/winston");
const dirTreeConf ={
    exclude:[
        /node_modules/,
        /.idea/,/.project/,/.classpath/,/.c9/,/.settings/,/.sublime-workspace/,
        /.vscode/,
        /.sass-cache/,/coverage/,
        /.DS_Store/,
        /nbproject/,
        /icons/,/images/,/img/,/svg/,
        /temp/
    ],
    normalizePath: true,
    hideEmptyDirectories:true
};
// Knex
const knex= require('../DDBB/knex');
const knexQuerys = require('../DDBB/querys');

/*--------------------------------------------------------------------------------------------------------------------*/
async function getFolderTree(req, res){
  if(req.body) {
    let folderPath = [];
    if(req.body.path){
      folderPath= req.body.path.split('/');
      for (let i = 0; i < folderPath.length; i++) {
        folderPath[i] = +folderPath[i];
      }
    }
    const userId =req.body.userId ? req.body.userId : 0;
    const userType = req.body.userType ? req.body.userType : '';
    if(!folderPath)
      res.status(400).send({code:'ERR-01', error:'Wrong path'});
    if(isValidPath(folderPath)){
      if(await haveAuth(userId,userType,folderPath)){
        const path =appDir+'/files/' + folderPath.join('/');
        const folderTree = await dirTree(path, dirTreeConf,null,null);
        res.status(200).json(folderTree);
      }else{
          logger.error('Unauthorized user '+userId+' type: '+userType);
          res.status(401).send({code:'ERR-02', error:'Unauthorized user'});
      }
    }else {
        logger.error('Invalid path '+userId+' type: '+userType +' path: '+ folderPath.join('/'));
        res.status(400).send({code: 'ERR-03', error: 'Invalid path'});
    }
  }else{
      logger.error('No input data');
      res.status(400).send({code:'ERR-04', error:'No input data'});
  }
}
async function getFolderFile(req,res){
  if(req.body) {
    let folderPath = [];
    let fileName;
    let path;
    if(req.body.path){
      let filePath =req.body.path;
      const userId =req.body.userId ? req.body.userId : 0;
      const userType = req.body.userType ? req.body.userType : '';
      if(!folderPath)
        res.status(400).send({code:'ERR-01', error:'Wrong path'});
      path=filePath.split('/');
      filePath=path.slice(path.indexOf('files')+1,path.length-1);
      for (let i = 0; i < filePath.length; i++) {
          filePath[i].Value = +filePath[i].Value;
      }
      fileName = path.slice(path.indexOf('files')+1,path.length);
      if(isValidPath(filePath)) {
          const path = appDir + '/files/' + fileName.join('/')
          const fileContent = await fs.readFileSync(path, "utf8");
          let resp = {"content": fileContent};
          res.status(200).send(resp);
      }else {
          logger.error('Invalid path '+userId+' type: '+userType +' path: '+ fileName.join('/'));
          res.status(400).send({code: 'ERR-03', error: 'Invalid path'});
      }
    }else{
        logger.error('A route has not been received');
        res.status(400).send({code:'ERR-05', error:'A route has not been received'});
    }
  }else {
      logger.error('No input data');
      res.status(400).send({code: 'ERR-04', error: 'No input data'});
  }
}
async function getFileComments(req,res) {
    if(req.body) {
        const correctionId=req.body.correctionId ? req.body.correctionId : 0;
        const filename= req.body.fileName;
        knexQuerys.getCorrection(knex,correctionId).then(data=>{
            const corr=data[0];
            knexQuerys.getFileCommentsByFileName(knex,corr.correction_id,filename).then(data=>{
                let cmExt=[];
                data.forEach(cm=>{
                   if(cm.line_start!==cm.line_end){//Si el comentario ocupa mas de una linea
                       for(let i=cm.line_start;i<=cm.line_end;i++){
                           cmExt[i]=cm;
                       }
                   } else{//si el comentario solo es de una linea
                       cmExt[cm.line_start]=cm;
                   }
                });
                res.status(200).send({status:corr.status,comments:cmExt,docPath:corr.doc_path,correctionId:corr.correction_id});
            }).catch(err=>{
                logger.error('DB error: '+err.message);
                res.status(400).send({code: 'ERR-06', error: 'DB query error'});
            });
        }).catch(err=>{
            logger.error('DB error: '+err.message);
            res.status(400).send({code: 'ERR-06', error: 'DB query error'})
        });
    } else {
        logger.error('No input data');
        res.status(400).send({code: 'ERR-04', error: 'No input data'});
    }
}
async function saveFileComments(req,res){
    if(req.body) {
        const userId =req.body.userId ? req.body.userId : 0;
        const userType = req.body.userType ? req.body.userType : '';
        const comments = req.body.comments ? req.body.comments : [];
        if(comments.length){
            let pos=1;
            while(pos<comments.length){
                let comment=comments[pos];
                if(!comment)
                    pos++;
                else {
                    comment.created_at= knexQuerys.getActDate();
                    if (comment.correctionLine_id) {//si es un comentario existente
                        let id={field:'correctionLine_id',value:comment.correctionLine_id};
                        delete comment.correctionLine_id;
                        knexQuerys.update(knex,'commentLines',id,comment).then(()=>{
                            console.log('update');
                        });
                    } else {//si es un nuevo comentario
                        knexQuerys.insert(knex,'commentLines',comment).then(()=>{
                            console.log('insert');
                        });
                    }
                    if (comment.line_start === comment.line_end)//Comentario de linea
                        pos++;
                    else//comentario multilinea
                        pos = comment.line_end + 1;
                }
            }
        }
        res.status(200).send({});
    } else {
        logger.error('No input data');
        res.status(400).send({code: 'ERR-04', error: 'No input data'});
    }
}
/*----------------------------------AUXILIAR---------------------------------*/
function isValidPath(folderPath){
  if(folderPath && folderPath.length>=3) {
    const path =appDir+'/files/' + folderPath.join('/');
    return fs.existsSync(path);
  }else return false;
}
async function haveAuth(userId,userType,path){
  if(userType==='TEACHER'){
    const result = await knexQuerys.isProfAssoc(knex,userId,path[0]);
    return result && result.length;
  }else if (userType==='STUDENT'){
    const so = await knexQuerys.submissionOwner(knex,path[2]);
    const tm = await knexQuerys.isTeamMember(knex,userId,so[0].team_id);
    const cr = await knexQuerys.CorrectionReviewer(knex,userId);
    return (so && tm)  || cr;
  }else
    return false;
}

module.exports={
    getFolderTree,
    getFolderFile,
    getFileComments,
    saveFileComments
};
