// Knex
const knex= require('../DDBB/knex');
const knexQuerys = require('../DDBB/querys');
//Winston logger
const logger = require("../../conf/winston");
const bcrypt = require('bcrypt');
const saltRounds = 10;
async function createUser(req,res){
    if(req.body){
        const name = req.body.name?req.body.name:'';
        const surnames = req.body.surnames?req.body.surnames:'';
        const email = req.body.email?req.body.email:'';
        const user_name = req.body.user_name?req.body.user_name:'';
        const password = req.body.password?req.body.password:'';
        const user_type = req.body.user_type?req.body.user_type:3;
        const active = req.body.active?req.body.active:false;
        if(name!=='' && surnames!=='' && email!=='' && user_name!=='' && password!=='' && user_type<3){
            bcrypt.genSalt(saltRounds, function(err, salt) {
                bcrypt.hash(password, salt, function(err, hash) {
                   let data={'name':name,'surnames':surnames,'email':email,'user_name':user_name,'password':hash,'user_type':user_type,'active':active};
                   knexQuerys.insert(knex,'users',data).then(resp=>{
                       logger.info('User '+resp+' inserted');
                       res.status(200).send({'status':'ok'});
                   }).catch(err=>{
                       logger.error('DB error insert user: '+err.message);
                       res.status(400).send({code: 'ERR-10', error: 'Failed to create user'})
                   });
                });
            });
        }else {
            logger.error('Invalid data');
            res.status(400).send({code: 'ERR-08', error: 'Invalid data'});
        }
    } else {
        logger.error('No input data');
        res.status(400).send({code: 'ERR-04', error: 'No input data'});
    }
}
async function getStudentList(req,res) {
    if(req.params){
        if(req.params.id){
            let teams=[];
            let banList=[];
            let teamIds= await knexQuerys.getTeamsBySubjectId(knex,req.params.id);
            for(let x=0; x<teamIds.length;x++){
                let teamId=teamIds[x].team_id;
                teams = teams.concat(await knexQuerys.getTeamMembersInfo(knex,teamId));
            }
            for(let x=0; x<teams.length;x++){
                banList.push(teams[x].user_id);
            }
            knexQuerys.getStudentsList(knex).then(list=>{

                if(list && list.length>0) {
                    if (teams.length) {
                        list = list.filter(student => banList.indexOf(student.user_id) === -1);
                        res.status(200).send({'usersList': list});
                    } else
                        res.status(200).send({'usersList': list});
                }else{
                    logger.error('No information available getStudentsList');
                    res.status(400).send({code: 'ERR-09', error: 'No information available'});
                }
            }).catch(err=>{
                logger.error('DB error getStudentsList: '+err.message);
                res.status(400).send({code: 'ERR-06', error: 'DB query error'});
            });
        }else {
            logger.error('Invalid data');
            res.status(400).send({code: 'ERR-08', error: 'Invalid data'});
        }
    } else {
        logger.error('No input data');
        res.status(400).send({code: 'ERR-04', error: 'No input data'});
    }
}
async function getProfessorsList(req,res){
    if(req.params){
        if(req.params.id){
            let banList=[];
            let professors=[];
            if(req.params.id!==0){
                professors = await knexQuerys.getProfessorsBySubjectId(knex,req.params.id);
                for(let x=0; x<professors.length;x++){
                    banList.push(professors[x].user_id);
                }
            }
            knexQuerys.getProfessorsList(knex).then(list=>{
                if(list && list.length>0){
                    if(professors.length){
                        list=list.filter(student=>banList.indexOf(student.user_id) === -1);
                        res.status(200).send({'usersList':list});
                    }else
                        res.status(200).send({'usersList':list});
                }else{
                    logger.error('No information available getProfessorsList');
                    res.status(400).send({code: 'ERR-09', error: 'No information available'});
                }
            }).catch(err=>{
                logger.error('DB error getProfessorsList: '+err.message);
                res.status(400).send({code: 'ERR-06', error: 'DB query error'});
            });
        }else {
            logger.error('Invalid data');
            res.status(400).send({code: 'ERR-08', error: 'Invalid data'});
        }
    } else {
        logger.error('No input data');
        res.status(400).send({code: 'ERR-04', error: 'No input data'});
    }
}
async function getUsersList(req,res){
    try {
        const professors=await knexQuerys.getProfessorsList(knex);
        const students=await knexQuerys.getStudentsList(knex);
        const admins=await knexQuerys.getAdminsList(knex);
        res.status(200).send({'professors':professors,'students':students,'admins':admins});
    }catch (err) {
        logger.error('Error getting admin users list: '+err.message);
        res.status(400).send({code: 'ERR-09', error: 'No information available'});
    }
}
async function changePassword(req,res){
    if(req.body){
        const userId=req.body.userId?req.body.userId:0;
        const password=req.body.password?req.body.password:'';
        if(userId>0 && password!==''){
            bcrypt.genSalt(saltRounds, function(err, salt) {
                bcrypt.hash(password, salt, function (err, hash) {
                    knexQuerys.update(knex,'users',{'field':'user_id','value':userId},{'password':hash}).then(()=>{
                        res.status(200).send({'status':'ok'});
                    }).catch(err=>{
                        logger.error('DB error update user password: '+err.message);
                        res.status(400).send({code: 'ERR-10', error: 'Failed to update user password'});
                    });
                })
            });
        }else {
            logger.error('Invalid data');
            res.status(400).send({code: 'ERR-08', error: 'Invalid data'});
        }
    } else {
        logger.error('No input data');
        res.status(400).send({code: 'ERR-04', error: 'No input data'});
    }
}
async function updateUser(req,res){
    if(req.body){
        const userId=req.body.user_id?req.body.user_id:0;
        const name=req.body.name?req.body.name:'';
        const surnames=req.body.surnames?req.body.surnames:'';
        const username=req.body.user_name?req.body.user_name:'';
        const email=req.body.email?req.body.email:'';
        if(userId>0 && name!=='' && surnames!=='' && username!=='' && email!==''){
            const data={'name':name,'surnames':surnames,'user_name':username,'email':email};
            knexQuerys.update(knex,'users',{'field':'user_id','value':userId},data).then(()=>{
                res.status(200).send({'status':'ok'});
            }).catch(err=>{
                logger.error('DB error update user: '+err.message);
                res.status(400).send({code: 'ERR-10', error: 'Failed to update user'});
            });
        }else {
            logger.error('Invalid data');
            res.status(400).send({code: 'ERR-08', error: 'Invalid data'});
        }
    } else {
        logger.error('No input data');
        res.status(400).send({code: 'ERR-04', error: 'No input data'});
    }
}
/*-------------------------------------------------CHECKERS FOR FORMS-------------------------------------------------*/
async function checkEmail(req,res){
    if(req.body){
        const email=req.body.email?req.body.email:'';
        if(email!==''){
            knexQuerys.checkEmailExist(knex,email).then(resp=>{
               if(resp && resp[0])
                   res.status(200).send({'exists':true});
               else
                   res.status(200).send({'exists':false});
            }).catch(err=>{
                logger.error('DB error checkEmailExist: '+err.message);
                res.status(400).send({code: 'ERR-06', error: 'DB query error'});
            });
        }else {
            logger.error('Invalid data');
            res.status(400).send({code: 'ERR-08', error: 'Invalid data'});
        }
    } else {
        logger.error('No input data');
        res.status(400).send({code: 'ERR-04', error: 'No input data'});
    }
}
async function checkUsername(req,res){
    if(req.body){
        const username=req.body.username?req.body.username:'';
        if(username!==''){
            knexQuerys.checkUsernameExist(knex,username).then(resp=>{
                if(resp && resp[0])
                    res.status(200).send({'exists':true});
                else
                    res.status(200).send({'exists':false});
            }).catch(err=>{
                logger.error('DB error checkUsernameExist: '+err.message);
                res.status(400).send({code: 'ERR-06', error: 'DB query error'});
            });
        }else {
            logger.error('Invalid data');
            res.status(400).send({code: 'ERR-08', error: 'Invalid data'});
        }
    } else {
        logger.error('No input data');
        res.status(400).send({code: 'ERR-04', error: 'No input data'});
    }
}
async function changeUserState(req,res){
    if(req.body){
        const userId=req.body.userId?req.body.userId:0;
        if(userId>0){
            const user = await knexQuerys.getUser(knex,userId);
            let changeState=1;
            console.log(user);
            if(user [0].active===1)
                changeState=0;
            console.log(changeState);
            knexQuerys.update(knex,'users',{'field':'user_id','value':userId},{'active':changeState}).then(()=>{
                res.status(200).send({'status':'ok'});
            }).catch(err=>{
                logger.error('DB error update user: '+err.message);
                res.status(400).send({code: 'ERR-06', error: 'DB query error'});
            });
        }else {
            logger.error('Invalid data');
            res.status(400).send({code: 'ERR-08', error: 'Invalid data'});
        }
    } else {
        logger.error('No input data');
        res.status(400).send({code: 'ERR-04', error: 'No input data'});
    }
}
async function getProfile(req,res){
    if(req.params){
        const userId=req.params.id?req.params.id:0;
        if(userId>0){
            knexQuerys.getUser(knex,userId).then(users=>{
                res.status(200).send(users[0]);
            }).catch(err=>{
                logger.error('DB error getUser: '+err.message);
                res.status(400).send({code: 'ERR-06', error: 'DB query error'});
            });
        }else {
            logger.error('Invalid data');
            res.status(400).send({code: 'ERR-08', error: 'Invalid data'});
        }
    } else {
        logger.error('No input data');
        res.status(400).send({code: 'ERR-04', error: 'No input data'});
    }
}
module.exports={
    getStudentList,
    getProfessorsList,
    createUser,
    checkEmail,
    checkUsername,
    getUsersList,
    changePassword,
    updateUser,
    changeUserState,
    getProfile
};
