// Knex
const knex= require('../DDBB/knex');
const knexQuerys = require('../DDBB/querys');
//Winston logger
const logger = require("../../conf/winston");


async function getAssignments(req,res){
    if(req.body) {
        const userId =req.body.userId ? req.body.userId : 0;
        const userType = req.body.userType ? req.body.userType : '';
        if(userType==='TEACHER'){//Submissions si e prof asignado de la asignatura al la que pertenece la tarea de la submission
            knexQuerys.getSubjectsProfAssoc(knex,userId).then(subjects=>{
                if(subjects && subjects.length){//el profe tiene asignaturas
                    let assignments={};
                    subjects.filter(sub=>sub.active===1);
                    let count=subjects.length;
                    subjects.forEach(sub=>{
                        assignments[sub.subject_id]=[];
                        knexQuerys.getAssignmentsBySubject(knex,sub.subject_id).then(resp=>{
                            if(resp && resp.length)
                                assignments[sub.subject_id]=resp;
                            count--;
                            if(count===0)
                                res.status(200).send({subjects:subjects,assignments:assignments});
                        }).catch(err=>{
                            count--;
                            logger.error('DB error getAssignmentsBySubject: '+err.message);
                            if(count===0)
                                res.status(200).send({subjects:subjects,assignments:assignments});
                        });
                    });
                }else {
                    res.status(200).send({subjects:[],assignments:{}});
                }
            }).catch(err=>{
                logger.error('DB error getSubjectsProfAssoc: '+err.message);
                res.status(400).send({code: 'ERR-06', error: 'DB query error'})
            });
        }else if(userType==='STUDENT'){// submission si el equipo pertenece a esa tarea que esta matriculado en la asignatura
            knexQuerys.getSubjectsStudent(knex,userId).then(subjects=>{
                if(subjects && subjects.length){//el alumno tiene asignaturas
                    let assignments={};
                    subjects.filter(sub=>sub.active===1);
                    let count=subjects.length;
                    subjects.forEach(sub=>{
                        assignments[sub.subject_id]=[];
                        knexQuerys.getActiveAssignmentsBySubject(knex,sub.subject_id).then(resp=>{
                            if(resp && resp.length)
                                assignments[sub.subject_id]=resp;
                            count--;
                            if(count===0)
                                res.status(200).send({subjects:subjects,assignments:assignments});
                        }).catch(err=>{
                            count--;
                            logger.error('DB error getAssignmentsBySubject: '+err.message);
                            if(count===0)
                                res.status(200).send({subjects:subjects,assignments:assignments});
                        });
                    });
                }else {
                    res.status(200).send({subjects:[],assignments:{}});
                }
            }).catch(err=>{
                logger.error('DB error getSubjectsStudent: '+err.message);
                res.status(400).send({code: 'ERR-06', error: 'DB query error'})
            });
        }else{
            logger.error('Wrong user type');
            res.status(400).send({code: 'ERR-07', error: 'Wrong user type'});
        }
    } else {
        logger.error('No input data');
        res.status(400).send({code: 'ERR-04', error: 'No input data'});
    }
}
async function saveAssignment(req,res){
    if(req.body) {
        const assignId=req.body.assignment_id;
        req.body.created_at= knexQuerys.getActDate();
        req.body.end_date=knexQuerys.transformDate(req.body.end_date);
        if(assignId){
            delete req.body.assignment_id;
            let id={field:'assignment_id',value:assignId};
            knexQuerys.update(knex,'assignments',id,req.body).then(()=>{
                res.status(200).send({'assignment_id': assignId});
            }).catch(err=>{
                logger.error('DB error update assignment: '+err.message);
                res.status(400).send({code: 'ERR-06', error: 'DB query error'})
            });
        }else{
            knexQuerys.insert(knex,'assignments',req.body).then(id=>{
                res.status(200).send({'assignment_id': id});
            }).catch(err=>{
                logger.error('DB error insert assignment: '+err.message);
                res.status(400).send({code: 'ERR-06', error: 'DB query error'})
            });
        }
    } else {
        logger.error('No input data');
        res.status(400).send({code: 'ERR-04', error: 'No input data'});
    }
}
async function removeAssignment(req,res){
    if(req.params.id){
        knexQuerys.logicalRemove(knex,'assignments',{field:'assignment_id',value:req.params.id}).then(()=>{
            logger.info('remove assignment '+req.params.id);
        });
    }else{
        logger.error('No input data');
        res.status(400).send({code: 'ERR-04', error: 'No input data'});
    }
}
async function activateAssignment(req,res){
    if(req.body.assignment_id){
        knexQuerys.logicalActivate(knex,'assignments',{field:'assignment_id',value:req.body.assignment_id}).then(()=>{
            logger.info('Activate assignment '+req.body.assignment_id);
        });
    }else{
        logger.error('No input data');
        res.status(400).send({code: 'ERR-04', error: 'No input data'});
    }
}
module.exports ={
    getAssignments,
    saveAssignment,
    removeAssignment,
    activateAssignment
};
