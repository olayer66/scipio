// Knex
const knex= require('../DDBB/knex');
const knexQuerys = require('../DDBB/querys');
//Winston logger
const logger = require("../../conf/winston");

async function deleteTeam(req,res){
    if(req.params){
        if(req.params.id){
            const id={'field':'team_id','value':req.params.id};
            knexQuerys.remove(knex,'teamMembers',id).then(()=>{
                knexQuerys.remove(knex,'teams',id).then(()=>{
                    res.status(200).send({'status':'ok'});
                }).catch(err=>{
                    logger.error('DB error remove teamMembers: '+err.message);
                    res.status(400).send({code: 'ERR-06', error: 'DB query error'});
                });
            }).catch(err=>{
                logger.error('DB error remove teamMembers: '+err.message);
                res.status(400).send({code: 'ERR-06', error: 'DB query error'});
            });
        }else {
            logger.error('Invalid data');
            res.status(400).send({code: 'ERR-08', error: 'Invalid data'});
        }
    } else {
        logger.error('No input data');
        res.status(400).send({code: 'ERR-04', error: 'No input data'});
    }
}
async function upgradeTeam(req,res){
    if(req.body){
        const teamId=req.body.teamId?req.body.teamId:0;
        const subjectId=req.body.subjectId?req.body.subjectId:0;
        const teamInfo=req.body.teamInfo?req.body.teamInfo:[];
        let status=true;
        if(teamId>=0 && subjectId>0 && teamInfo.length>0 ){
            if(teamId===0){//New
                knexQuerys.insert(knex,'teams',{'subject_id':subjectId}).then(teamId=>{
                   for(let i=0;i<teamInfo.length;i++){
                       knexQuerys.insert(knex,'teamMembers',{'student_id':teamInfo[i].user_id,'team_id':teamId}).then(()=>{
                           logger.info('User: '+teamInfo[i].user_id+' to team '+teamId);
                       }).catch(err=>{
                           delete teamInfo[i];
                           logger.error('DB error insert teamMembers: '+err.message);
                           status=false;
                       })
                   }
                   res.status(200).send({'status':status,'teamId':teamId,'teamInfo':teamInfo});
                });
            } else {//Update only can change students
                const id={'field':'team_id','value':teamId};
                knexQuerys.remove(knex,'teamMembers',id).then(()=>{//Delete old members
                    for(let i=0;i<teamInfo.length;i++){
                        knexQuerys.insert(knex,'teamMembers',{'student_id':teamInfo[i].user_id,'team_id':teamId}).then(()=>{
                            logger.info('User: '+teamInfo[i].user_id+' to team '+teamId);
                        }).catch(err=>{
                            delete teamInfo[i];
                            logger.error('DB error insert teamMembers: '+err.message);
                            status=false;
                        })
                    }
                    res.status(200).send({'status':status,'teamId':teamId,'teamInfo':teamInfo});
                }).catch(err=>{
                    logger.error('DB error remove teamMembers: '+err.message);
                    res.status(400).send({code: 'ERR-06', error: 'DB query error'});
                });
            }
        }else {
            logger.error('Invalid data');
            res.status(400).send({code: 'ERR-08', error: 'Invalid data'});
        }
    } else {
        logger.error('No input data');
        res.status(400).send({code: 'ERR-04', error: 'No input data'});
    }
}
module.exports={
    deleteTeam,
    upgradeTeam
};
