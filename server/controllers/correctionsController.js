// Knex
const knex= require('../DDBB/knex');
const knexQuerys = require('../DDBB/querys');
//Winston logger
const logger = require("../../conf/winston");
//PDFKit
const PDFDocument= require('pdfkit');
//files
const fs = require('fs');

const path = require('path');
const appDir = path.dirname(require.main.filename);

//Text configuration
const fsBig=24;
const fsSMed=16;
const fsMin=12;
const fsText=10;
const font='Times-Roman';
const fontBold='Times-Bold';
const fontCode='Courier';
async function finalizeCorrection(req,res){
    // fichero pdf pdf/correction_id.pdf
    if(req.params && req.body){
        const subjectId=req.body.subjectId?req.body.subjectId:0;
        const submissionId=req.body.submissionId?req.body.submissionId:0;
        const assignmentId=req.body.assignmentId?req.body.assignmentId:0;
        const teamId=req.body.teamId?req.body.teamId:0;
        const assignmentName=req.body.assignmentName?req.body.assignmentName:'';
        const langId=req.body.langId?req.body.langId:'es';
        if(req.params.id && req.params.id>0 && subjectId>0 && assignmentId>0 && submissionId>0 && teamId>0 && assignmentName!==''){
            const path=subjectId+'/'+assignmentId+'/'+submissionId+'/';
            const filename=req.params.id+'.pdf';
            const today= new Date();
            const crDetails={'name':assignmentName,'teamId':teamId,'date':today.getDate()+'/'+(today.getMonth()+1)+'/'+today.getFullYear()};
            const lang= require('../i18n/'+langId+'.js')(crDetails);
            const doc = new PDFDocument({ layout: 'portrait'});
            await fs.mkdirSync(appDir+'/reports/'+path,{ recursive: true });
            const teamMembers= await knexQuerys.getTeamMembersInfo(knex,teamId);
            const countComments= await knexQuerys.countCommentedLinesByFile(knex,req.params.id);
            knexQuerys.getCorrectionComments(knex,req.params.id).then(resp=>{
                const commentsByFile={};
                resp.forEach(cm=>{
                    if(!commentsByFile[cm.file_name])
                        commentsByFile[cm.file_name]=[];
                    commentsByFile[cm.file_name].push(cm);
                    if(commentsByFile[cm.file_name].length>1)
                        commentsByFile[cm.file_name].sort((a,b) => (a.line_start > b.line_start) ? 1 : ((b.line_start > a.line_start) ? -1 : 0));
                });
                //PDF header
                doc.image(appDir+'/images/logo_sc.png',15,30,{fit: [100, 100]}).font(fontBold,fsBig).fillColor('#800000').text(lang.main_title,50,30,{ align: 'center'}).image(appDir+'/images/escudo.jpg',doc.page.width-60,15,{fit: [50, 50]}).fillColor('black').text('',{ align: 'justify'});
                doc.moveTo(15,70).lineTo(doc.page.width-15,70).stroke();
                //intro
                doc.font(fontBold,fsMin).text(lang.assigment,250,80).font(font,fsMin).text(crDetails.name);
                doc.font(fontBold,fsMin).text(lang.date,doc.page.width-175,80).font(font,fsMin).text(crDetails.date);
                doc.font(fontBold,fsMin).text(lang.team_id,15,80);
                for(const menber of teamMembers)
                    doc.font(font,fsText).text(menber.name+' '+menber.surnames+' ('+menber.email+')',15);
                doc.moveDown();
                doc.font(fontBold,fsMin).text(lang.summary,15);
                let countList=[];
                for(const fc of countComments) {
                    let sOp = fc.num_comments > 1 ? lang.com_cor : lang.com_cor_single;
                    countList.push(lang.file_header + fc.file_name + ': ' + fc.num_comments + ' ' + sOp);
                }
                doc.font(font,fsText).list(countList,{x:100});
                doc.moveDown();
                //PDF body (Comments by file)
                Object.keys(commentsByFile).forEach(async file=>{// Loop file
                    doc.moveDown();
                    doc.font(fontBold,fsSMed).fillColor('#800000').text('',15).text(lang.file_header+file,{underline:true}).fillColor('black');
                    for (const comment of commentsByFile[file]) {//loop comments
                        if(comment.code_modified){//Correction
                            if(comment.line_start < comment.line_end){//multiline
                                doc.font(fontBold,fsMin).text('',30).text(lang.correction+comment.line_start+lang.prep_to+comment.line_end,{underline:true});
                            }else {//One line
                                doc.font(fontBold,fsMin).text('',30).text(lang.comment+comment.line_start+lang.prep_to+comment.line_end,{underline:true});
                            }
                            doc.font(font,fsText).text(comment.message);
                            doc.moveDown();
                            //Insert code
                            doc.font(fontBold,fsMin).text(lang.or_code.toString());//original
                            doc.font(fontCode,fsText).fillColor('red').text(comment.original_code).fillColor('black');
                            doc.moveDown();
                            doc.font(fontBold,fsMin).text(lang.mod_code);//modified
                            doc.font(fontCode,fsText).fillColor('green').text(comment.modified_code).fillColor('black');
                        } else {//comment
                            if(comment.line_start < comment.line_end){//multiline
                                doc.font(fontBold,fsMin).text('',30).text(lang.comment+comment.line_start+lang.prep_to+comment.line_end,{underline:true});
                            }else {//One line
                                doc.font(fontBold,fsMin).text('',30).text(lang.comment+comment.line_start,{underline:true});
                            }
                            doc.font(font,fsText).text(comment.message);
                        }
                        doc.moveDown();
                    }
                });
                //Footer
                doc.moveTo(15,doc.page.height - 50).lineTo(doc.page.width-15,doc.page.height - 50).stroke();
                doc.font(font,fsMin).text(today.getFullYear()+' '+lang.footer, 175, doc.page.height - 30, {
                    align:'center',
                    lineBreak: false
                });
                 doc.pipe(fs.createWriteStream(appDir+'/reports/'+path+'/'+filename))
                    .on('finish', function () {
                        knexQuerys.update(knex,'corrections',{'field':'correction_id','value':req.params.id},{'state':2,'doc_path':path+filename}).then(()=>{
                            res.status(200).send({'path':path});
                        }).catch(err=>{
                            logger.error('DB error getSubmissionByAssignmentId: '+err.message);
                            res.status(400).send({code: 'ERR-06', error: 'DB query error'});
                        });
                    });
                 doc.end();
            }).catch(err=>{
                logger.error('DB error getSubmissionByAssignmentId: '+err.message);
                res.status(400).send({code: 'ERR-06', error: 'DB query error'})
            });
        } else {
            logger.error('Invalid data');
            res.status(400).send({code: 'ERR-08', error: 'Invalid data'});
        }
    }else {
        logger.error('No input data');
        res.status(400).send({code: 'ERR-04', error: 'No input data'});
    }
}
async function createPeerReview(req,res){
    if(req.params){
        const assignmentId= req.params.id?req.params.id:0;
        if(assignmentId!==0){
            knexQuerys.getSubmissionByAssignmentId(knex,assignmentId).then(submissions=>{
                let teamIds=[];
                submissions.forEach(sub=>{
                    teamIds.push(sub.team_id);
                });
                teamIds=shuffle(teamIds);
                let count=submissions.length;
                submissions.forEach(sub=>{
                    let tPos=0;
                    while(tPos===sub.team_id)//Hasta que Id team distinto de team dueño
                        tPos++;
                    const prTeam=teamIds[tPos];
                    teamIds.splice(tPos,1);
                    knexQuerys.update(knex,'submissions',{'field':'submission_id','value':sub.submission_id},{'peer_review_team_id':prTeam}).then(()=>{
                        knexQuerys.getTeamMembersInfo(knex,prTeam).then(teamMembers=>{
                            let insertData=[];
                            for(let i=0;i<teamMembers.length;i++)
                                insertData.push({'submission_id':sub.submission_id,'reviewer_id':teamMembers[i].user_id,'team_id':prTeam});
                            knexQuerys.insert(knex,'corrections',insertData).then(result=>{
                                count--;
                                if(count===0){
                                    knexQuerys.update(knex,'assignments',{field:'assignment_id',value:assignmentId},{'peer_review':1}).then(()=>{
                                        res.status(200).send({'status':'ok'});
                                    }).catch(err=>{
                                        logger.error('DB error getSubmissionByAssignmentId: '+err.message);
                                        res.status(400).send({code: 'ERR-06', error: 'DB query error'})
                                    });
                                }
                            }).catch(err=>{
                                count--;
                                logger.error('DB error insert corrections: '+err.message);
                                if(count===0)
                                    res.status(400).send({code: 'ERR-06', error: 'DB query error'})
                            });
                        }).catch(err=>{
                            count--;
                            logger.error('DB error getTeamMembersInfo: '+err.message);
                            if(count===0)
                                res.status(400).send({code: 'ERR-06', error: 'DB query error'})
                        });
                    }).catch(err=>{
                        count--;
                        logger.error('DB error update submission: '+err.message);
                        if(count===0)
                            res.status(400).send({code: 'ERR-06', error: 'DB query error'})
                    });
                });
            }).catch(err=>{
                logger.error('DB error getSubmissionByAssignmentId: '+err.message);
                res.status(400).send({code: 'ERR-06', error: 'DB query error'})
            });
        }else {
            logger.error('Invalid data');
            res.status(400).send({code: 'ERR-08', error: 'Invalid data'});
        }
    }else {
        logger.error('No input data');
        res.status(400).send({code: 'ERR-04', error: 'No input data'});
    }
}
async function changeStatePeerReview(req,res){
    if(req.body){
        const assignmentId=req.body.assignmentId?req.body.assignmentId:0;
        const status=req.body.status?req.body.status:0;
        if(assignmentId!==0 && status!==0){
            knexQuerys.update(knex,'assignments',{field:'assignment_id',value:assignmentId},{'peer_review':status}).then(()=>{
                res.status(200).send({'status':'ok'});
            }).catch(err=>{
                logger.error('DB error update assigment peer review: '+err.message);
                res.status(400).send({code: 'ERR-06', error: 'DB query error'})
            });
        } else {
            logger.error('Invalid data');
            res.status(400).send({code: 'ERR-08', error: 'Invalid data'});
        }
    } else {
        logger.error('No input data');
        res.status(400).send({code: 'ERR-04', error: 'No input data'});
    }
}
async function changeCorrectionState(req,res){
    if(req.body && req.params){
        if(req.body.state && req.params.id){
            knexQuerys.update(knex,'corrections',{'field':'correction_id','value':req.params.id},{'state':req.body.state}).then(()=>{
                res.status(200).send({'status':'ok'});
            }).catch(err=>{
                logger.error('DB error update correction status: '+err.message);
                res.status(400).send({code: 'ERR-06', error: 'DB query error'})
            });
        } else {
            logger.error('Invalid data');
            res.status(400).send({code: 'ERR-08', error: 'Invalid data'});
        }
    } else {
        logger.error('No input data');
        res.status(400).send({code: 'ERR-04', error: 'No input data'});
    }
}
async function countComments(req,res){
    if(req.body && req.body.correctionIds){
        let count = req.body.correctionIds.length;
        let correctionsInfo={};
        req.body.correctionIds.forEach(id=>{
            knexQuerys.countCommentedLinesByFile(knex,id).then(comments=>{
                correctionsInfo[id]=comments;
                count--;
                if(count===0)
                    res.status(200).send(correctionsInfo);
            }).catch(err=>{
                count--;
                logger.error('DB error countCommentedLinesByFile: '+err.message);
                if(count===0)
                    res.status(200).send(correctionsInfo);
            });
        });
    } else {
        logger.error('No input data');
        res.status(400).send({code: 'ERR-04', error: 'No input data'});
    }
}
async function getPdfFile(req,res){
    if(req.body && req.body.path){
        let file = fs.createReadStream(appDir+'/reports/'+req.body.path);
        let stat = fs.statSync(appDir+'/reports/'+req.body.path);
        res.setHeader('Content-Length', stat.size);
        res.setHeader('Content-Type', 'application/pdf');
        res.setHeader('Content-Disposition', 'attachment; filename=quote.pdf');
        file.pipe(res);
    } else {
        logger.error('No input data');
        res.status(400).send({code: 'ERR-04', error: 'No input data'});
    }

}
module.exports={
    createPeerReview,
    changeStatePeerReview,
    finalizeCorrection,
    changeCorrectionState,
    countComments,
    getPdfFile
};
/*-------------------------------------------------------------------------------------------------------------------*/
function shuffle(array) {
    console.log(array)
    let random=Math.floor((Math.random() * array.length)+1);
    if(random===array.length)
        random--;
    console.log('Ramdom: '+random);
    for(let i=0;i<random;i++){
        let top=array.shift();
        array.push(top);
    }
    console.log(array)
    return array;
}

