// Knex
const knex= require('../DDBB/knex');
const knexQuerys = require('../DDBB/querys');
//Winston logger
const logger = require("../../conf/winston");

async function getSubjectsData(req,res) {
    if(req.body){
        const userId =req.body.userId ? req.body.userId : 0;
        const userType = req.body.userType ? req.body.userType : '';
        if(userId>0 && userType!=='') {
            let teams={};
            let teamsInfo={};
            let profAssoc={};
            let subjectsByEdition={};
            let codes= await knexQuerys.getSubjectCodes(knex);
            let editions = await knexQuerys.getEditions(knex);
            for(let i=0;i<editions.length;i++){
                    subjectsByEdition[editions[i].edition]=[];
            }
            if(userType==='TEACHER') {
                let subjects= await knexQuerys.getSubjectsByProfessor(knex,userId);
                if(subjects && subjects.length>0){
                    for(let i=0;i<subjects.length;i++){
                        subjectsByEdition[subjects[i].edition].push(subjects[i]);
                        let subjectId=subjects[i].subject_id;
                        profAssoc[subjectId]=  await  knexQuerys.getProfessorsBySubjectId(knex,subjectId);
                        if(profAssoc[subjectId] && profAssoc[subjectId].length>0){
                            teams[subjectId]=[];
                            let teamIds= await knexQuerys.getTeamsBySubjectId(knex,subjectId);
                            for(let x=0; x<teamIds.length;x++){
                                let teamId=teamIds[x].team_id;
                                teams[subjectId].push(teamId);
                                teamsInfo[teamId]= await knexQuerys.getTeamMembersInfo(knex,teamId);
                            }
                        } else {
                            logger.error('DB error getProfessorsBySubjectId');
                        }
                    }
                    res.status(200).send({'codes':codes,'editions':editions,'subjects':subjects,'teams':teams,'profAssoc':profAssoc,'subjectsByEdition':subjectsByEdition,'teamsInfo':teamsInfo});
                }else{
                    logger.warn('DB warn getSubjectsByProfessor no data found');
                    res.status(200).send({'codes':codes,'editions':editions,'subjects':subjects,'teams':teams,'profAssoc':profAssoc,'subjectsByEdition':subjectsByEdition,'teamsInfo':teamsInfo});
                }
            }
            if(userType==='STUDENT'){
                let subjects = await knexQuerys.getSubjectsStudent(knex,userId);
                if(subjects && subjects.length>0) {
                    for (let i = 0; i < subjects.length; i++) {
                        subjectsByEdition[subjects[i].edition].push(subjects[i]);
                        let subjectId = subjects[i].subject_id;
                        profAssoc[subjectId] = await knexQuerys.getProfessorsBySubjectId(knex, subjectId);
                        if (profAssoc[subjectId] && profAssoc[subjectId].length > 0) {
                            teams[subjectId] = [];
                            let teamId = await knexQuerys.getTeamsBySubjectId(knex, subjectId);
                            console.log(teamId[0].team_id);
                            teams[subjectId].push(teamId[0].team_id);
                            teamsInfo[teamId[0].team_id] = await knexQuerys.getTeamMembersInfo(knex, teamId[0].team_id);
                        } else {
                            logger.error('DB error getProfessorsBySubjectId');
                        }
                    }
                }
                res.status(200).send({'codes':codes,'editions':editions,'subjects':subjects,'teams':teams,'profAssoc':profAssoc,'subjectsByEdition':subjectsByEdition,'teamsInfo':teamsInfo});
            }
        }else {
            logger.error('Invalid data');
            res.status(400).send({code: 'ERR-08', error: 'Invalid data'});
        }
    } else {
        logger.error('No input data');
        res.status(400).send({code: 'ERR-04', error: 'No input data'});
    }
}
async function changeStateSubject(req,res){
    if(req.body){
        const subjectId=req.body.subjectId?req.body.subjectId:0;
        if(subjectId>0){
            let active=true;
            let subject=await knexQuerys.getSubject(knex,subjectId);
            console.log(subject[0]);
            if(subject[0].active===1)
                active=false;
            knexQuerys.update(knex,'subjects',{'field':'subject_id','value':subjectId},{'active':active}).then(data=>{
                res.status(200).send({'active':active});
            }).catch(err=>{
                logger.error('DB error update subjects: '+err.message);
                res.status(400).send({code: 'ERR-06', error: 'DB query error'});
            });
        }else {
            logger.error('Invalid data');
            res.status(400).send({code: 'ERR-08', error: 'Invalid data'});
        }
    } else {
        logger.error('No input data');
        res.status(400).send({code: 'ERR-04', error: 'No input data'});
    }
}
async function upgradeSubject(req,res){
    if(req.body){
        const subject=req.body.subject?req.body.subject:{};
        const profAssoc=req.body.profAssoc?req.body.profAssoc:[];
        if(Object.keys(subject).length>0 && profAssoc.length>0){
            let status=true;
            if(!subject.subject_id){//new
                knexQuerys.insert(knex,'subjects',subject).then(subjectId=>{
                    for(let i=0;i<profAssoc.length;i++){
                        knexQuerys.insert(knex,'profassoc',{'prof_id':profAssoc[i].user_id,'subject_id':subjectId}).then(()=>{
                            logger.info('User: '+profAssoc[i].user_id+' assigned to subject '+subjectId);
                        }).catch(err=>{
                            delete profAssoc[i];
                            logger.error('DB error insert teamMembers: '+err.message);
                            status=false;
                        })
                    }
                    knexQuerys.getSubject(knex,subjectId).then(subject=>{
                        res.status(200).send({'status':status,'subject':subject[0],'profAssoc':profAssoc});
                    });
                }).catch(err=>{
                    logger.error('DB error update subjects: '+err.message);
                    res.status(400).send({code: 'ERR-06', error: 'DB query error'});
                });
            } else {//update
                let upSubject={code_id:subject.code_id,
                    name:subject.name,
                    period:subject.period,
                    edition:subject.edition,
                    active:subject.active,
                    created_at:knexQuerys.getActDate()
                };
                knexQuerys.update(knex, 'subjects',{'field':'subject_id','value':subject.subject_id},upSubject).then(() => {
                    knexQuerys.remove(knex, 'profassoc', {'field': 'subject_id', 'value': subject.subject_id}).then(() => {
                        for (let i = 0; i < profAssoc.length; i++) {
                            knexQuerys.insert(knex, 'profassoc', {'prof_id': profAssoc[i].user_id, 'subject_id': subject.subject_id}).then(() => {
                                logger.info('User: ' + profAssoc[i].user_id + ' assigned to subject ' + subject.subject_id);
                            }).catch(err => {
                                delete profAssoc[i];
                                logger.error('DB error insert teamMembers: ' + err.message);
                                status = false;
                            })
                        }
                        res.status(200).send({'status': status, 'subject': subject, 'profAssoc': profAssoc});
                    });
                });
            }
        }else {
            logger.error('Invalid data');
            res.status(400).send({code: 'ERR-08', error: 'Invalid data'});
        }
    } else {
        logger.error('No input data');
        res.status(400).send({code: 'ERR-04', error: 'No input data'});
    }
}
async function getSubjectCodes(req,res){
    try{
        const codes=await knexQuerys.getCodes(knex);
        const editions= await knexQuerys.getEditions(knex);
        res.status(200).send({'codes':codes,'editions':editions});
    }catch (err) {
        logger.error('Error getting admin codes list: '+err.message);
        res.status(400).send({code: 'ERR-09', error: 'No information available'});
    }
}
async function upgradeCode(req,res){
    if(req.body){
        const code_id=req.body.code_id?req.body.code_id:0;
        const cod_gea=req.body.cod_gea?req.body.cod_gea:0;
        const abbr_code=req.body.abbr_code?req.body.abbr_code:'';
        const name=req.body.name?req.body.name:'';
        if(cod_gea>0 && abbr_code!=='' && name!==''){
            const data={'cod_gea':cod_gea,'abbr_code':abbr_code,'name':name};
            if(code_id===0){//new
                knexQuerys.insert(knex,'codes',data).then(id=>{
                    res.status(200).send({'code_id':id,'cod_gea':cod_gea,'abbr_code':abbr_code,'name':name});
                }).catch(err=>{
                    logger.error('DB error insert code: '+err.message);
                    res.status(400).send({code: 'ERR-06', error: 'DB query error'});
                });
            }else{ //update
                knexQuerys.update(knex,'codes',{'field':'code_id','value':code_id},data).then(resp=>{
                    res.status(200).send({'code_id':code_id,'cod_gea':cod_gea,'abbr_code':abbr_code,'name':name});
                }).catch(err=>{
                    logger.error('DB error update code: '+err.message);
                    res.status(400).send({code: 'ERR-06', error: 'DB query error'});
                });
            }
        }else {
            logger.error('Invalid data');
            res.status(400).send({code: 'ERR-08', error: 'Invalid data'});
        }
    } else {
        logger.error('No input data');
        res.status(400).send({code: 'ERR-04', error: 'No input data'});
    }
}
async function upgradeEdition(req,res){
    if(req.body){
        const edition_id=req.body.edition_id?req.body.edition_id:0;
        const actual=req.body.actual?req.body.actual:false;
        const edition=req.body.edition?req.body.edition:'';
        if(edition!==''){
            const data={'edition':edition,'actual':actual};
            if(edition_id===0){//new
                knexQuerys.insert(knex,'editions',data).then(id=>{
                    res.status(200).send({'edition_id':id,'edition':edition,'actual':actual});
                }).catch(err=>{
                    logger.error('DB error insert edition: '+err.message);
                    res.status(400).send({code: 'ERR-06', error: 'DB query error'});
                });
            }else{ //update
                knexQuerys.update(knex,'editions',{'field':'edition_id','value':edition_id},data).then(resp=>{
                    res.status(200).send({'edition_id':edition_id,'edition':edition,'actual':actual});
                }).catch(err=>{
                    logger.error('DB error update edition: '+err.message);
                    res.status(400).send({code: 'ERR-06', error: 'DB query error'});
                });
            }
        }else {
            logger.error('Invalid data');
            res.status(400).send({code: 'ERR-08', error: 'Invalid data'});
        }
    } else {
        logger.error('No input data');
        res.status(400).send({code: 'ERR-04', error: 'No input data'});
    }
}
async function checkCode(req,res){
    if(req.body){
        const cod_gea=req.body.cod_gea?req.body.cod_gea:0;
        if(cod_gea>0){
            knexQuerys.checkCodGeaExist(knex,cod_gea).then(resp=>{
                if(resp && resp[0])
                    res.status(200).send({'exists':true});
                else
                    res.status(200).send({'exists':false});
            }).catch(err=>{
                logger.error('DB error checkCodGeaExist: '+err.message);
                res.status(400).send({code: 'ERR-06', error: 'DB query error'});
            });
        }else {
            logger.error('Invalid data');
            res.status(200).send({code: 'ERR-08', error: 'Invalid data'});
        }
    } else {
        logger.error('No input data');
        res.status(400).send({code: 'ERR-04', error: 'No input data'});
    }
}
async function checkEdition(req,res){
    if(req.body){
        const edition=req.body.edition?req.body.edition:0;
        if(cod_gea>0){
            knexQuerys.checkEditionExist(knex,edition).then(resp=>{
                if(resp && resp[0])
                    res.status(200).send({'exists':true});
                else
                    res.status(200).send({'exists':false});
            }).catch(err=>{
                logger.error('DB error checkEditionExist: '+err.message);
                res.status(400).send({code: 'ERR-06', error: 'DB query error'});
            });
        }else {
            logger.error('Invalid data');
            res.status(200).send({code: 'ERR-08', error: 'Invalid data'});
        }
    } else {
        logger.error('No input data');
        res.status(400).send({code: 'ERR-04', error: 'No input data'});
    }
}
async function changeEditionState(req,res){
    if(req.body){
        const editionId=req.body.editionId?req.body.editionId:0;
        if(editionId>0){
            const edition = await knexQuerys.getEdition(knex,editionId);
            let changeState=1;
            console.log(edition);
            if(edition [0].actual===1)
                changeState=0;
            console.log(changeState);
            knexQuerys.update(knex,'editions',{'field':'edition_id','value':editionId},{'actual':changeState}).then(()=>{
                res.status(200).send({'status':'ok'});
            }).catch(err=>{
                logger.error('DB error update edition: '+err.message);
                res.status(400).send({code: 'ERR-06', error: 'DB query error'});
            });
        }else {
            logger.error('Invalid data');
            res.status(400).send({code: 'ERR-08', error: 'Invalid data'});
        }
    } else {
        logger.error('No input data');
        res.status(400).send({code: 'ERR-04', error: 'No input data'});
    }
}
module.exports={
    getSubjectsData,
    changeStateSubject,
    upgradeSubject,
    getSubjectCodes,
    upgradeEdition,
    upgradeCode,
    checkCode,
    checkEdition,
    changeEditionState
};
