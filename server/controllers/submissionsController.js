// Knex
const knex= require('../DDBB/knex');
const knexQuerys = require('../DDBB/querys');
//Winston logger
const logger = require("../../conf/winston");

const path = require('path');
const appDir = path.dirname(require.main.filename);

const AdmZip = require('adm-zip');
const unzip= require('unzipper');
const fs=require('fs');
const rimraf = require("rimraf");
async function getSubmissions(req,res){
    if(req.body) {
        const userId =req.body.userId ? req.body.userId : 0;
        const userType = req.body.userType ? req.body.userType : '';
        const assignmentId= req.body.assignmentId ? req.body.assignmentId : '';
        const teamId= req.body.teamId ? req.body.teamId : 0;
        if(userType==='TEACHER'){//Submissions si e prof asignado de la asignatura a la que pertenece la tarea de la submission
            knexQuerys.getSubmissions(knex,assignmentId).then(submissions=>{
                if(submissions && submissions.length) {
                    let count = submissions.length;
                    let teamsInfo={};
                    let corrections={};
                    submissions.forEach(sub=>{
                        teamsInfo[sub.team_id]=[];
                        knexQuerys.getTeamMembersInfo(knex,sub.team_id).then(teamInfo=>{
                            teamsInfo[sub.team_id]=teamInfo;
                            knexQuerys.getCorrectionsBySubmission(knex,sub.submission_id).then(cor=>{
                                corrections[sub.submission_id]=cor;
                                count--;
                                if(count===0)
                                    res.status(200).send({submissions:submissions,teamsInfo:teamsInfo,corrections:corrections});
                            }).catch(err=>{
                                count--;
                                logger.error('DB error getCorrectionsBySubmission: '+err.message);
                                if(count===0)
                                    res.status(200).send({submissions:submissions,teamsInfo:teamsInfo,corrections:corrections});
                            });
                        }).catch(err=>{
                            count--;
                            logger.error('DB error getTeamMembersInfo: '+err.message);
                            if(count===0)
                                res.status(200).send({submissions:submissions,teamsInfo:teamsInfo,corrections:corrections});
                        });
                    });
                } else
                    res.status(200).send({submissions:[],teamsInfo:{},corrections:{}});
            }).catch(err=>{
                logger.error('DB error getSubmissions: '+err.message);
                res.status(400).send({code: 'ERR-06', error: 'DB query error'})
            });
        }else if(userType==='STUDENT'){// submission si el equipo pertenece a esa tarea que esta matriculado en la asignatura
            knexQuerys.getSubmissionByTeamId(knex,assignmentId,teamId).then(submissions=>{
                if(submissions && submissions.length)
                    knexQuerys.getSubmissionReviewsByTeamId(knex,assignmentId,teamId).then(reviewSubs=>{
                        if(reviewSubs && reviewSubs.length)
                            submissions= submissions.concat(reviewSubs);
                        console.log(submissions);
                        knexQuerys.getTeamMembersInfo(knex,teamId).then(teamInfo=>{
                            let teamsInfo={};
                            if(teamInfo && teamInfo.length)
                                teamsInfo[teamId]=teamInfo;
                            let corrections={};
                            let count = submissions.length;
                            submissions.forEach(sub=>{
                                knexQuerys.getCorrectionsBySubmissionAndTeam(knex,sub.submission_id,teamId).then(cor=>{
                                    if(cor && cor.length){
                                        corrections[sub.submission_id]=cor;
                                        count--;
                                        if(count===0)
                                            res.status(200).send({submissions:submissions,teamsInfo:teamsInfo,corrections:corrections});
                                    }else{
                                        knexQuerys.getCorrectionsBySubmission(knex,sub.submission_id,0).then(corTeam=>{
                                            corrections[sub.submission_id]=corTeam;
                                            count--;
                                            if(count===0)
                                                res.status(200).send({submissions:submissions,teamsInfo:teamsInfo,corrections:corrections});
                                        })
                                    }
                                }).catch(err=>{
                                    count--;
                                    logger.error('DB error getCorrectionsBySubmissionAndOwner: '+err.message);
                                });
                            });
                        }).catch(err=>{
                            logger.error('DB error getTeamMembersInfo: '+err.message);
                            res.status(400).send({code: 'ERR-06', error: 'DB query error'})
                        });
                    });
                else
                    res.status(200).send({submissions:[]});
            });
        }else{
            logger.error('Wrong user type');
            res.status(400).send({code: 'ERR-07', error: 'Wrong user type'});
        }
    } else {
        logger.error('No input data');
        res.status(400).send({code: 'ERR-04', error: 'No input data'});
    }
}

async function uploadSubmission(req,res){
    if(req.body){
        const subjectId=req.body.subjectId?req.body.subjectId:0;
        const assignmentId=req.body.assignmentId?req.body.assignmentId:0;
        const userId=req.body.userId?req.body.userId:0;
        if(subjectId!==0 && assignmentId!==0 && userId!==0){
            const destPath=appDir+'/files/'+subjectId.toString()+'/'+assignmentId.toString()+'/';
            const tempDirName=req.file.fieldname + Math.floor(Math.random()*1000).toString();
            const path= appDir+'/temp/'+tempDirName;
            const zip = new AdmZip(req.file.buffer);
            zip.extractAllTo(path,false);//Primer nivel de zips(estan en carpetas)
            const directories = fs.readdirSync(path);
            let count=directories.length;
            for (const dir of directories) {
                let dirPath=path +'/'+dir;
                let prZips=fs.readdirSync(dirPath);
                const teamId= prZips[0].split('.')[0];
                knexQuerys.insert(knex,'submissions',{'assignment_id':assignmentId,'team_id':teamId}).then(result=>{
                    fs.mkdir(destPath+result[0],(err)=>{
                        fs.createReadStream(dirPath+'/'+prZips[0])
                            .pipe(unzip.Extract({ path: destPath+result[0] }))
                            .on('close', function () {
                                knexQuerys.update(knex,'submissions',{'field':'submission_id','value':result[0]},{'path':subjectId+'/'+assignmentId+'/'+result[0]}).then(()=>{
                                    knexQuerys.insert(knex,'corrections',{'submission_id':result[0],'reviewer_id':userId}).then(()=>{
                                        knexQuerys.update(knex,'assignments',{'field':'assignment_id','value':assignmentId},{'have_submission':true}).then(()=>{
                                            count--;
                                            if(count===0) {
                                                rimraf(path,()=>{res.status(200).send({resp: 'ok'})});
                                            }
                                        }).catch(err=>{
                                            count--;
                                            logger.error('DB error update submission: '+err.message);
                                            if(count===0)
                                                res.status(400).send({resp: 'Error'});
                                        });
                                    });
                                }).catch(err=>{
                                    count--;
                                    logger.error('DB error update submission: '+err.message);
                                    if(count===0)
                                        res.status(400).send({resp: 'Error'});
                                });
                            });
                    });
                }).catch(err=>{
                    count--;
                    logger.error('DB error insert submission: '+err.message);
                    if(count===0)
                        res.status(400).send({resp: 'Error'});
                });
            }
        }
    }else{
        logger.error('No input data');
        res.status(400).send({code: 'ERR-04', error: 'No input data'});
    }
}

module.exports ={
    getSubmissions,
    uploadSubmission
};
