const knex= require('../DDBB/knex');
const knexQuerys = require('../DDBB/querys');
//Winston logger
const logger = require("../../conf/winston");

const path = require('path');
const appDir = path.dirname(require.main.filename);

//Encryption
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const fs= require('fs');
const privateKEY  = fs.readFileSync(appDir+'/secure/keys/private.key', 'utf8');
const publicKEY  = fs.readFileSync(appDir+'/secure/keys/public.key', 'utf8');

async function login(req,res){
    if(req.body){
        const user=req.body.user?req.body.user:'';
        const password=req.body.password?req.body.password:'';
        if(user!=='' && password!==''){
            knexQuerys.getUserByNick(knex,user).then(users=>{
                if(users && users[0]){//Have user by this nick
                    const user=users[0];
                    bcrypt.compare(password, user.password, function(err, validated) {
                        if(err) {
                            console.log(err);
                            res.status(400).send({code: 'ERR-11', error: 'Login failed'});
                        }else {
                            if (validated) {
                                const token = jwt.sign({
                                    'userId': user.user_id,
                                    'type': user.user_type
                                }, privateKEY, {algorithm: 'RS256'}); // RS512 algorithm
                                res.status(200).send({'user_id': user.user_id, 'user_type': user.user_type,'user_name':user.user_name ,'token': token});
                            } else {
                                res.status(401).send({
                                    code: 'ERR-12',
                                    error: 'Inactive user, contact an administrator'
                                });
                            }
                        }
                    });
                }else
                    res.status(400).send({code: 'ERR-11', error: 'Login failed'});
            });
        }else {
            logger.error('Invalid data');
            res.status(400).send({code: 'ERR-08', error: 'Invalid data'});
        }
    } else {
        logger.error('No input data');
        res.status(400).send({code: 'ERR-04', error: 'No input data'});
    }
}
async function logout(req,res){

}
module.exports={
    login
};
