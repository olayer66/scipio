const express = require('express');
const router = express.Router();
const usCon =require('../controllers/usersController');
const maCon =require('../controllers/mainController');
const lgCon =require('../controllers/loginController');
const sjCon = require('../controllers/subjectsControllers');

router.get('/',function(req,res){res.status(403).send({code:'ERROR-00',message:'Access denied, secure access required'})});
/*-------------------------------------------------LOGIN CONTROLLER---------------------------------------------------*/
router.post('/login',lgCon.login);
/*-------------------------------------------------USERS CONTROLLER---------------------------------------------------*/
router.put('/users/create',usCon.createUser);
router.post('/users/checkemail',usCon.checkEmail);
router.post('/users/checkusername',usCon.checkUsername);
router.post('/subjects/checkcode',sjCon.checkCode);
router.post('/subjects/checkedition',sjCon.checkEdition);
/*--------------------------------------------------MAIN CONTROLLER---------------------------------------------------*/
router.get('/codemirror/fileTypes',maCon.getFileTypes);
module.exports = router;
