const express = require('express');
const router = express.Router();
const multer = require('multer');
const cmCon = require('../controllers/codeMirrorController');
const smCon = require('../controllers/submissionsController');
const asCon = require('../controllers/assignmentsController');
const crCon = require('../controllers/correctionsController');
const sjCon = require('../controllers/subjectsControllers');
const tmCon = require('../controllers/teamsController');
const usCon = require('../controllers/usersController');
const lgCon =require('../controllers/loginController');

const PATH = './uploads';
let storage =multer.memoryStorage();

let upload = multer({storage: storage});

router.get('/',function(req,res){res.status(403).send({code:'ERROR-00',message:'Access denied, secure access required'})});
/*--------------------------------------------------USERS CONTROLLER--------------------------------------------------*/
router.get('/users/students/:id',usCon.getStudentList);
router.get('/users/professors/:id',usCon.getProfessorsList);
router.get('/users/admin/list',usCon.getUsersList);
router.patch('/users/update/password',usCon.changePassword);
router.patch('/users/update/user',usCon.updateUser);
router.put('/users/admin/create',usCon.createUser);
router.patch('/users/admin/state',usCon.changeUserState);
router.get('/users/profile/:id',usCon.getProfile);
/*--------------------------------------------------TEAMS CONTROLLER--------------------------------------------------*/
router.delete('/teams/delete/:id',tmCon.deleteTeam);
router.put('/teams/upgrade',tmCon.upgradeTeam);
/*------------------------------------------------SUBJECTS CONTROLLER-------------------------------------------------*/
router.post('/subjects',sjCon.getSubjectsData);
router.patch('/subjects/upgrade',sjCon.upgradeSubject);
router.patch('/subjects/state',sjCon.changeStateSubject);
router.get('/subjects/admin/list',sjCon.getSubjectCodes);
router.patch('/subjects/admin/upgrade/edition',sjCon.upgradeEdition);
router.patch('/subjects/admin/upgrade/code',sjCon.upgradeCode);
router.patch('/subjects/admin/edition/state',sjCon.changeEditionState);
/*-----------------------------------------------ASSIGNMENTS CONTROLLER-----------------------------------------------*/
router.post('/assignments',asCon.getAssignments);
router.put('/assignments/save',asCon.saveAssignment);
router.delete('/assignments/remove/:id',asCon.removeAssignment);
router.patch('/assignments/activate',asCon.activateAssignment);
/*-----------------------------------------------SUBMISSION CONTROLLER------------------------------------------------*/
router.post('/submissions',smCon.getSubmissions);
router.post('/submissions/upload',upload.single('subFile'),smCon.uploadSubmission);
/*-----------------------------------------------CODEMIRROR CONTROLLER------------------------------------------------*/
router.post('/codemirror/Tree',cmCon.getFolderTree);
router.post('/codemirror/file',cmCon.getFolderFile);
router.post('/codemirror/file/comments',cmCon.getFileComments);
router.put('/codemirror/file/comments/save',cmCon.saveFileComments);
/*-----------------------------------------------CORRECTIONS CONTROLLER-----------------------------------------------*/
router.get('/corrections/peer/activate/:id',crCon.createPeerReview);
router.patch('/corrections/peer/change',crCon.changeStatePeerReview);
router.post('/corrections/send/:id',crCon.finalizeCorrection);
router.patch('/corrections/:id/state',crCon.changeCorrectionState);
router.post('/corrections/comments/count',crCon.countComments);
router.post('/corrections/pdf',crCon.getPdfFile);
/*--------------------------------------------------------------------------------------------------------------------*/

module.exports = router;
