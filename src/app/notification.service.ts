import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import {TranslateService} from "@ngx-translate/core";

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private toastr: ToastrService, private translate: TranslateService) { }
  success(reqTrans,message, title){
    if(reqTrans){
      this.translate.get(message, {value: 'world'}).subscribe((res: string) => {
        message=res;
      });
      this.translate.get(title, {value: 'world'}).subscribe((res: string) => {
        title=res;
      });
    }
    this.toastr.success(message, title);
  }
  error(reqTrans,message, title){
    if(reqTrans){
      this.translate.get(message, {value: 'world'}).subscribe((res: string) => {
        message=res;
      });
      this.translate.get(title, {value: 'world'}).subscribe((res: string) => {
        title=res;
      });
    }
    this.toastr.error(message, title);
  }
  warn(reqTrans,message,title){
    if(reqTrans){
      this.translate.get(message, {value: 'world'}).subscribe((res: string) => {
        message=res;
      });
      this.translate.get(title, {value: 'world'}).subscribe((res: string) => {
        title=res;
      });
    }
    this.toastr.warning(message,title);
  }
}
