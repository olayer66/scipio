import { BrowserModule } from '@angular/platform-browser';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {HttpClient, HttpClientModule } from '@angular/common/http';
import { DatePipe } from '@angular/common';

import { AngularMaterialModule } from './angular-material.module';
import {
    MatDialogModule, MatIconModule,
    MatMenuModule,
    MatNativeDateModule,
    MatSidenavModule,
    MatToolbarModule
} from "@angular/material";
import {MAT_DATE_LOCALE} from '@angular/material';
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from './app.component';

import {PublicModule} from './public/public.module';
import {SecureModule} from './secure/secure.module';
import {Globals} from './globals';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {FlexLayoutModule} from '@angular/flex-layout';

import { ToastrModule } from 'ngx-toastr';

import {FileUploadModule} from 'ng2-file-upload';
// import ngx-translate and the http loader
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {UpgradeAssignmentComponent} from "./secure/assignments/upgrade-assignment/upgrade-assignment.component";
import {UploadSubmissionComponent} from "./secure/assignments/upload-submission/upload-submission.component";
import {commentLineComponent} from "./secure/codemirror/comment-modal/comment-line.component";
import {ConfirmationDialogComponent} from "./secure/confirmation-dialog/confirmation-dialog.component";
import {ConfirmationDialogService} from "./secure/confirmation-dialog/confirmation-dialog.service";
import {EditTeamComponent} from "./secure/subjects/edit-team/edit-team.component";
import {EditSubjectComponent} from "./secure/subjects/edit-subject/edit-subject.component";
import {CreateStudentComponent} from "./public/login/create-student/create-student.component";
import {UserModalComponent} from "./secure/admin-users/user-modal/user-modal.component";
import {CodeModalComponent} from "./secure/admin-codes/code-modal/code-modal.component";
import {ChangePasswordComponent} from "./secure/admin-users/change-password/change-password.component";
import {EditionModalComponent} from "./secure/admin-codes/edition-modal/edition-modal.component";
import {CreateUserComponent} from './secure/admin-users/create-user/create-user.component';
import {ChangeProfilePwdComponent} from "./secure/profile/change-profile-pwd/change-profile-pwd.component";

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
    declarations: [
      AppComponent,
      ConfirmationDialogComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        PublicModule,
        SecureModule,
        BrowserAnimationsModule,
        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        AngularMaterialModule,
        MatDialogModule,
        MatNativeDateModule,
        FlexLayoutModule,
        ToastrModule.forRoot({
            positionClass:'top-center',
            closeButton: true,

        }),
        MatSidenavModule,
        MatToolbarModule,
        MatMenuModule,
        MatIconModule
    ],
    exports:[TranslateModule,FileUploadModule],
    providers: [Globals,DatePipe,{ provide: MAT_DATE_LOCALE, useValue: 'es-ES' },ConfirmationDialogService],
    bootstrap: [AppComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    entryComponents:[
        UpgradeAssignmentComponent,
        UploadSubmissionComponent,
        commentLineComponent,
        ConfirmationDialogComponent,
        EditSubjectComponent,
        EditTeamComponent,
        CreateStudentComponent,
        UserModalComponent,
        CodeModalComponent,
        ChangePasswordComponent,
        EditionModalComponent,
        CreateUserComponent,
        ChangeProfilePwdComponent
    ]
})
export class AppModule { }

