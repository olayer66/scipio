import {ChangeDetectorRef, Component} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {MatIconRegistry} from "@angular/material";
import {SecureService} from "./secure/secure.service";
import {MediaMatcher} from "@angular/cdk/layout";
import {DomSanitizer} from "@angular/platform-browser";
import {Globals} from "./globals";
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'scipio';
  translate:TranslateService;
  langSelected:string='es';
  private params: any;
    mobileQuery: MediaQueryList;
   readonly _mobileQueryListener: () => void;
   smallScreen: boolean;
  constructor(translate: TranslateService,private ss:SecureService,changeDetectorRef: ChangeDetectorRef, media: MediaMatcher,iconRegistry: MatIconRegistry, sanitizer: DomSanitizer,gb:Globals){
    iconRegistry.addSvgIcon('earth', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/svg/earth.svg'));
    iconRegistry.addSvgIcon('es', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/svg/es.svg'));
    iconRegistry.addSvgIcon('gb', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/svg/gb.svg'));
    iconRegistry.addSvgIcon('copyright', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/svg/copyright.svg'));
    this.smallScreen = window.innerWidth < gb.minWidth;
    this.translate=translate;
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
    translate.setDefaultLang(this.langSelected);
  }
  changeLang(lang){
    this.langSelected=lang;
    this.translate.use(lang);
  }
}
