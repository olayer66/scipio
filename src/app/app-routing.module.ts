import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MainPageComponent} from "./public/main-page/main-page.component";
import {LoginComponent} from "./public/login/login.component";

const routes: Routes = [
  { path: '',pathMatch: 'full', redirectTo: 'login'}, // Home page, info, instructions,etc
  { path: 'login',component:LoginComponent},
  { path: 'secure', loadChildren:'./secure/secure.module#SecureModule'},
];
@NgModule({
  imports: [RouterModule.forRoot(routes,{ enableTracing: false })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
