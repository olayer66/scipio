import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainPageComponent } from './main-page/main-page.component';
import {AppRoutingModule} from "../app-routing.module";
import { LoginComponent } from './login/login.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {AngularMaterialModule} from "../angular-material.module";
import {TranslateModule} from "@ngx-translate/core";
import {MatCardModule} from "@angular/material/card";
import {FlexModule} from "@angular/flex-layout";
import { CreateStudentComponent } from './login/create-student/create-student.component';
@NgModule({
    imports: [
        CommonModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        AngularMaterialModule,
        TranslateModule,
        MatCardModule,
        FlexModule
    ],
  exports: [LoginComponent,],
  declarations: [MainPageComponent, LoginComponent, CreateStudentComponent]
})
export class PublicModule { }
