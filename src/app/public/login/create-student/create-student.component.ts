import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MatDialogRef} from "@angular/material/dialog";
import {LoginComponent} from "../login.component";
import {APIService} from "../../api.service";
//Custom validators
import {MatchValue} from "../../../validators/match-value.validator";
import {EmailUnique} from "../../../validators/email-unique.validator";
import {UsernameUnique} from "../../../validators/username-unique.validator";
import {User} from "../../../secure/interfaces.model";

@Component({
    selector: 'app-create-student',
    templateUrl: './create-student.component.html',
    styleUrls: ['./create-student.component.scss']
})
export class CreateStudentComponent implements OnInit {
    userForm: FormGroup;
    submitted=false;
    private user:User;
    constructor( private dialogRef: MatDialogRef<LoginComponent>,private formBuilder: FormBuilder,private api:APIService) {
        this.user={
            name:'',
            surnames:'',
            email:'',
            user_name:'',
            password:'',
            user_type:2,
            active:true
        }
    }

    ngOnInit() {
        this.userForm = this.formBuilder.group({
            name:[this.user.name, Validators.required],
            surnames:[this.user.surnames, Validators.required],
            email:[this.user.email, [Validators.required,Validators.email]],
            username: [this.user.user_name, [Validators.required,Validators.minLength(6)]],
            password: [this.user.password, [Validators.required,Validators.pattern('^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s).*$'),Validators.minLength(8)]],
            confirmPassword: ['', [Validators.required,Validators.pattern('^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s).*$'),Validators.minLength(8)]],
        }, {
            validators:[ MatchValue('password', 'confirmPassword'),
                         EmailUnique('email',this.api),
                         UsernameUnique('username',this.api)
            ]
        });
    }
    send(){
        this.submitted=true;
        if(!this.userForm.invalid) {
            this.user.name=this.userForm.value.name;
            this.user.surnames=this.userForm.value.surnames;
            this.user.email=this.userForm.value.email;
            this.user.user_name=this.userForm.value.username;
            this.user.password=this.userForm.value.password;
            this.dialogRef.close(this.user);
        }
    }
    close() {
        this.dialogRef.close();
    }
}
