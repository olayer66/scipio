import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthenticationService} from "../authentication.service";
import {first} from "rxjs/operators";
import {Globals} from "../../globals";
import {MediaMatcher} from "@angular/cdk/layout";
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import {CreateStudentComponent} from "./create-student/create-student.component";
import {APIService} from "../api.service";
import {NotificationService} from "../../notification.service";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    error = '';
    private smallScreen: boolean;
    mobileQuery: MediaQueryList;
    private _mobileQueryListener: () => void;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        changeDetectorRef: ChangeDetectorRef,
        media: MediaMatcher,
        private gb:Globals,
        private dialog: MatDialog,
        private api:APIService,
        private ns: NotificationService,
    ) {
        // redirect to home if already logged in
        this.smallScreen = window.innerWidth < gb.minWidth;

        this.mobileQuery = media.matchMedia('(max-width: 600px)');
        this._mobileQueryListener = () => changeDetectorRef.detectChanges();
        this.mobileQuery.addListener(this._mobileQueryListener);
        if (this.authenticationService.currentUserValue) {
            this.router.navigate(['/secure'],{ queryParams: this.authenticationService.currentUserValue });
        }
    }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: ['', [Validators.required,Validators.minLength(8)]],
            password: ['', [Validators.required,Validators.minLength(8)]]
        });

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/secure';
    }

    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }

    login() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }
        this.loading = true;
        this.authenticationService.login(this.f.username.value, this.f.password.value)
            .pipe(first())
            .subscribe(
                data => {
                    console.log(data);
                    this.router.navigate([this.returnUrl],{queryParams:data});
                },
                error => {
                    this.error = error;
                    this.loading = false;
                });
    }
    newStudent(){
        const dialogConfig = new MatDialogConfig();
        dialogConfig.autoFocus = true;
        const dialogRefUpload = this.dialog.open(CreateStudentComponent, dialogConfig);
        dialogRefUpload.afterClosed().subscribe(data => {
            if(data)
                this.api.createStudent(data).subscribe(resp=>{
                    this.ns.success(true,'new_student_success','success');
                }, error=>{
                    this.ns.error(false,error.error.error,error.error.code);
                })
        });
    }
}
