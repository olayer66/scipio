import {Injectable, NgZone} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Globals} from "../globals";
import {CheckFieldResp} from "./interfaces.model";
import {User} from "../secure/interfaces.model";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class APIService {
  private http: HttpClient;
  private gb: Globals;
  private localIp:any;

  private ipRegex = new RegExp(/([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/);

  constructor(private httpClient: HttpClient, private globals: Globals,private zone: NgZone) {
    this.http = httpClient;
    this.gb = globals;
    this.determineLocalIp();
    this.localIp=sessionStorage.getItem('LOCAL_IP');
  }
  //login
  public login(username:string,password:string){
    return this.http.post<any>(this.gb.serverUrl+`public/login`, { 'user':username, 'password':password },{headers: this.gb.headers});
  }
//Create user validations
  public checkEmail(email:string){
    return this.http.post<CheckFieldResp>(this.gb.serverUrl+'public/users/checkemail',{'email':email},{headers: this.gb.headers});
  }
  public checkUsername(username:string){
    return this.http.post<CheckFieldResp>(this.gb.serverUrl+'public/users/checkusername',{'username':username},{headers: this.gb.headers});
  }
  public createStudent(user:User){
    return this.http.put(this.gb.serverUrl+'public/users/create',user,{headers: this.gb.headers});
  }
  //Check subjects data
  public checkCode(cod_gea:number){
    return this.http.post<CheckFieldResp>(this.gb.serverUrl+'public/subjects/checkcode',{'cod_gea':cod_gea},{headers: this.gb.headers});
  }
  public checkEdition(edition:string){
    return this.http.post<CheckFieldResp>(this.gb.serverUrl+'public/subjects/checkedition',{'edition':edition},{headers: this.gb.headers});
  }
  /*---------------------------------------DYNAMIC IP DETECTTION------------------------------------------------------*/
  private determineLocalIp() {
    window.RTCPeerConnection = this.getRTCPeerConnection();
    const pc = new RTCPeerConnection({iceServers: []});
    pc.createDataChannel('');
    pc.createOffer().then(pc.setLocalDescription.bind(pc));
    pc.onicecandidate = (ice) => {
      this.zone.run(() => {
        if (!ice || !ice.candidate || !ice.candidate.candidate)
          return;
        this.localIp = 'http://'+this.ipRegex.exec(ice.candidate.candidate)[1]+':3000/';
        sessionStorage.setItem('LOCAL_IP', this.localIp);
        pc.onicecandidate = () => {};
        pc.close();
      });
    };
  }
  private getRTCPeerConnection() {
    return window.RTCPeerConnection ||
        window.mozRTCPeerConnection ||
        window.webkitRTCPeerConnection;
  }
}
