import { Component, OnInit } from '@angular/core';
import {User} from "../../secure/interfaces.model";

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent implements OnInit {
   userRoot:User;
   userTeacher:User;
   userStudent:User;
  constructor() {
    this.userRoot={
      user_id:1,
      user_type:0,
      user_name:'mfreire',
      password:'1234',
      name:'paco',
      surnames:'Porras Aguilar',
      active:true,
      email:'Falso@ucm.es',
      tel_number:234543342,
      created_at:'2019-08-02 11:51:08'
    };
    this.userTeacher={
      user_id:2,
      user_type:1,
      user_name:'profesor',
      password:'1234',
      name:'paco',
      surnames:'Porras Aguilar',
      active:true,
      email:'prof1@ucm.es',
      tel_number:234543342,
      created_at:'2019-08-02 11:51:08'
    };
    this.userStudent={
      user_id:3,
      user_type:2,
      user_name:'PacoPor',
      password:'1234',
      name:'paco',
      surnames:'Porras Aguilar',
      active:true,
      email:'Falso@ucm.es',
      tel_number:234543342,
      created_at:'2019-08-02 11:51:08'
    };
  }

  ngOnInit() {
  }

}
