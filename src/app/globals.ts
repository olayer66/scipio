import { Injectable } from '@angular/core';
import {HttpHeaders} from '@angular/common/http';

@Injectable()
export class Globals {
  serverUrl = 'http://localhost:3000/';
  headers = {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*'
  };
  userTypes =['ADMIN','TEACHER','STUDENT'];
  minWidth =1025;
  spanishRangeLabel = (page: number, pageSize: number, length: number) => {
    if (length == 0 || pageSize == 0) { return `0 de ${length}`; }

    length = Math.max(length, 0);

    const startIndex = page * pageSize;

    // If the start index exceeds the list length, do not try and fix the end index to the end.
    const endIndex = startIndex < length ?
        Math.min(startIndex + pageSize, length) :
        startIndex + pageSize;

    return `${startIndex + 1} - ${endIndex} de ${length}`;
  }
}
