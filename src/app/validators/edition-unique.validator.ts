import { FormGroup } from "@angular/forms";// custom validator to check that two fields match
import {APIService} from "../public/api.service";
export function EditionUnique(controlName: string, api:APIService) {
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        if (control.errors && !control.errors.validEdition) {
            // return if another validator has already found an error on the matchingControl
            return;
        }
        // set error on matchingControl if validation fails
        api.checkEdition(control.value).subscribe(resp=>{
            if (resp.exists) {
                control.setErrors({ validEdition: true });
            } else {
                control.setErrors(null);
            }
        });
    }
}
