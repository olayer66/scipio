import { FormGroup } from "@angular/forms";// custom validator to check that two fields match
import {APIService} from "../public/api.service";
export function EmailUnique(controlName: string, api:APIService) {
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        if (control.errors && !control.errors.validMail) {
            // return if another validator has already found an error on the matchingControl
            return;
        }
        // set error on matchingControl if validation fails
        api.checkEmail(control.value).subscribe(resp=>{
            if (resp.exists) {
                control.setErrors({ validMail: true });
            } else {
                control.setErrors(null);
            }
        });
    }
}
