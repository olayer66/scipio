import {ChangeDetectorRef, Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {SecureService} from "./secure.service";
import {MediaMatcher} from "@angular/cdk/layout";
import {TranslateService} from "@ngx-translate/core";
import {MatSidenav} from "@angular/material";
import {Globals} from "../globals";
import {AuthenticationService} from "../public/authentication.service";

@Component({
  selector: 'app-code-viewer',
  templateUrl: './secure.component.html',
  styleUrls: ['./secure.scss']
})
export class SecureComponent implements OnInit {
  @ViewChild('snav',{static: false }) sidenav: MatSidenav;
   userType: string;
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.configureSideNav()
  }

   smallScreen: boolean=false;
   params: any;
    mobileQuery: MediaQueryList;
   readonly _mobileQueryListener: () => void;
   translate: TranslateService;
   langSelected: any;
   title:string='test';
   userImage ='./assets/images/anonymous-user.png';
  constructor(translate: TranslateService,private route: ActivatedRoute,private router: Router,private ss:SecureService,changeDetectorRef: ChangeDetectorRef, media: MediaMatcher,private gb:Globals,private authenticationService: AuthenticationService) {
    this.smallScreen = window.innerWidth < gb.minWidth;

    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
    this.translate=translate;
    this.route.queryParams.subscribe(params => {
      this.params=params;
      this.ss.getQueryParams(params);
    });
  }

  ngOnInit() {

    this.userType=this.gb.userTypes[this.params.user_type];
    if(this.params.userImage)
      this.userImage =this.params.userImage;
    if(this.userType==="ADMIN"){
      this.router.navigate(['/admin/users'],{ queryParams: this.params });
    }else{
      this.router.navigate(['/subjects'],{ queryParams: this.params });
    }
  }

  changeLang(lang){
    this.langSelected=lang;
    this.translate.use(lang);
  }
  logout(){
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }
  configureSideNav() {
    this.smallScreen = window.innerWidth < this.gb.minWidth;
    if (!this.smallScreen) {
      this.sidenav.mode = "side"
      this.sidenav.opened = true
    } else {
      this.sidenav.mode = 'over'
      this.sidenav.opened = false
    }
  }
}
