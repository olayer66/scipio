import { NgModule } from '@angular/core';
// Modules
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CodemirrorModule } from '@ctrl/ngx-codemirror';
import {MatTreeModule} from '@angular/material/tree';
import {CdkTreeModule} from '@angular/cdk/tree';
import {
    MatButtonModule, MatCardModule, MatChipsModule, MatDatepickerModule,
    MatDialogModule, MatExpansionModule, MatFormFieldModule,
    MatIconModule, MatInputModule,
    MatLineModule, MatListModule, MatMenuModule, MatPaginatorModule, MatSidenavModule, MatTableModule,
    MatTabsModule,
    MatToolbarModule
} from '@angular/material';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { SecureRoutingModule } from './secure-routing.module';
import {PopoverModule} from 'ngx-smart-popover';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {TranslateModule} from '@ngx-translate/core';
// Service
import { SecureService } from './secure.service';
import {CodemirrorService} from './codemirror/codemirror.service';
import {SubmissionsService} from "./submissions/submissions.service";
import {APIService} from './api.service';
// Components
import { CodemirrorComponent } from './codemirror/codemirror.component';
import { SecureComponent } from './secure.component';
import { SaveDialogComponent } from './save-dialog/save-dialog.component';
import {SubmissionsComponent} from "./submissions/submissions.component";
import { AssignmentsComponent } from './assignments/assignments.component';
import { UpgradeAssignmentComponent } from './assignments/upgrade-assignment/upgrade-assignment.component';
import {FlexModule} from "@angular/flex-layout";
import { UploadSubmissionComponent } from './assignments/upload-submission/upload-submission.component';
import {FileUploadModule} from "ng2-file-upload";
import { commentLineComponent} from './codemirror/comment-modal/comment-line.component';
import {DragDropModule} from "@angular/cdk/drag-drop";
import { SubjectsComponent } from './subjects/subjects.component';
import { EditTeamComponent } from './subjects/edit-team/edit-team.component';
import { EditSubjectComponent } from './subjects/edit-subject/edit-subject.component';
import {MatSelectModule} from "@angular/material/select";
import {MatSortModule} from "@angular/material/sort";
import {MatCheckboxModule} from "@angular/material/checkbox";
import { AdminUsersComponent } from './admin-users/admin-users.component';
import { AdminCodesComponent } from './admin-codes/admin-codes.component';
import { UserModalComponent } from './admin-users/user-modal/user-modal.component';
import { CodeModalComponent } from './admin-codes/code-modal/code-modal.component';
import { ChangePasswordComponent } from './admin-users/change-password/change-password.component';
import { EditionModalComponent } from './admin-codes/edition-modal/edition-modal.component';
import { CreateUserComponent } from './admin-users/create-user/create-user.component';
import { ProfileComponent } from './profile/profile.component';
import {DisableControlDirective} from './profile/disable-control-directive.directive';
import { ChangeProfilePwdComponent } from './profile/change-profile-pwd/change-profile-pwd.component';


@NgModule({
    imports: [
        CommonModule,
        MatTreeModule,
        CdkTreeModule,
        MatIconModule,
        CodemirrorModule,
        FormsModule,
        ReactiveFormsModule,
        NgxSmartModalModule.forChild(),
        SecureRoutingModule,
        PopoverModule,
        NgbModule,
        MatButtonModule,
        TranslateModule,
        MatDialogModule,
        MatTabsModule,
        MatToolbarModule,
        MatLineModule,
        MatListModule,
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,
        MatDatepickerModule,
        FlexModule,
        MatChipsModule,
        FileUploadModule,
        MatSidenavModule,
        MatMenuModule,
        DragDropModule,
        MatTableModule,
        MatPaginatorModule,
        MatExpansionModule,
        MatSelectModule,
        MatSortModule,
        MatCheckboxModule
    ],
    declarations: [CodemirrorComponent, SecureComponent, SaveDialogComponent, SubmissionsComponent, AssignmentsComponent, UpgradeAssignmentComponent, UploadSubmissionComponent, commentLineComponent, SubjectsComponent, EditTeamComponent, EditSubjectComponent, AdminUsersComponent, AdminCodesComponent, UserModalComponent, CodeModalComponent, ChangePasswordComponent, EditionModalComponent, CreateUserComponent, ProfileComponent, DisableControlDirective, DisableControlDirective, ChangeProfilePwdComponent],
  exports: [CodemirrorComponent,SubmissionsComponent],
  providers: [SecureService, CodemirrorService , APIService,SubmissionsService],
  bootstrap: [CodemirrorComponent,  SecureComponent,SubmissionsComponent]
})
export class SecureModule { }
