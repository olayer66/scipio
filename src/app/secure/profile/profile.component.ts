import {AfterViewInit, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ProfileService} from "./profile.service";
import {TranslateService} from "@ngx-translate/core";
import {NotificationService} from "../../notification.service";
import {Globals} from "../../globals";
import {MediaMatcher} from "@angular/cdk/layout";
import {MatIconRegistry} from "@angular/material/icon";
import {DomSanitizer} from "@angular/platform-browser";
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import {User} from "../interfaces.model";
import {ActivatedRoute} from "@angular/router";
import {Location} from "@angular/common";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {APIService} from "../../public/api.service";
import {UsernameUnique} from "../../validators/username-unique.validator";
import {EmailUnique} from "../../validators/email-unique.validator";
import {ChangePasswordComponent} from "../admin-users/change-password/change-password.component";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, AfterViewInit {
  private smallScreen: boolean;
   mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;

  profile:User;
  private params: any;
   profileForm: FormGroup;
   submitted: boolean;
   active:boolean=false;
  isLoaded:boolean=false;
  show:boolean=false;

  usernameChange:boolean=false;
  emailChange:boolean=false;

  constructor(private prs: ProfileService,
              private route: ActivatedRoute,
              private translate: TranslateService,
              private ns: NotificationService,
              private location: Location,
              private gb:Globals,
              private formBuilder: FormBuilder,
              private api:APIService,
              private dialog: MatDialog,
              changeDetectorRef: ChangeDetectorRef,
              media: MediaMatcher,
              iconRegistry: MatIconRegistry,
              sanitizer: DomSanitizer) {
    this.smallScreen = window.innerWidth < gb.minWidth;
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);

    this.route.queryParams.subscribe(params => {
      this.params=params;
    });
    this.prs.getProfile(this.params.user_id).subscribe(resp=>{
      this.profile=resp;
      this.isLoaded=true;
      this.ngAfterViewInit();
    },error => {
      this.ns.error(false,error.error.error,error.error.code);
      this.location.back();
    })
  }

  ngOnInit() {
  }
  ngAfterViewInit(): void {
    if (this.isLoaded) {
      this.profileForm = this.formBuilder.group({
        name: [{'value':this.profile.name,'disabled':!this.active}, Validators.required],
        surnames: [{'value':this.profile.surnames,'disabled':!this.active}, Validators.required],
        email: [{'value':this.profile.email,'disabled':!this.active}, [Validators.required, Validators.email]],
        username: [{'value':this.profile.user_name,'disabled':!this.active}, [Validators.required, Validators.minLength(6)]],
      }, {
        validators: [
          EmailUnique('email', this.api),
          UsernameUnique('username', this.api)
        ]
      });
      this.show=true;
    }
  }
  activeEdit(){
    this.active=true;
  }
  cancelEdit(){
    this.active=false;
  }
  send(){
    this.submitted=true;
    if(!this.profileForm.invalid) {
      this.profile.name=this.profileForm.value.name;
      this.profile.surnames=this.profileForm.value.surnames;
      this.profile.email=this.profileForm.value.email;
      this.profile.user_name=this.profileForm.value.username;
      this.prs.updateUser(this.profile).subscribe(resp=>{
        this.ns.success(true, 'user_updated', 'success');
      },error => {
        this.ns.error(false,error.error.error,error.error.code);
      });
    }else{
      if(!this.emailChange && !this.usernameChange){
        this.profile.name=this.profileForm.value.name;
        this.profile.surnames=this.profileForm.value.surnames;
        this.profile.email=this.profileForm.value.email;
        this.profile.user_name=this.profileForm.value.username;
        this.prs.updateUser(this.profile).subscribe(resp=>{
          this.ns.success(true, 'user_updated', 'success');
        },error => {
          this.ns.error(false,error.error.error,error.error.code);
        });
      }
    }
  }
  changePassword(){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    const dialogRefUpload = this.dialog.open(ChangePasswordComponent, dialogConfig);
    dialogRefUpload.afterClosed().subscribe(password => {
      if (password) {
        this.prs.changePassword(this.profile.user_id, password).subscribe(
            () => {
              this.ns.success(true, 'user_pwd_changed', 'success');
            }, error => {
              this.ns.error(false, error.error.error, error.error.code);
            }
        );
      }
    });
  }

  usernameChanged(){
    if(this.profile.user_name!==this.profileForm.value.username)
      this.usernameChange=true;
    else
      this.usernameChange=false;
  }
  emailChanged(){
    if(this.profile.email!==this.profileForm.value.email)
      this.emailChange=true;
    else
      this.emailChange=false;
  }
}
