import { Injectable } from '@angular/core';
import {APIService} from "../api.service";
import {User} from "../interfaces.model";

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  constructor(private api:APIService) {

  }
  getProfile(userId:number){
    return this.api.getProfile(userId);
  }
  updateUser(user:User){
    return this.api.updateUser(user);
  }
  changePassword(userId,password){
    return this.api.changePassword(userId, password);
  }
}
