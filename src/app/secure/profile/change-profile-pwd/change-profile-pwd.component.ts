import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MatDialogRef} from "@angular/material/dialog";
import {MatchValue} from "../../../validators/match-value.validator";
import {ProfileComponent} from "../profile.component";

@Component({
  selector: 'app-change-profile-pwd',
  templateUrl: './change-profile-pwd.component.html',
  styleUrls: ['./change-profile-pwd.component.scss']
})
export class ChangeProfilePwdComponent implements OnInit {
  userForm: FormGroup;
  submitted = false;
  constructor(private dialogRef: MatDialogRef<ProfileComponent>, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.userForm = this.formBuilder.group({
      password: ['', [Validators.required, Validators.pattern('^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s).*$'), Validators.minLength(8)]],
      confirmPassword: ['', [Validators.required, Validators.pattern('^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s).*$'), Validators.minLength(8)]],
    }, {
      validators: [ MatchValue('password', 'confirmPassword')]
    });
  }
  send() {
    this.submitted = true;
    if (!this.userForm.invalid) {
      this.dialogRef.close(this.userForm.value.password);
    }
  }
  close() {
    this.dialogRef.close();
  }

}
