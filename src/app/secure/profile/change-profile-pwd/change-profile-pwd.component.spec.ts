import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeProfilePwdComponent } from './change-profile-pwd.component';

describe('ChangeProfilePwdComponent', () => {
  let component: ChangeProfilePwdComponent;
  let fixture: ComponentFixture<ChangeProfilePwdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeProfilePwdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeProfilePwdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
