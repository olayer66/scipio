import {Submission} from "../interfaces.model";

export class SubmissionsBody{
    userId:number;
    userType:string;
    assignmentId:number;
    teamId?:number;
}
export class CorrectionInfo extends Array{
    file_name: string;
    num_comments: number;
    sum(key) {
        return this.reduce((a, b) => a + (b[key] || 0), 0);
    }
}
export interface CountCorrectionInfo {
    num_files:number;
    num_comments:number;
}

export interface SendCorrectionBody {
    subjectId:number;
    assignmentId:number;
    submissionId:number;
    teamId:number;
    assignmentName:string;
    langId:string;
}
