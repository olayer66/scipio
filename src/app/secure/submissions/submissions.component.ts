import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {CorrectionInfo, CountCorrectionInfo, SendCorrectionBody, SubmissionsBody} from "./submissionsTemplates";
import {ActivatedRoute} from "@angular/router";
import {APIService} from "../api.service";
import {Globals} from "../../globals";
import {MatDialog, MatIconRegistry} from "@angular/material";
import {DomSanitizer} from "@angular/platform-browser";
import {SubmissionsService} from "./submissions.service";
import {MediaMatcher} from "@angular/cdk/layout";
import {SecureService} from "../secure.service";

@Component({
    selector: 'app-submissions',
    templateUrl: './submissions.component.html',
    styleUrls: ['./submissions.component.scss']
})

export class SubmissionsComponent implements OnInit {
     params: any;
     corData:any={};
     userType: string;
     smallScreen: boolean=false;
      mobileQuery: MediaQueryList;
     readonly _mobileQueryListener: () => void;
     displayedColumns: string[] = ['file_name', 'num_comments'];
     correctionsInfo:CorrectionInfo[][];
     countCorrectionsInfo:CountCorrectionInfo[][];
    constructor(private sus:SubmissionsService,private route: ActivatedRoute, private api: APIService, private gb:Globals,iconRegistry: MatIconRegistry, sanitizer: DomSanitizer,private dialog: MatDialog,changeDetectorRef: ChangeDetectorRef, media: MediaMatcher) {
        iconRegistry.addSvgIcon('file-document-box-search-outline', sanitizer.bypassSecurityTrustResourceUrl('../../assets/images/svg/file-document-box-search-outline.svg'));
        iconRegistry.addSvgIcon('file-document-edit-outline', sanitizer.bypassSecurityTrustResourceUrl('../../assets/images/svg/file-document-edit-outline.svg'));
        iconRegistry.addSvgIcon('send-submission', sanitizer.bypassSecurityTrustResourceUrl('../../assets/images/svg/send-submission.svg'));
        iconRegistry.addSvgIcon('pdf-box', sanitizer.bypassSecurityTrustResourceUrl('../../assets/images/svg/pdf-box.svg'));
        this.smallScreen = window.innerWidth < gb.minWidth;

        this.mobileQuery = media.matchMedia('(max-width: 600px)');
        this._mobileQueryListener = () => changeDetectorRef.detectChanges();
        this.mobileQuery.addListener(this._mobileQueryListener);

        this.route.queryParams.subscribe(params => {
            this.params=params;
            this.sus.getQueryParams(params);
        });
    }

    ngOnInit() {
        this.userType=this.gb.userTypes[this.params.user_type];
        let subPet:SubmissionsBody={
            userId:this.params.user_id,
            userType:this.userType,
            assignmentId:this.params.assignmentId,
            teamId:this.params.teamId
        };
        console.log(subPet)
        this.api.getSubmissionsList(subPet).subscribe(data=>{
            this.corData=data;
            this.correctionsInfo=[];
            this.countCorrectionsInfo=[];
            this.api.countComments(Object.keys(this.corData.corrections)).subscribe(resp=>{
                this.correctionsInfo=resp;
                Object.keys(this.correctionsInfo).forEach(key=>{
                    this.countCorrectionsInfo[key]={
                        num_comments:this.sumComments(this.correctionsInfo[key]),
                        num_files:Object.keys(this.correctionsInfo[key]).length
                    }
                });
                console.log(this.corData);
            });
        });
    }
    sumComments(comments:CorrectionInfo[]):number{
        let sum:number=0;
        if(comments.length)
            comments.forEach(cm=>{
                sum+=cm.num_comments;
            });
        return sum;
    }
    SendCorrection(correctionId,teamId,submissionId,pos){
        let body:SendCorrectionBody={
            subjectId:this.params.subjectId,
            assignmentId:this.params.assignmentId,
            submissionId:submissionId,
            teamId:teamId,
            assignmentName:this.params.assignmentName,
            langId:'es'
        };
        this.api.SendCorrection(correctionId,body).subscribe(data=>{
            //this.corData.corrections[submissionId][pos].doc_path=data.doc_path;
            this.corData.corrections[submissionId][pos].state=2;
        });
        //Estado a 2
        //Generar informe pdf
    }
    showPDF(path){
        this.api.showPdf(path).subscribe(response => {
            const file = new Blob([response], {type: 'application/pdf'});
            const fileURL = window.URL.createObjectURL(file);
            if (window.navigator.msSaveOrOpenBlob) {
                window.navigator.msSaveOrOpenBlob(file, fileURL.split(':')[1] + '.pdf');
            } else {
                window.open(fileURL, "_blank", 'location=yes,width=500,height=500,scrollbars=no');
            }
        });
    }
    changeState(correctionId:number,state:number){
        if(state===0) {
            this.api.changeCorrectionState(correctionId, 1).subscribe(resp => {
                console.log(resp);
            });
        }
    }
}
