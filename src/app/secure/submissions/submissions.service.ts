import { Injectable } from '@angular/core';
import {APIService} from "../api.service";
import {Subject} from "rxjs";
import {Globals} from "../../globals";

@Injectable({
  providedIn: 'root'
})
export class SubmissionsService {
  private params: any;
  constructor(private api:APIService,private gb:Globals) {

  }
  getQueryParams(params){
    this.params=params;
  }
}
