import { Injectable } from '@angular/core';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ConfirmationDialogComponent } from './confirmation-dialog.component';
import {TranslateService} from "@ngx-translate/core";

@Injectable()
export class ConfirmationDialogService {

  constructor(private modalService: NgbModal,private translate: TranslateService) { }

  public confirm(title: string, message: string, btnOkText: string, btnCancelText: string, dialogSize: 'sm'|'lg' = 'sm'): Promise<boolean> {
      let msg:string;
      let tt:string;
      let confBtn:string;
      let cancelBtn:string;
      this.translate.get(message, {value: 'world'}).subscribe((res: string) => {
        msg=res;
      });
      this.translate.get(title, {value: 'world'}).subscribe((res: string) => {
        tt=res;
      });
      this.translate.get(btnOkText, {value: 'world'}).subscribe((res: string) => {
        confBtn=res;
      });
      this.translate.get(btnCancelText, {value: 'world'}).subscribe((res: string) => {
        cancelBtn=res;
      });
      const modalRef = this.modalService.open(ConfirmationDialogComponent, { size: dialogSize});
        modalRef.componentInstance.title = tt;
        modalRef.componentInstance.message = msg;
        modalRef.componentInstance.btnOkText = confBtn;
        modalRef.componentInstance.btnCancelText = cancelBtn;

        return modalRef.result;
  }

}
