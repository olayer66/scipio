import { Injectable } from '@angular/core';
import {APIService} from '../api.service';
import {Code, Edition} from '../interfaces.model';

@Injectable({
  providedIn: 'root'
})
export class AdminCodesService {

  constructor(private api:APIService) { }

  getList(){
    return this.api.getCodesList();
  }
  upgradeCode(code:Code){
    return this.api.upgradeCode(code);
  }
  upgradeEdition(edition:Edition){
    return this.api.upgradeEdition(edition);
  }
  changeEditionState(editionId:number){
    return this.api.changeEditionState(editionId);
  }
}
