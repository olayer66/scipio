import {AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {Globals} from "../../globals";
import {MediaMatcher} from "@angular/cdk/layout";
import {MatIconRegistry} from "@angular/material/icon";
import {DomSanitizer} from "@angular/platform-browser";
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {Code, Edition, User} from '../interfaces.model';
import {MatPaginator, MatPaginatorIntl, MatSort, MatTable, MatTableDataSource} from '@angular/material';
import {AdminCodesService} from './admin-codes.service';
import {TranslateService} from '@ngx-translate/core';
import {NotificationService} from '../../notification.service';
import {CodeModalComponent} from './code-modal/code-modal.component';
import {EditionModalComponent} from './edition-modal/edition-modal.component';

@Component({
    selector: 'app-admin-codes',
    templateUrl: './admin-codes.component.html',
    styleUrls: ['./admin-codes.component.scss']
})
export class AdminCodesComponent implements OnInit,AfterViewInit {

    private smallScreen: boolean;
     mobileQuery: MediaQueryList;
    private _mobileQueryListener: () => void;
    //List
    codes: Code[] = [];
    editions:Edition[] = [];
    //Tables
    codeDisplayedColumns: string[] = ['cod_gea','abbr_code','name', 'actions'];
    editionDisplayedColumns: string[] = ['edition','actual', 'actions'];
    codDataSource: MatTableDataSource<Code>;
    ediDataSource: MatTableDataSource<Edition>;
    // Tabs
    selectedTab = -1;
    // paginator
    @ViewChild('codPaginator', {static: true}) codPaginator: MatPaginator;
    @ViewChild('ediPaginator', {static: true}) ediPaginator: MatPaginator;
    // sorting
    @ViewChild('codTableSort', {static: true}) codSort: MatSort;
    @ViewChild('ediTableSort', {static: true}) ediSort: MatSort;
    // tables
    @ViewChild('codTable', {static: true}) codTable: MatTable<User>;
    @ViewChild('ediTable', {static: true}) ediTable: MatTable<User>;

    private isLoaded = false;

    constructor(private acs:AdminCodesService,
                private translate: TranslateService,
                private ns: NotificationService,
                private gb:Globals,
                changeDetectorRef: ChangeDetectorRef,
                media: MediaMatcher,
                iconRegistry: MatIconRegistry,
                sanitizer: DomSanitizer,
                private dialog: MatDialog) {
        this.smallScreen = window.innerWidth < gb.minWidth;
        this.mobileQuery = media.matchMedia('(max-width: 600px)');
        this._mobileQueryListener = () => changeDetectorRef.detectChanges();
        this.mobileQuery.addListener(this._mobileQueryListener);

        iconRegistry.addSvgIcon('pencil-outline', sanitizer.bypassSecurityTrustResourceUrl('../../assets/images/svg/pencil-outline.svg')); // edit user
        iconRegistry.addSvgIcon('plus', sanitizer.bypassSecurityTrustResourceUrl('../../assets/images/svg/plus.svg')); // edit user

        this.acs.getList().subscribe(list=>{
            this.codes=list.codes;
            this.editions=list.editions;
            this.codDataSource = new MatTableDataSource(list.codes);
            this.ediDataSource = new MatTableDataSource(list.editions);

            this.isLoaded = true;
            this.ngAfterViewInit();
        },error=>{
            this.ns.error(false, error.error.error, error.error.code);
        });

    }

    ngOnInit() {
    }
    ngAfterViewInit(): void {
        if (this.isLoaded) {
            const paginatorIntl = this.setPaginationIntl();
            // codes
            this.codDataSource.paginator = this.codPaginator;
            this.codDataSource.sort = this.codSort;
            // editions
            this.ediDataSource.paginator = this.ediPaginator;
            this.ediDataSource.sort = this.ediSort;
            // i18n
            this.codDataSource.paginator._intl = paginatorIntl;
            this.ediDataSource.paginator._intl = paginatorIntl;
        }
    }
    createCode(){
        const dialogConfig = new MatDialogConfig();
        dialogConfig.autoFocus = true;
        const dialogRefUpload = this.dialog.open(CodeModalComponent, dialogConfig);
        dialogRefUpload.afterClosed().subscribe(code => {
            if (code) {
                this.acs.upgradeCode(code).subscribe(
                    resp => {
                        this.codes.push(resp);
                        this.codDataSource = new MatTableDataSource(this.codes);
                        this.codTable.renderRows();
                        this.codDataSource.paginator = this.codPaginator;
                        this.ns.success(true, 'code_new_success', 'success');
                    }, error => {
                        this.ns.error(false, error.error.error, error.error.code);
                    }
                );
            }
        });
    }
    editCode(row:Code){
        const dialogConfig = new MatDialogConfig();
        dialogConfig.autoFocus = true;
        dialogConfig.data=row;
        const dialogRefUpload = this.dialog.open(CodeModalComponent, dialogConfig);
        dialogRefUpload.afterClosed().subscribe(code => {
            if (code) {
                this.acs.upgradeCode(code).subscribe(
                    resp => {
                        let index= this.codes.findIndex(code=>code.code_id===resp.code_id);
                        this.codes[index]=resp;
                        this.codDataSource = new MatTableDataSource(this.codes);
                        this.codTable.renderRows();
                        this.ns.success(true, 'code_edit_success', 'success');
                    }, error => {
                        this.ns.error(false, error.error.error, error.error.code);
                    }
                );
            }
        });
    }
    createEdition(){
        const dialogConfig = new MatDialogConfig();
        dialogConfig.autoFocus = true;
        const dialogRefUpload = this.dialog.open(EditionModalComponent, dialogConfig);
        dialogRefUpload.afterClosed().subscribe(edition => {
            if (edition) {
                this.acs.upgradeEdition(edition).subscribe(
                    resp => {
                        this.editions.push(resp);
                        this.ediDataSource = new MatTableDataSource(this.editions);
                        this.ediTable.renderRows();
                        this.ediDataSource.paginator = this.ediPaginator;
                        this.ns.success(true, 'edition_new_success', 'success');
                    }, error => {
                        this.ns.error(false, error.error.error, error.error.code);
                    }
                );
            }
        });
    }
    editEdition(row:Edition){
        const dialogConfig = new MatDialogConfig();
        dialogConfig.autoFocus = true;
        dialogConfig.data=row;
        const dialogRefUpload = this.dialog.open(EditionModalComponent, dialogConfig);
        dialogRefUpload.afterClosed().subscribe(edition => {
            if (edition) {
                this.acs.upgradeEdition(edition).subscribe(
                    resp => {
                        let index= this.editions.findIndex(edition=>edition.edition_id===resp.edition_id);
                        this.editions[index]=resp;
                        this.ediDataSource = new MatTableDataSource(this.editions);
                        this.ediTable.renderRows();
                        this.ns.success(true, 'edition_edit_success', 'success');
                    }, error => {
                        this.ns.error(false, error.error.error, error.error.code);
                    }
                );
            }
        });
    }
    changeEditionState(editionId:number){
        this.acs.changeEditionState(editionId).subscribe(resp=>{
            this.ns.success(true, 'edition_change_state_success', 'success');
        },error=>{
            this.ns.error(false, error.error.error, error.error.code);
        });
    }
    /*-------------------------------------------------FILTERS--------------------------------------------------------*/
    codApplyFilter(filterValue: string) {
        this.codDataSource.filter = filterValue.trim().toLowerCase();

        if (this.codDataSource.paginator) {
            this.codDataSource.paginator.firstPage();
        }
    }
    ediApplyFilter(filterValue: string) {
        this.ediDataSource.filter = filterValue.trim().toLowerCase();

        if (this.ediDataSource.paginator) {
            this.ediDataSource.paginator.firstPage();
        }
    }
    /*----------------------------------------------------------------------------------------------------------------*/
    setPaginationIntl(): MatPaginatorIntl {
        const paginatorIntl = new MatPaginatorIntl();
        this.translate.get('students_per_page', {value: 'world'}).subscribe((res: string) => {
            paginatorIntl.itemsPerPageLabel = res;
        });
        if (!this.translate.currentLang || this.translate.currentLang === 'es') {
            console.log('paso por aqui');
            this.translate.get('next_page', {value: 'world'}).subscribe((res: string) => {
                paginatorIntl.nextPageLabel = res;
            });
            this.translate.get('previous_page', {value: 'world'}).subscribe((res: string) => {
                paginatorIntl.previousPageLabel = res;
            });
            this.translate.get('first_page', {value: 'world'}).subscribe((res: string) => {
                paginatorIntl.firstPageLabel = res;
            });
            this.translate.get('last_page', {value: 'world'}).subscribe((res: string) => {
                paginatorIntl.lastPageLabel = res;
            });
            paginatorIntl.getRangeLabel = this.gb.spanishRangeLabel;
        }

        return paginatorIntl;
    }
}
