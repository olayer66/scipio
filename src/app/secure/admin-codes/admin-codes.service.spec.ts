import { TestBed } from '@angular/core/testing';

import { AdminCodesService } from './admin-codes.service';

describe('AdminCodesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdminCodesService = TestBed.get(AdminCodesService);
    expect(service).toBeTruthy();
  });
});
