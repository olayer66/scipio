import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {AdminCodesComponent} from '../admin-codes.component';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {APIService} from '../../../public/api.service';
import {Edition} from '../../interfaces.model';
import {CodeUnique} from '../../../validators/code-unique.validator';
import {EditionUnique} from '../../../validators/edition-unique.validator';

@Component({
  selector: 'app-edition-modal',
  templateUrl: './edition-modal.component.html',
  styleUrls: ['./edition-modal.component.scss']
})
export class EditionModalComponent implements OnInit {
  edition:Edition;
  editionForm: FormGroup;
  submitted=false;
  private editionPattern=/([12]\d{3}\/[12]\d{3})/;
  constructor(private dialogRef: MatDialogRef<AdminCodesComponent>,private formBuilder: FormBuilder,private api:APIService,@Inject(MAT_DIALOG_DATA) data) {
    if(data)
      this.edition=data;
    else
      this.edition={edition:'',actual:false};
  }

  ngOnInit() {
    this.editionForm = this.formBuilder.group({
      edi:[this.edition.edition, [Validators.required,Validators.pattern(this.editionPattern)]],
    }, {
      validators:[
        EditionUnique('edi',this.api)
      ]
    });
  }
  send(){
    this.submitted=true;
    if(!this.editionForm.invalid) {
      this.edition.edition=this.editionForm.value.edi;
      this.dialogRef.close(this.edition);
    }
  }
  close() {
    this.dialogRef.close();
  }
}
