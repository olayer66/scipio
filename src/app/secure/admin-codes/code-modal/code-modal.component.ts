import {Component, Inject, OnInit} from '@angular/core';
import {Code} from '../../interfaces.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {APIService} from '../../../public/api.service';
import {AdminCodesComponent} from '../admin-codes.component';
import {CodeUnique} from '../../../validators/code-unique.validator';

@Component({
  selector: 'app-code-modal',
  templateUrl: './code-modal.component.html',
  styleUrls: ['./code-modal.component.scss']
})
export class CodeModalComponent implements OnInit {
  code:Code;
  codeForm: FormGroup;
  submitted=false;
  constructor(private dialogRef: MatDialogRef<AdminCodesComponent>,private formBuilder: FormBuilder,private api:APIService,@Inject(MAT_DIALOG_DATA) data) {
    if(data)
      this.code=data;
    else
      this.code={cod_gea:0,abbr_code:'',name:''};
  }

  ngOnInit() {
    this.codeForm = this.formBuilder.group({
      name:[this.code.name, [Validators.required, Validators.minLength(8)]],
      codGea:[this.code.cod_gea, Validators.required],
      abbrCode:[this.code.abbr_code,[Validators.required, Validators.minLength(2)]],
    }, {
      validators:[
        CodeUnique('codGea',this.api)
      ]
    });
  }
  send(){
    this.submitted=true;
    if(!this.codeForm.invalid) {
      this.code.name=this.codeForm.value.name;
      this.code.abbr_code=this.codeForm.value.abbrCode;
      this.code.cod_gea=this.codeForm.value.codGea;
      this.dialogRef.close(this.code);
    }
  }
  close() {
    this.dialogRef.close();
  }
}
