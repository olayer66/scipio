import { Injectable } from '@angular/core';
import {APIService} from "../api.service";
import {Globals} from "../../globals";
import {Subject, User} from "../interfaces.model";
import {UpgradeSubject} from "./subjectsTemplates";

@Injectable({
  providedIn: 'root'
})
export class SubjectsService {
  private params: any;
  constructor(private api:APIService,private gb:Globals) {

  }
  getQueryParams(params){
    this.params=params;
  }
  upgradeSubject(data:UpgradeSubject){
    return this.api.upgradeSubject(data);
  }
  changeSubjectState(subjectId:number){
    return this.api.changeSubjectState(subjectId);
  }
  upgradeTeam(subjectId:number,teamId:number,teamInfo:User[]){
    return this.api.upgradeTeam({'subjectId':subjectId,'teamId':teamId,'teamInfo':teamInfo});
  }
  deleteTeam(teamId){
    return this.api.deleteTeam(teamId);
  }
  getStudentList(subjectId:number){
    return this.api.getStudentsList(subjectId);
  }
  getProfessorsList(subjectId:number){
    return this.api.getProfessorsList(subjectId);
  }
}
