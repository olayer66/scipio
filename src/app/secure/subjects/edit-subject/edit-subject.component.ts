import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormBuilder,FormControl, FormGroup, Validators} from "@angular/forms";
import {Code, Edition, Subject, User} from "../../interfaces.model";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {SubjectsComponent} from "../subjects.component";
import {MatPaginator, MatPaginatorIntl} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {MatTable, MatTableDataSource} from "@angular/material/table";
import {SelectionModel} from "@angular/cdk/collections";
import {MatIconRegistry} from "@angular/material/icon";
import {DomSanitizer} from "@angular/platform-browser";
import {TranslateService} from "@ngx-translate/core";
import {Globals} from "../../../globals";

@Component({
    selector: 'app-edit-subject',
    templateUrl: './edit-subject.component.html',
    styleUrls: ['./edit-subject.component.scss']
})

export class EditSubjectComponent implements OnInit {
     form: FormGroup;
     subject: Subject;
     editions: Edition[];
     periods:string[];
     isNew:boolean=false;
     saveActive:boolean=true;
     profAssoc:User[];
     professorsList:User[];
     codes: Code[];
     displayedColumns: string[] = ['select', 'surnames', 'name', 'email'];
     prlDataSource: MatTableDataSource<User>;
     palDataSource: MatTableDataSource<User>;
     palSelection = new SelectionModel<User>(true, []);
     prlSelection = new SelectionModel<User>(true, []);
    @ViewChild('prlPaginator', {static: true}) prlPaginator: MatPaginator;
    @ViewChild('palPaginator', {static: true}) palPaginator: MatPaginator;
    @ViewChild('prlTableSort', {static: true}) prlSort: MatSort;
    @ViewChild('palTableSort', {static: true}) palSort: MatSort;
    @ViewChild('palTable', {static: true}) palTable: MatTable<User>;
    @ViewChild('prlTable', {static: true}) prlTable: MatTable<User>;

    constructor( private fb: FormBuilder,
                 private gb:Globals,
                 private translate: TranslateService,
                 private dialogRef: MatDialogRef<SubjectsComponent>,
                 private iconRegistry: MatIconRegistry,
                 private sanitizer: DomSanitizer,
                 @Inject(MAT_DIALOG_DATA) data) {
        iconRegistry.addSvgIcon('arrow-right-thick', sanitizer.bypassSecurityTrustResourceUrl('../../assets/images/svg/arrow-right-thick.svg'));
        iconRegistry.addSvgIcon('arrow-left-thick', sanitizer.bypassSecurityTrustResourceUrl('../../assets/images/svg/arrow-left-thick.svg'));

        this.professorsList=data.professorsList;
        this.subject=data.subject;
        this.editions=data.editions;
        this.codes=[];
       for(let i=0;i<data.codes.length;i++){
           if(data.codes[i]!==null)
               this.codes.push(data.codes[i]);
       }
        this.profAssoc=data.profAssoc;
        this.periods=['Cuatrimestral','Anual'];
        if(!this.subject.subject_id)
            this.isNew = true;

        this.prlDataSource = new MatTableDataSource(this.professorsList);
        this.palDataSource = new MatTableDataSource(this.profAssoc);
        

        this.form = this.fb.group({
            name:new FormControl(this.subject.name,[Validators.required,Validators.maxLength(10)]),
            editionControl: new FormControl(this.subject.edition, [Validators.required]),
            periodControl: new FormControl(this.subject.period, [Validators.required]),
            codeControl: new FormControl(this.subject.code_id, [Validators.required])
        });
    }
    ngOnInit() {
        const paginatorIntl =  this.setPaginationIntl();
        //students list table
        this.prlDataSource.paginator = this.prlPaginator;
        this.prlDataSource.sort = this.prlSort;
        //usersInfo table
        this.palDataSource.paginator = this.palPaginator;
        this.palDataSource.sort = this.palSort;
        //i18n
        this.palDataSource.paginator._intl=paginatorIntl;
        this.prlDataSource.paginator._intl=paginatorIntl;
    }
    prlApplyFilter(filterValue: string) {
        this.prlDataSource.filter = filterValue.trim().toLowerCase();

        if (this.prlDataSource.paginator) {
            this.prlDataSource.paginator.firstPage();
        }
    }
    palApplyFilter(filterValue: string) {
        this.palDataSource.filter = filterValue.trim().toLowerCase();

        if (this.palDataSource.paginator) {
            this.palDataSource.paginator.firstPage();
        }
    }
    close() {
        this.dialogRef.close();
    }
    save() {
        this.subject.name=this.form.value.name;
        this.subject.period=this.form.value.periodControl;
        this.subject.edition=this.form.value.editionControl;
        this.subject.code_id=this.form.value.codeControl;
        if (!this.form.invalid)
            this.dialogRef.close({'subject':this.subject,'profAssoc':this.profAssoc});
    }

    toProfAssoc($event){
        $event.preventDefault();
        //For each student selected remove from students list and add to profAssoc list (team members)
        this.prlSelection.selected.forEach(student=>{
            this.profAssoc.push(student);
            this.professorsList= this.professorsList.filter(obj=>obj.user_id !== student.user_id);
            this.prlDataSource.data = this.prlDataSource.data.filter(obj=>obj.user_id !== student.user_id);
        });
        this.prlTable.renderRows();
        this.palDataSource = new MatTableDataSource(this.profAssoc);
        this.ngOnInit();
        this.prlSelection.clear();
        this.saveActive = this.profAssoc.length > 0;
    }
    toProfessorsList($event){
        $event.preventDefault();
        this.palSelection.selected.forEach(student=>{
            this.professorsList.push(student);
            this.profAssoc= this.profAssoc.filter(obj=>obj.user_id !== student.user_id);
            this.palDataSource.data = this.palDataSource.data.filter(obj=>obj.user_id !== student.user_id);
        });
        this.prlDataSource = new MatTableDataSource(this.professorsList);
        this.saveActive = this.profAssoc.length > 0;
        this.ngOnInit();
        this.palTable.renderRows();
        this.palSelection.clear();
    }
    //users info list selection
    palIsAllSelected() {
        const numSelected = this.palSelection.selected.length;
        const numRows = this.palDataSource.data.length;
        return numSelected === numRows;

    }
    /** Selects all rows if they are not all selected; otherwise clear selection. */
    palMasterToggle() {
        this.palIsAllSelected() ?
            this.palSelection.clear() :
            this.palDataSource.data.forEach(row => this.palSelection.select(row));
    }
    /** The label for the checkbox on the passed row */
    palCheckboxLabel(row?: User): string {
        if (!row) {
            return `${this.palIsAllSelected() ? 'select' : 'deselect'} all`;
        }
        return `${this.palSelection.isSelected(row) ? 'deselect' : 'select'}`;
    }

    //Students list selection
    prlIsAllSelected() {
        const numSelected = this.palSelection.selected.length;
        const numRows = this.palDataSource.data.length;
        return numSelected === numRows;

    }
    /** The label for the checkbox on the passed row */
    prlCheckboxLabel(row?: User): string {
        if (!row) {
            return `${this.prlIsAllSelected() ? 'select' : 'deselect'} all`;
        }
        return `${this.prlSelection.isSelected(row) ? 'deselect' : 'select'}`;
    }
    setPaginationIntl():MatPaginatorIntl{
        const paginatorIntl = new MatPaginatorIntl();
        this.translate.get('students_per_page', {value: 'world'}).subscribe((res: string) => {
            paginatorIntl.itemsPerPageLabel = res;
        });
        if(!this.translate.currentLang || this.translate.currentLang==='es'){
            console.log('paso por aqui')
            this.translate.get('next_page', {value: 'world'}).subscribe((res: string) => {
                paginatorIntl.nextPageLabel = res;
            });
            this.translate.get('previous_page', {value: 'world'}).subscribe((res: string) => {
                paginatorIntl.previousPageLabel = res
            });
            this.translate.get('first_page', {value: 'world'}).subscribe((res: string) => {
                paginatorIntl.firstPageLabel = res;
            });
            this.translate.get('last_page', {value: 'world'}).subscribe((res: string) => {
                paginatorIntl.lastPageLabel = res;
            });
            paginatorIntl.getRangeLabel= this.gb.spanishRangeLabel;
        }

        return paginatorIntl;
    }
}
