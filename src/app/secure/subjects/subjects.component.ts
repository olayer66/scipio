import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {SubjectsService} from "./subjects.service";
import {ActivatedRoute} from "@angular/router";
import {APIService} from "../api.service";
import {Globals} from "../../globals";
import {MatDialog, MatDialogConfig, MatIconRegistry} from "@angular/material";
import {DomSanitizer} from "@angular/platform-browser";
import {MediaMatcher} from "@angular/cdk/layout";
import {SubjectsData} from "./subjectsTemplates";
import {Subject} from "../interfaces.model";
import {EditSubjectComponent} from "./edit-subject/edit-subject.component";
import {Location} from "@angular/common";
import {NotificationService} from "../../notification.service";
import {TranslateService} from "@ngx-translate/core";
import {EditTeamComponent} from "./edit-team/edit-team.component";
import {ConfirmationDialogService} from "../confirmation-dialog/confirmation-dialog.service";

@Component({
    selector: 'app-subjects',
    templateUrl: './subjects.component.html',
    styleUrls: ['./subjects.component.scss']
})
export class SubjectsComponent implements OnInit {
     selectedTab = -1;
     params: any;
     subjectsData:SubjectsData;
     userType: string;
     smallScreen: boolean=false;
      mobileQuery: MediaQueryList;
     readonly _mobileQueryListener: () => void;
     isLoaded:boolean=false;
     step:number[]=[];
    constructor(private ss:SubjectsService,
                private route: ActivatedRoute,
                private api: APIService,
                private translate: TranslateService,
                private gb:Globals,
                private confirmationDialogService: ConfirmationDialogService,
                private location: Location,
                private ns: NotificationService,
                private iconRegistry: MatIconRegistry,
                private sanitizer: DomSanitizer,
                private dialog: MatDialog,
                private changeDetectorRef: ChangeDetectorRef,
                private media: MediaMatcher) {
        iconRegistry.addSvgIcon('pencil-outline', sanitizer.bypassSecurityTrustResourceUrl('../../assets/images/svg/pencil-outline.svg'));//edit subject
        iconRegistry.addSvgIcon('team-edit', sanitizer.bypassSecurityTrustResourceUrl('../../assets/images/svg/team-edit.svg'));//edit team
        iconRegistry.addSvgIcon('plus', sanitizer.bypassSecurityTrustResourceUrl('../../assets/images/svg/plus.svg'));//add subject
        iconRegistry.addSvgIcon('bell-ring', sanitizer.bypassSecurityTrustResourceUrl('../../assets/images/svg/bell-ring.svg'));//Activate subject
        iconRegistry.addSvgIcon('sleep', sanitizer.bypassSecurityTrustResourceUrl('../../assets/images/svg/sleep.svg'));//Deactivate subject
        iconRegistry.addSvgIcon('delete-outline', sanitizer.bypassSecurityTrustResourceUrl('../../assets/images/svg/delete-outline.svg'));//delete team
        iconRegistry.addSvgIcon('account-multiple-plus-outline', sanitizer.bypassSecurityTrustResourceUrl('../../assets/images/svg/account-multiple-plus-outline.svg'));//add group

        this.smallScreen = window.innerWidth < gb.minWidth;
        this.mobileQuery = media.matchMedia('(max-width: 600px)');
        this._mobileQueryListener = () => changeDetectorRef.detectChanges();
        this.mobileQuery.addListener(this._mobileQueryListener);

        this.route.queryParams.subscribe(params => {
            this.params=params;
            this.ss.getQueryParams(params);
        });
        this.userType=this.gb.userTypes[this.params.user_type];
        this.api.getSubjects({userId:this.params.user_id,userType:this.userType}).subscribe(
            resp=>{
            this.subjectsData=resp;
            this.isLoaded=true;
            if(this.subjectsData.subjects.length>0) {
                for (let i = 0; i < this.subjectsData.subjects.length; i++)
                    this.step[this.subjectsData.subjects[i].code_id] = -1;
            }
        },(error) => {
            this.ns.error(false,error.error.error,error.error.code);
        });
    }
    ngOnInit() {}
    //subjects actions
    upgradeSubject(subjectId?:number,sjPos?:number){
        const dialogConfig = new MatDialogConfig();
        let subject:Subject;
        let profAssoc=[];
        dialogConfig.autoFocus = true;
        if(subjectId){//edit
            subject=this.subjectsData.subjects[sjPos];
        }else{//new
            subject={
                code_id:0,
                name:'',
                period:'',
                edition:this.subjectsData.editions[0].edition,
                active:false
            };
            subjectId=0;
        }
        this.ss.getProfessorsList(subjectId).subscribe(list=>{
            if(subjectId===0){
                profAssoc.push(list.usersList.find(prof => prof.user_id === Number.parseInt(this.params.user_id)));
                list.usersList.splice(list.usersList.findIndex(prof => prof.user_id === Number.parseInt(this.params.user_id)),1);
            }
            dialogConfig.data={'subject':subject,'editions':this.subjectsData.editions,'profAssoc':this.subjectsData.profAssoc[subjectId]?this.subjectsData.profAssoc[subjectId]:profAssoc,'professorsList':list.usersList,'codes':this.subjectsData.codes};
            const dialogRefUpload = this.dialog.open(EditSubjectComponent, dialogConfig);
            dialogRefUpload.afterClosed().subscribe(data => {
                if(data && data.subject){
                    this.ss.upgradeSubject({'subject':data.subject,'profAssoc':data.profAssoc}).subscribe((resp) => {
                        if(subjectId){//Update
                            this.subjectsData.subjects[subjectId]=data.subject;
                            if(subject.edition!==data.subject.edition) { //Change edition
                                this.subjectsData.subjectsByEdition[subject.edition].splice(sjPos, 1);
                                this.subjectsData.subjectsByEdition[data.subject.edition].push(data.subject);
                            } else
                                this.subjectsData.subjectsByEdition[data.subject.edition][sjPos]=data.subject;
                            this.subjectsData.profAssoc[data.subject.subject_id]=resp.profAssoc;
                            this.ns.success(true,'upgrade_subject_msg_success','success');
                        } else {//Insert
                            this.subjectsData.teams[resp.subject.subject_id]=[];
                            this.subjectsData.profAssoc[resp.subject.subject_id]=resp.profAssoc;
                            this.subjectsData.subjects[resp.subject.subject_id]=resp.subject;
                            this.subjectsData.subjectsByEdition[resp.subject.edition].push(resp.subject);
                            if(resp.status)
                                this.ns.success(true,'insert_subject_success','success');
                            else
                                this.ns.warn(true,'insert_subject_warn','warn');
                        }

                    }, (error) => {
                        this.ns.error(false,error.error.error,error.error.code);
                    });
                }
            });
        },error => {
            this.ns.error(false,error.error.error,error.error.code);
        });
    }
    changeStateSubject(subjectId:number,sjPos:number){
        this.ss.changeSubjectState(subjectId).subscribe(resp=>{
            let state:boolean=false;
            if(!this.subjectsData.subjects[sjPos].active)
                state=true;
            this.subjectsData.subjects[sjPos].active=state;
            this.subjectsData.subjectsByEdition[this.subjectsData.subjects[sjPos].edition][sjPos].active=state;
        },error => {
            this.ns.error(false,error.error.error,error.error.code);
        });
    }
    addTeam(subjectId:number,sjPos:number){
        const dialogConfig = new MatDialogConfig();
        dialogConfig.autoFocus = true;
        let teamInfo=[];
        this.ss.getStudentList(subjectId).subscribe(list=>{
            dialogConfig.data={'teamId':0,'teamInfo':teamInfo,'studentsList':list.usersList};
            const dialogRefUpload = this.dialog.open(EditTeamComponent, dialogConfig);
            dialogRefUpload.afterClosed().subscribe(data => {
                if(data && data.teamInfo){
                    this.ss.upgradeTeam(subjectId,0,data.teamInfo).subscribe(resp=>{
                        this.subjectsData.teams[subjectId].push(resp.teamId);
                        this.subjectsData.teamsInfo[resp.teamId]=resp.teamInfo;
                        if(resp.status)
                            this.ns.success(true,'insert_team_success','success');
                        else
                            this.ns.warn(true,'insert_team_warn','warn');
                    },error => {
                        this.ns.error(false,error.error.error,error.error.code);
                    });
                }
            });
        },error => {
            this.ns.error(false,error.error.error,error.error.code);
        });
    }
    //Teams actions
    editTeam(subjectId:number,teamId:number){
        const dialogConfig = new MatDialogConfig();
        dialogConfig.autoFocus = true;
        let teamInfo=this.subjectsData.teamsInfo[teamId];
        this.ss.getStudentList(subjectId).subscribe(list=>{
            dialogConfig.data={'teamId':teamId,'teamInfo':teamInfo,'studentsList':list.usersList};
            const dialogRefUpload = this.dialog.open(EditTeamComponent, dialogConfig);
            dialogRefUpload.afterClosed().subscribe(data => {
                if(data && data.teamInfo){
                    this.ss.upgradeTeam(subjectId,teamId,data.teamInfo).subscribe(resp=>{
                        this.subjectsData.teamsInfo[teamId]=data.teamInfo;
                    },error => {
                        this.ns.error(false,error.error.error,error.error.code);
                    });
                }
            });
        });
    }
    deleteTeam(teamId:number,subjectId:number,sjPos:number){
        this.confirmationDialogService.confirm('remove_modal_title', 'remove_modal_message','remove','cancel')
            .then((confirmed) =>{
                if(confirmed){
                    this.ss.deleteTeam(teamId).subscribe(resp=>{
                        delete this.subjectsData.teamsInfo[teamId];
                        this.subjectsData.teams[subjectId].splice(sjPos,1);
                        this.step[subjectId]=-1;
                    },error => {
                        this.ns.error(false,error.error.error,error.error.code);
                    });
                }
            })
            .catch((error) =>{
                this.ns.error(false,error.message,'Error');
        });
    }
    //Accordion expansion panel
    setStep(index: number,subjectId:number) {
        this.step[subjectId] = index;
    }
    nextStep(subjectId:number) {
        this.step[subjectId]++;
    }
    prevStep(subjectId:number) {
        this.step[subjectId]--;
    }

}
