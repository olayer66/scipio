import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {SubjectsComponent} from "../subjects.component";
import {User} from "../../interfaces.model";
import {MatPaginator,MatPaginatorIntl} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {MatTable, MatTableDataSource} from "@angular/material/table";
import {TranslateService} from "@ngx-translate/core";
import {Globals} from "../../../globals";
import {SelectionModel} from "@angular/cdk/collections";
import {MatIconRegistry} from "@angular/material/icon";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: 'app-edit-team',
  templateUrl: './edit-team.component.html',
  styleUrls: ['./edit-team.component.scss']
})
export class EditTeamComponent implements OnInit {
     isNew:boolean=false;
     saveActive:boolean=true;
     teamInfo:User[];
     teamId: number;
     studentsList:User[];
     displayedColumns: string[] = ['select', 'surnames', 'name', 'email'];
     stlDataSource: MatTableDataSource<User>;
     uilDataSource: MatTableDataSource<User>;
     uilSelection = new SelectionModel<User>(true, []);
     stlSelection = new SelectionModel<User>(true, []);

    @ViewChild('stlPaginator', {static: true}) stlPaginator: MatPaginator;
    @ViewChild('uilPaginator', {static: true}) uilPaginator: MatPaginator;
    @ViewChild('stlTableSort', {static: true}) stlSort: MatSort;
    @ViewChild('uilTableSort', {static: true}) uilSort: MatSort;
    @ViewChild('uilTable', {static: true}) uilTable: MatTable<User>;
    @ViewChild('stlTable', {static: true}) stlTable: MatTable<User>;
    constructor(private translate: TranslateService,
                private gb:Globals,
                private dialogRef: MatDialogRef<SubjectsComponent>,
                private iconRegistry: MatIconRegistry,
                private sanitizer: DomSanitizer,
                @Inject(MAT_DIALOG_DATA) data) {
        iconRegistry.addSvgIcon('arrow-right-thick', sanitizer.bypassSecurityTrustResourceUrl('../../assets/images/svg/arrow-right-thick.svg'));
        iconRegistry.addSvgIcon('arrow-left-thick', sanitizer.bypassSecurityTrustResourceUrl('../../assets/images/svg/arrow-left-thick.svg'));
        this.teamInfo=data.teamInfo;
        this.teamId=data.teamId;
        this.studentsList=data.studentsList;
        this.stlDataSource = new MatTableDataSource(this.studentsList);
        this.uilDataSource = new MatTableDataSource(this.teamInfo);
        if(this.teamId===0){
            this.saveActive=false;
            this.isNew=true;
        }
    }
    ngOnInit() {
        const paginatorIntl =  this.setPaginationIntl();
        //students list table
        this.stlDataSource.paginator = this.stlPaginator;
        this.stlDataSource.sort = this.stlSort;
        //usersInfo table
        this.uilDataSource.paginator = this.uilPaginator;
        this.uilDataSource.sort = this.uilSort;
        //i18n
        this.uilDataSource.paginator._intl=paginatorIntl;
        this.uilDataSource.paginator._intl=paginatorIntl;
    }
    stlApplyFilter(filterValue: string) {
        this.stlDataSource.filter = filterValue.trim().toLowerCase();

        if (this.stlDataSource.paginator) {
            this.stlDataSource.paginator.firstPage();
        }
    }
    uilApplyFilter(filterValue: string) {
        this.uilDataSource.filter = filterValue.trim().toLowerCase();

        if (this.uilDataSource.paginator) {
            this.uilDataSource.paginator.firstPage();
        }
    }
    close() {
        this.dialogRef.close();
    }
    save() {
        this.dialogRef.close({'teamInfo':this.teamInfo});
    }
    toUserInfo(){
        //For each student selected remove from students list and add to teamInfo list (team members)
        this.stlSelection.selected.forEach(student=>{
            this.teamInfo.push(student);
            this.studentsList= this.studentsList.filter(obj=>obj.user_id !== student.user_id);
            this.stlDataSource.data = this.stlDataSource.data.filter(obj=>obj.user_id !== student.user_id);
        });
        this.stlTable.renderRows();
        this.uilDataSource = new MatTableDataSource(this.teamInfo);
        this.ngOnInit();
        this.stlSelection.clear();
        this.saveActive = this.teamInfo.length > 0;
    }
    toStudentList(){
        this.uilSelection.selected.forEach(student=>{
            this.studentsList.push(student);
            this.teamInfo= this.teamInfo.filter(obj=>obj.user_id !== student.user_id);
            this.uilDataSource.data = this.uilDataSource.data.filter(obj=>obj.user_id !== student.user_id);
        });
        this.stlDataSource = new MatTableDataSource(this.studentsList);
        this.saveActive = this.teamInfo.length > 0;
        this.ngOnInit();
        this.uilTable.renderRows();
        this.uilSelection.clear();
    }
    //users info list selection
    uilIsAllSelected() {
        const numSelected = this.uilSelection.selected.length;
        const numRows = this.uilDataSource.data.length;
        return numSelected === numRows;

    }
    /** Selects all rows if they are not all selected; otherwise clear selection. */
    uilMasterToggle() {
        this.uilIsAllSelected() ?
            this.uilSelection.clear() :
            this.uilDataSource.data.forEach(row => this.uilSelection.select(row));
    }
    /** The label for the checkbox on the passed row */
    uilCheckboxLabel(row?: User): string {
        if (!row) {
            return `${this.uilIsAllSelected() ? 'select' : 'deselect'} all`;
        }
        return `${this.uilSelection.isSelected(row) ? 'deselect' : 'select'}`;
    }

    //Students list selection
    stlIsAllSelected() {
        const numSelected = this.uilSelection.selected.length;
        const numRows = this.uilDataSource.data.length;
        return numSelected === numRows;

    }
    /** The label for the checkbox on the passed row */
    stlCheckboxLabel(row?: User): string {
        if (!row) {
            return `${this.stlIsAllSelected() ? 'select' : 'deselect'} all`;
        }
        return `${this.stlSelection.isSelected(row) ? 'deselect' : 'select'}`;
    }
    setPaginationIntl():MatPaginatorIntl{
        const paginatorIntl = new MatPaginatorIntl();
        this.translate.get('students_per_page', {value: 'world'}).subscribe((res: string) => {
            paginatorIntl.itemsPerPageLabel = res;
        });
        if(!this.translate.currentLang || this.translate.currentLang==='es'){
            console.log('paso por aqui')
            this.translate.get('next_page', {value: 'world'}).subscribe((res: string) => {
                paginatorIntl.nextPageLabel = res;
            });
            this.translate.get('previous_page', {value: 'world'}).subscribe((res: string) => {
                paginatorIntl.previousPageLabel = res
            });
            this.translate.get('first_page', {value: 'world'}).subscribe((res: string) => {
                paginatorIntl.firstPageLabel = res;
            });
            this.translate.get('last_page', {value: 'world'}).subscribe((res: string) => {
                paginatorIntl.lastPageLabel = res;
            });
            paginatorIntl.getRangeLabel= this.gb.spanishRangeLabel;
        }

        return paginatorIntl;
    }

}
