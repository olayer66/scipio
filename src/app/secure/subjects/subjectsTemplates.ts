import {Code, Edition, Subject, User} from "../interfaces.model";

export interface SubjectsData {
    subjects:Subject[];
    teams: number[][];
    teamsInfo:User[][];
    codes: Code[];
    editions:Edition[];
    profAssoc:User[][];
    subjectsByEdition:Subject[][];
}
export class TeamData{
    team_id:number;
    members:Array<User>;
}
export interface UpgradeTeamBody{
    subjectId:number;
    teamId:number;
    teamInfo:User[];
}
export interface SubjectsBody  {
    userId:number;
    userType:string;
}
export interface UsersList {
    usersList:User[];
}
export interface UpgradeSubject{
    subject:Subject;
    profAssoc:User[];
}
 export interface UpgradeTeamResp {
    status:boolean;
    teamId:number;
    teamInfo:User[];
 }
export interface UpgradeSubjectResp {
    status:boolean;
    subject:Subject;
    profAssoc:User[];
}
export interface UsersListResponse {
    admins:User[];
    professors:User[];
    students:User[];
}
export interface CodesListResponse {
    codes:Code[];
    editions:Edition[];
}
