import {Injectable} from '@angular/core';
@Injectable({
    providedIn: 'root'
})

export class SecureService {
    private params:any;
    constructor () {}
    getQueryParams(params){
        this.params=params;
    }
    setQueryParams():any{
        return this.params;
    }
}
