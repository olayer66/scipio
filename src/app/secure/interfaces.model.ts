export class CommentedLine{
    readonly correctionLine_id?: number;
    file_name:string;
    line_start: number;
    line_end: number;
    message: string;
    correction_id:number;
    code_modified: boolean;
    original_code: string;
    modified_code: string;
    readonly created_at?: string;
    deleted: boolean;
}
export class Submission{
    readonly submission_id?: number;
    assignment_id:number;
    team_id:number;
    path:string;
    peer_review_team_id:number;
    readonly created_at?: string;
}
export class Correction{
    readonly correction_id?: number;
    submission_id: number;
    reviewer_id:number;
    team_id:number;
    state:boolean;
    doc_path:string;
    readonly created_at?: string;
}
export class Subject{
    readonly subject_id?: number;
    code_id:number;
    name:string;
    period:string;
    edition:string;
    active:boolean;
    readonly created_at?: string;
}
export class Code{
    readonly code_id?: number;
    name:string;
    abbr_code:string;
    cod_gea:number;
    readonly created_at?: string;
}
export class Assignment{
    readonly assignment_id?: number;
    subject_id:number;
    name:string;
    description:string;
    end_date:string;
    peer_review:boolean;
    have_submission:boolean;
    active:boolean;
    readonly created_at?: string;
}
export class User{
    readonly user_id?: number;
    user_type:number;
    user_name:string;
    password:string;
    name:string;
    surnames:string;
    email:string;
    tel_number?:number;
    active:boolean;
    readonly created_at?: string;
}
export class Edition{
    readonly edition_id?: number;
    edition:string;
    actual:boolean;
    readonly created_at?: string;
}
