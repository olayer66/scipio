import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CodemirrorComponent} from "./codemirror/codemirror.component";

import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import {SecureComponent} from "./secure.component";
import {AssignmentsComponent} from "./assignments/assignments.component";
import {SubmissionsComponent} from "./submissions/submissions.component";
import {SubjectsComponent} from "./subjects/subjects.component";
import {AuthGuard} from "./auth.guard";
import {AdminUsersComponent} from "./admin-users/admin-users.component";
import {AdminCodesComponent} from "./admin-codes/admin-codes.component";
import {ProfileComponent} from "./profile/profile.component";

@Injectable()
export class ConfirmDeactivateGuard implements CanDeactivate<CodemirrorComponent> {
    canDeactivate(target: CodemirrorComponent) {
        if (target.hasChanges()) {
            if (confirm(target.hasChangesMessage()))
                target.saveBeforeExit().subscribe(resp=>{
                target.removeCache();
                return true;
            });
            else {//Guardar si por aqui
                return true;
            }
        }
        return true;
    }
}

const routes: Routes = [
            { path: '',  component: SecureComponent, children:[
                    /*{ path: '', pathMatch: 'full', redirectTo: 'subjects', canActivate: [AuthGuard]},*/
                    { path: 'codemirror', component: CodemirrorComponent ,canActivate: [AuthGuard],canDeactivate:[ConfirmDeactivateGuard]},//codemirror viewer
                    { path: 'assignments', component: AssignmentsComponent, canActivate: [AuthGuard]},//Assignments by subject
                    { path: 'submissions', component: SubmissionsComponent, canActivate: [AuthGuard]},//Submission info + corrections
                    { path: 'subjects', component: SubjectsComponent, canActivate: [AuthGuard]},//Subjects + teams
                    { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard]},//User profile
                    { path: 'admin/users', component: AdminUsersComponent, canActivate: [AuthGuard]},//admin users
                    { path: 'admin/subjects', component: AdminCodesComponent, canActivate: [AuthGuard]}//Subjects users
            ]}, // main secure
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers:[ConfirmDeactivateGuard]
})
export class SecureRoutingModule { }
