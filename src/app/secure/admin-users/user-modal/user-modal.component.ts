import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {User} from "../../interfaces.model";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {APIService} from "../../../public/api.service";
import {EmailUnique} from "../../../validators/email-unique.validator";
import {UsernameUnique} from "../../../validators/username-unique.validator";
import {AdminUsersComponent} from "../admin-users.component";

@Component({
  selector: 'app-user-modal',
  templateUrl: './user-modal.component.html',
  styleUrls: ['./user-modal.component.scss']
})
export class UserModalComponent implements OnInit {
  userForm: FormGroup;
  submitted=false;
  pwdChange:boolean=false;
    usernameChange:boolean=false;
    emailChange:boolean=false;
  private user:User;
  constructor(private dialogRef: MatDialogRef<AdminUsersComponent>,private formBuilder: FormBuilder,private api:APIService,@Inject(MAT_DIALOG_DATA) data) {
    this.user=data;
  }

  ngOnInit() {
    this.userForm = this.formBuilder.group({
      name:[this.user.name, Validators.required],
      surnames:[this.user.surnames, Validators.required],
      email:[this.user.email, [Validators.required,Validators.email]],
      username: [this.user.user_name, [Validators.required,Validators.minLength(6)]],
    }, {
      validators:[
        EmailUnique('email',this.api),
        UsernameUnique('username',this.api)
      ]
    });
  }
  send(){
    this.submitted=true;
    if(!this.userForm.invalid) {
      this.user.name=this.userForm.value.name;
      this.user.surnames=this.userForm.value.surnames;
      this.user.email=this.userForm.value.email;
      this.user.user_name=this.userForm.value.username;
      this.dialogRef.close(this.user);
    }else{
       if(!this.emailChange && !this.usernameChange){
           this.user.name=this.userForm.value.name;
           this.user.surnames=this.userForm.value.surnames;
           this.user.email=this.userForm.value.email;
           this.user.user_name=this.userForm.value.username;
           this.dialogRef.close(this.user);
       }
    }
  }
  close() {
    this.dialogRef.close();
  }
  usernameChanged(){
      if(this.user.user_name!==this.userForm.value.username)
        this.usernameChange=true;
      else
          this.usernameChange=false;
  }
    emailChanged(){
      if(this.user.email!==this.userForm.value.email)
        this.emailChange=true;
      else
          this.emailChange=false;
    }
}
