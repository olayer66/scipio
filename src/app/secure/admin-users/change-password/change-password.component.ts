import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from '@angular/material';

import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatchValue} from '../../../validators/match-value.validator';
import {AdminUsersComponent} from '../admin-users.component';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  userForm: FormGroup;
  submitted = false;
  constructor( private dialogRef: MatDialogRef<AdminUsersComponent>, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.userForm = this.formBuilder.group({
      password: ['', [Validators.required, Validators.pattern('^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s).*$'), Validators.minLength(8)]],
      confirmPassword: ['', [Validators.required, Validators.pattern('^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s).*$'), Validators.minLength(8)]],
    }, {
      validators: [ MatchValue('password', 'confirmPassword')]
    });
  }
  send() {
    this.submitted = true;
    if (!this.userForm.invalid) {
      this.dialogRef.close(this.userForm.value.password);
    }
  }
  close() {
    this.dialogRef.close();
  }
}
