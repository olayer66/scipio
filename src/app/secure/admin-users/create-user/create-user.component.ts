import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {User} from '../../interfaces.model';
import {MatDialogRef} from '@angular/material';
import {APIService} from '../../../public/api.service';
import {MatchValue} from '../../../validators/match-value.validator';
import {EmailUnique} from '../../../validators/email-unique.validator';
import {UsernameUnique} from '../../../validators/username-unique.validator';
import {AdminUsersComponent} from '../admin-users.component';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {
  userForm: FormGroup;
  submitted = false;
  private user: User;
  constructor( private dialogRef: MatDialogRef<AdminUsersComponent>, private formBuilder: FormBuilder, private api: APIService) {
    this.user = {
      name: '',
      surnames: '',
      email: '',
      user_name: '',
      password: '',
      user_type: 2,
      active: true
    };
  }

  ngOnInit() {
    this.userForm = this.formBuilder.group({
      name:[this.user.name, Validators.required],
      surnames:[this.user.surnames, Validators.required],
      email:[this.user.email, [Validators.required, Validators.email]],
      username: [this.user.user_name, [Validators.required, Validators.minLength(6)]],
      password: [this.user.password, [Validators.required, Validators.pattern('^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s).*$'),Validators.minLength(8)]],
      confirmPassword: ['', [Validators.required, Validators.pattern('^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s).*$'),Validators.minLength(8)]],
    }, {
      validators: [ MatchValue('password', 'confirmPassword'),
        EmailUnique('email', this.api),
        UsernameUnique('username', this.api)
      ]
    });
  }
  send() {
    this.submitted = true;

    if(!this.userForm.invalid) {
      this.user.name=this.userForm.value.name;
      this.user.surnames=this.userForm.value.surnames;
      this.user.email=this.userForm.value.email;
      this.user.user_name=this.userForm.value.username;
      this.user.password=this.userForm.value.password;
      this.dialogRef.close(this.user);
    }
  }
  close() {
    this.dialogRef.close();
  }

}
