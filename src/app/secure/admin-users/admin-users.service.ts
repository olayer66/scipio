import { Injectable } from '@angular/core';
import {APIService} from "../api.service";
import {User} from "../interfaces.model";

@Injectable({
    providedIn: 'root'
})
export class AdminUsersService {

    constructor(private api:APIService) { }

    getUsersList(){
        return this.api.getUsersList();
    }
    updateUser(user:User){
        return this.api.updateUser(user);
    }
    changePassword(userId,password){
        return this.api.changePassword(userId, password);
    }
    createUser(user: User) {
        return this.api.createUser(user);
    }
    changeUserState(userId:number){
        return this.api.changeUserState(userId);
    }
}
