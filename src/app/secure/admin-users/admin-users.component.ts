import {AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {MediaMatcher} from '@angular/cdk/layout';
import {Globals} from '../../globals';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';
import {User} from '../interfaces.model';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import {MatPaginator, MatPaginatorIntl} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {AdminUsersService} from './admin-users.service';
import {TranslateService} from '@ngx-translate/core';
import {UserModalComponent} from './user-modal/user-modal.component';
import {NotificationService} from '../../notification.service';
import {ChangePasswordComponent} from './change-password/change-password.component';
import {CreateUserComponent} from './create-user/create-user.component';

@Component({
    selector: 'app-admin-users',
    templateUrl: './admin-users.component.html',
    styleUrls: ['./admin-users.component.scss']
})
export class AdminUsersComponent implements OnInit, AfterViewInit {
    private smallScreen: boolean;
     mobileQuery: MediaQueryList;
    private _mobileQueryListener: () => void;
    // List
    professors: User[] = [];
    students: User[] = [];
    admins: User[] = [];
    // Tables
    displayedColumns: string[] = ['surnames', 'name', 'user_name', 'email', 'active', 'actions'];
    prlDataSource: MatTableDataSource<User>;
    stdDataSource: MatTableDataSource<User>;
    admDataSource: MatTableDataSource<User>;
    // Tabs
    selectedTab = -1;
    // paginator
    @ViewChild('prlPaginator', {static: true}) prlPaginator: MatPaginator;
    @ViewChild('stdPaginator', {static: true}) stdPaginator: MatPaginator;
    @ViewChild('admPaginator', {static: true}) admPaginator: MatPaginator;
    // sorting
    @ViewChild('prlTableSort', {static: true}) prlSort: MatSort;
    @ViewChild('stdTableSort', {static: true}) stdSort: MatSort;
    @ViewChild('admTableSort', {static: true}) admSort: MatSort;
    // tables
    @ViewChild('prlTable', {static: true}) prlTable: MatTable<User>;
    @ViewChild('stdTable', {static: true}) stdTable: MatTable<User>;
    @ViewChild('admTable', {static: true}) admTable: MatTable<User>;
    private isLoaded = false;

    constructor(
        private aus: AdminUsersService,
        private gb: Globals,
        private translate: TranslateService,
        private ns: NotificationService,
        changeDetectorRef: ChangeDetectorRef,
        media: MediaMatcher,
        iconRegistry: MatIconRegistry,
        sanitizer: DomSanitizer,
        private dialog: MatDialog) {
        iconRegistry.addSvgIcon('lock-reset', sanitizer.bypassSecurityTrustResourceUrl('../../assets/images/svg/lock-reset.svg')); // edit user
        iconRegistry.addSvgIcon('pencil-outline', sanitizer.bypassSecurityTrustResourceUrl('../../assets/images/svg/pencil-outline.svg')); // edit user
        iconRegistry.addSvgIcon('plus', sanitizer.bypassSecurityTrustResourceUrl('../../assets/images/svg/plus.svg')); // edit user
        this.smallScreen = window.innerWidth < gb.minWidth;
        this.mobileQuery = media.matchMedia('(max-width: 600px)');
        this._mobileQueryListener = () => changeDetectorRef.detectChanges();
        this.mobileQuery.addListener(this._mobileQueryListener);
        this.aus.getUsersList().subscribe(list => {
            this.professors = list.professors;
            this.students = list.students;
            this.admins = list.admins;
            this.prlDataSource = new MatTableDataSource(list.professors);
            this.stdDataSource = new MatTableDataSource(list.students);
            this.admDataSource = new MatTableDataSource(list.admins);
            this.isLoaded = true;
            this.ngAfterViewInit();
        });
    }
    ngOnInit() {}
    ngAfterViewInit(): void {
        if (this.isLoaded) {
            const paginatorIntl = this.setPaginationIntl();
            // professors
            this.prlDataSource.paginator = this.prlPaginator;
            this.prlDataSource.sort = this.prlSort;
            // students
            this.stdDataSource.paginator = this.stdPaginator;
            this.stdDataSource.sort = this.stdSort;
            // admins
            this.admDataSource.paginator = this.admPaginator;
            this.admDataSource.sort = this.admSort;

            // i18n
            this.admDataSource.paginator._intl = paginatorIntl;
            this.prlDataSource.paginator._intl = paginatorIntl;
            this.stdDataSource.paginator._intl = paginatorIntl;
        }
    }

    editUser(type: string, row: User) {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.autoFocus = true;
        dialogConfig.data = row;
        const dialogRefUpload = this.dialog.open(UserModalComponent, dialogConfig);
        dialogRefUpload.afterClosed().subscribe(data => {
            if (data) {
                this.aus.updateUser(data).subscribe(() => {
                    let index;
                    console.log(type);
                    switch (type) {
                        case 'PROFESSORS':
                            index = this.professors.findIndex(prof => prof.user_id === data.user_id);
                            this.professors[index] = data;
                            this.prlDataSource = new MatTableDataSource(this.professors);
                            this.prlTable.renderRows();
                            break;
                        case 'STUDENTS':
                            index = this.students.findIndex(stu => stu.user_id === data.user_id);
                            this.students[index] = data;
                            this.stdDataSource = new MatTableDataSource(this.students);
                            this.stdTable.renderRows();
                            break;
                        case 'ADMINS':
                            index = this.admins.findIndex(adm => adm.user_id === data.user_id);
                            this.admins[index] = data;
                            this.admDataSource = new MatTableDataSource(this.admins);
                            this.admTable.renderRows();
                            break;
                    }
                    this.ns.success(true, 'user_updated', 'success');
                }, error => {
                    this.ns.error(false, error.error.error, error.error.code);
                });
            }
        });

    }
    changePassword(row: User) {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.autoFocus = true;
        const dialogRefUpload = this.dialog.open(ChangePasswordComponent, dialogConfig);
        dialogRefUpload.afterClosed().subscribe(password => {
            if (password) {
                this.aus.changePassword(row.user_id, password).subscribe(
                    () => {
                        this.ns.success(true, 'user_pwd_changed', 'success');
                    }, error => {
                        this.ns.error(false, error.error.error, error.error.code);
                    }
                );
            }
    });
    }
    createUser(type: number) {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.autoFocus = true;
        const dialogRefUpload = this.dialog.open(CreateUserComponent, dialogConfig);
        dialogRefUpload.afterClosed().subscribe(user => {
            if (user) {
                user.user_type = type;
                this.aus.createUser(user).subscribe(resp => {
                    user.user_id = resp.user_id;
                    user.password = '';
                    switch (type) {
                        case 0:
                            this.admins.push(user);
                            this.admDataSource = new MatTableDataSource(this.admins);
                            this.admTable.renderRows();
                            this.admDataSource.paginator = this.admPaginator;
                            break;
                        case 1:
                            this.professors.push(user);
                            this.prlDataSource = new MatTableDataSource(this.professors);
                            this.prlTable.renderRows();
                            this.prlDataSource.paginator = this.prlPaginator;
                            break;
                        case 2:
                            this.students.push(user);
                            this.stdDataSource = new MatTableDataSource(this.students);
                            this.stdTable.renderRows();
                            this.stdDataSource.paginator = this.stdPaginator;
                            break;
                    }
                    this.ns.success(true, 'new_user_success', 'success');
                }, error => {
                    this.ns.error(false, error.error.error, error.error.code);
                });
            }
        });
    }
    changeUserState(userId:number) {
        this.aus.changeUserState(userId).subscribe(resp=>{
                this.ns.success(true, 'user_state_changed', 'success');
            },error=>{
                this.ns.error(false, error.error.error, error.error.code);
            }
        );
    }
    /*-------------------------------------------------FILTERS--------------------------------------------------------*/
    prlApplyFilter(filterValue: string) {
        this.prlDataSource.filter = filterValue.trim().toLowerCase();

        if (this.prlDataSource.paginator) {
            this.prlDataSource.paginator.firstPage();
        }
    }
    stdApplyFilter(filterValue: string) {
        this.stdDataSource.filter = filterValue.trim().toLowerCase();

        if (this.stdDataSource.paginator) {
            this.stdDataSource.paginator.firstPage();
        }
    }
    admApplyFilter(filterValue: string) {
        this.admDataSource.filter = filterValue.trim().toLowerCase();

        if (this.admDataSource.paginator) {
            this.admDataSource.paginator.firstPage();
        }
    }
    /*----------------------------------------------------------------------------------------------------------------*/
    setPaginationIntl(): MatPaginatorIntl {
        const paginatorIntl = new MatPaginatorIntl();
        this.translate.get('students_per_page', {value: 'world'}).subscribe((res: string) => {
            paginatorIntl.itemsPerPageLabel = res;
        });
        if (!this.translate.currentLang || this.translate.currentLang === 'es') {
            console.log('paso por aqui');
            this.translate.get('next_page', {value: 'world'}).subscribe((res: string) => {
                paginatorIntl.nextPageLabel = res;
            });
            this.translate.get('previous_page', {value: 'world'}).subscribe((res: string) => {
                paginatorIntl.previousPageLabel = res;
            });
            this.translate.get('first_page', {value: 'world'}).subscribe((res: string) => {
                paginatorIntl.firstPageLabel = res;
            });
            this.translate.get('last_page', {value: 'world'}).subscribe((res: string) => {
                paginatorIntl.lastPageLabel = res;
            });
            paginatorIntl.getRangeLabel = this.gb.spanishRangeLabel;
        }

        return paginatorIntl;
    }
}
