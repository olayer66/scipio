import {Injectable, NgZone} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Globals } from '../globals';
import {FileNode, FolderTreeBody, FileBody, FileCommentsBody, SaveFileCommentsBody} from './codemirror/codemirrorTemplates';
import {CorrectionInfo, SendCorrectionBody, SubmissionsBody} from './submissions/submissionsTemplates';
import {Observable} from 'rxjs';
import {AssignmentsBody} from './assignments/assignmentsTemplates';
import {Assignment, Code, Edition, Subject} from './interfaces.model';
import {
  CodesListResponse,
  SubjectsBody,
  SubjectsData,
  UpgradeSubject, UpgradeSubjectResp,
  UpgradeTeamBody,
  UpgradeTeamResp,
  UsersList, UsersListResponse
} from './subjects/subjectsTemplates';
import {User} from './interfaces.model';
declare global {
  interface Window {
    RTCPeerConnection: RTCPeerConnection;
    mozRTCPeerConnection: RTCPeerConnection;
    webkitRTCPeerConnection: RTCPeerConnection;
  }
}
@Injectable({
  providedIn: 'root'
})

export class APIService {
  private http: HttpClient;
  private gb: Globals;
  private localIp: any;

  private ipRegex = new RegExp(/([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/);
  private headers: any;

  constructor(private httpClient: HttpClient, private globals: Globals, private zone: NgZone) {
    this.http = httpClient;
    this.gb = globals;
    this.determineLocalIp();
    this.headers = this.gb.headers;
    const user = JSON.parse(sessionStorage.getItem('currentUser'));
    this.headers.token = user.token;
    this.localIp = sessionStorage.getItem('LOCAL_IP');

  }
  private determineLocalIp() {
    window.RTCPeerConnection = this.getRTCPeerConnection();
    const pc = new RTCPeerConnection({iceServers: []});
    pc.createDataChannel('');
    pc.createOffer().then(pc.setLocalDescription.bind(pc));
    pc.onicecandidate = (ice) => {
      this.zone.run(() => {
        if (!ice || !ice.candidate || !ice.candidate.candidate) {
          return;
        }
        this.localIp = 'http://' + this.ipRegex.exec(ice.candidate.candidate)[1] + ':3000/';
        sessionStorage.setItem('LOCAL_IP', this.localIp);
        pc.onicecandidate = () => {};
        pc.close();
      });
    };
  }
  private getRTCPeerConnection() {
    return window.RTCPeerConnection ||
        window.mozRTCPeerConnection ||
        window.webkitRTCPeerConnection;
  }
  // Users
  public getProfile(userId:number){
    return this.http.get<User>(this.gb.serverUrl + 'secure/users/profile/'+userId,{headers: this.headers});
  }
  public changeUserState(userId:number){
    return this.http.patch(this.gb.serverUrl + 'secure/users/admin/state', {'userId':userId}, {headers: this.headers});
  }
  public getStudentsList(subjectId: number) {
    return this.http.get<UsersList>(this.gb.serverUrl + 'secure/users/students/' + subjectId, {headers: this.headers});
  }
  public getProfessorsList(subjectId: number) {
    return this.http.get<UsersList>(this.gb.serverUrl + 'secure/users/professors/' + subjectId, {headers: this.headers});
  }
  public getUsersList() {
    return this.http.get<UsersListResponse>(this.gb.serverUrl + 'secure/users/admin/list', {headers: this.headers});
  }
  public updateUser(user: User) {
    return this.http.patch(this.gb.serverUrl + 'secure/users/update/user', user, {headers: this.headers});
  }
  public changePassword(userId, password) {
    return this.http.patch(this.gb.serverUrl + 'secure/users/update/password', {'userId': userId, 'password': password}, {headers: this.headers});
  }
  public createUser(user: User) {
    return this.http.put<{user_id: string}>(this.gb.serverUrl + 'secure/users/admin/create', user, {headers: this.gb.headers});
  }
  // Subjects
  public getSubjects(pet: SubjectsBody) {
    return this.http.post<SubjectsData>(this.gb.serverUrl + 'secure/subjects', pet, {headers: this.headers});
  }
  public upgradeSubject(data: UpgradeSubject) {
    return this.http.patch<UpgradeSubjectResp>(this.gb.serverUrl + 'secure/subjects/upgrade', data, {headers: this.headers});
  }
  public changeSubjectState(subjectId: number) {
    return this.http.patch(this.gb.serverUrl + 'secure/subjects/state', {'subjectId': subjectId}, {headers: this.headers});
  }
  public getCodesList() {
    return this.http.get<CodesListResponse>(this.gb.serverUrl + 'secure/subjects/admin/list', {headers: this.headers});
  }
  public upgradeCode(code:Code){
    return this.http.patch<Code>(this.gb.serverUrl + 'secure/subjects/admin/upgrade/code', code, {headers: this.headers});
  }
  public upgradeEdition(edition:Edition){
    return this.http.patch<Edition>(this.gb.serverUrl + 'secure/subjects/admin/upgrade/edition', edition, {headers: this.headers});
  }
  public changeEditionState(editionId:number){
    return this.http.patch(this.gb.serverUrl + 'secure/subjects/admin/edition/state', {'editionId':editionId}, {headers: this.headers});
  }
  // Teams
  public upgradeTeam(pet: UpgradeTeamBody) {
    return this.http.put<UpgradeTeamResp>(this.gb.serverUrl + 'secure/teams/upgrade', pet, {headers: this.headers});
  }
  public deleteTeam(teamId: number) {
    return this.http.delete(this.gb.serverUrl + 'secure/teams/delete/' + teamId, {headers: this.headers});
  }

  // Assignments
  public getAssignments(pet: AssignmentsBody): Observable<any> {
    return this.http.post<AssignmentsBody>(this.gb.serverUrl + 'secure/assignments', pet, {headers: this.headers});
  }
  public saveAssignment(pet: Assignment) {
    return this.http.put<Assignment>(this.gb.serverUrl + 'secure/assignments/save', pet, {headers: this.headers});
  }
  public removeAssignment(pet: number) {
    return this.http.delete<number>(this.gb.serverUrl + 'secure/assignments/remove/' + pet, {headers: this.headers});
  }
  public activateAssignment(pet: Assignment) {
    return this.http.patch(this.gb.serverUrl + 'secure/assignments/activate', pet, {headers: this.headers});
  }
  // Codemirror
  public getFolderTree(pet: FolderTreeBody): Observable<FileNode> {
    return this.http.post<FileNode>(this.gb.serverUrl + 'secure/codemirror/tree', pet, {headers: this.headers});
  }
  public getFile(pet: FileBody): Observable<any> {
    return this.http.post<string>(this.gb.serverUrl + 'secure/codemirror/file', pet, {headers: this.headers});
  }
  public getFileTypes(): Observable<any> {
    const userData = JSON.parse(localStorage.getItem('currentUser'));
    console.log(userData);
    return this.http.get(this.gb.serverUrl + 'public/codemirror/fileTypes', {headers: this.headers});
  }
  public getFileComments(pet: FileCommentsBody): Observable<any> {
    return this.http.post<any>(this.gb.serverUrl + 'secure/codemirror/file/comments', pet, {headers: this.headers});
  }
  public saveFileComments(pet: SaveFileCommentsBody): Observable<any> {
    return this.http.put<SaveFileCommentsBody>(this.gb.serverUrl + 'secure/codemirror/file/comments/save', pet, {headers: this.headers});
  }
  // Submissions
  public getSubmissionsList(pet: SubmissionsBody): Observable<any> {
    return this.http.post<SubmissionsBody>(this.gb.serverUrl + 'secure/submissions', pet, {headers: this.headers});
  }
  // Corrections
  public activatePeerReview(assignmentId: number) {
    return this.http.get(this.gb.serverUrl + 'secure/corrections/peer/activate/' + assignmentId, {headers: this.headers});
  }
  public changeStatePeerReview(assignmentId: number, status: number) {
    return this.http.patch(this.gb.serverUrl + 'secure/corrections/peer/change', {assignmentId: assignmentId, status: status}, {headers: this.headers});
  }
  public SendCorrection(correctionId: number, body: SendCorrectionBody) {
    return this.http.post(this.gb.serverUrl + 'secure/corrections/send/' + correctionId, body, {headers: this.headers});
  }
  public changeCorrectionState(correctionId: number, state: number) {
    return this.http.patch(this.gb.serverUrl + 'secure/corrections/' + correctionId + '/state', {'state': state}, {headers: this.headers});
  }
  public countComments(correctionIds) {
    return this.http.post<CorrectionInfo[][]>(this.gb.serverUrl + 'secure/corrections/comments/count', {correctionIds: correctionIds}, {headers: this.headers});
  }
  public showPdf(path: string) {
    return this.http.post(this.gb.serverUrl + 'secure/corrections/pdf', {'path': path}, {responseType: 'arraybuffer', headers: this.headers});
  }
}
