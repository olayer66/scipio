import { Injectable } from '@angular/core';
import {APIService} from "../api.service";
import {Observable, Subject} from "rxjs";
import {AssignmentsData} from "./assignmentsTemplates";
import {Globals} from "../../globals";
import {Assignment} from "../interfaces.model";
@Injectable({
  providedIn: 'root'
})
export class AssignmentsService {
  private asData = new Subject<AssignmentsData>();
  private params: any;
  constructor(private api:APIService,private gb:Globals) {}
  getQueryParams(params){
      this.params=params;
  }
  saveAssignment(assignment:Assignment){
    return this.api.saveAssignment(assignment);
  }
  removeAssignment(assignmentId:number){
    this.api.removeAssignment(assignmentId).subscribe(res=>{
      console.log('saved');
    });
  }
  activateAssignment(assignment:Assignment){
    this.api.activateAssignment(assignment).subscribe(res=>{
      console.log('activated');
    });
  }
  getAsData():Observable<AssignmentsData>{
    return this.asData.asObservable();
  }
}
