import {Component, Inject, OnInit} from '@angular/core';
import {FileUploader} from "ng2-file-upload";
import {Globals} from "../../../globals";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {AssignmentsComponent} from "../assignments.component";
import {NotificationService} from "../../../notification.service";
import {TranslateService} from "@ngx-translate/core";


@Component({
  selector: 'app-upload-submission',
  templateUrl: './upload-submission.component.html',
  styleUrls: ['./upload-submission.component.scss']
})
export class UploadSubmissionComponent implements OnInit {
   uploader: FileUploader ;
   uploadData: any;
  constructor(private ns: NotificationService, private gb:Globals,private translate: TranslateService,private dialogRef: MatDialogRef<AssignmentsComponent>, @Inject(MAT_DIALOG_DATA) data) {
    this.uploadData=data;
    const user = JSON.parse(sessionStorage.getItem('currentUser'));
    this.uploader=new FileUploader({
      url: this.gb.serverUrl+'secure/submissions/upload',
      headers:[{'name':'token','value':user.token}],
      itemAlias: 'subFile',
      additionalParameter:{'subjectId':this.uploadData.subjectId,'assignmentId':this.uploadData.assignmentId,'userId':this.uploadData.userId}
    });
  }
  ngOnInit() {
    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
    };
    this.uploader.onCompleteItem = (item: any, status: any) => {
      let title='';
      let message='';
      if(item.isSuccess && item.isUploaded){
        this.ns.success(true,'upload_success','success');
        this.dialogRef.close({'status':true});
      }else {
        this.ns.error(true,'upload_error','error');
        this.dialogRef.close({'status':false});
      }
    };
  }
  close() {
    this.dialogRef.close();
  }
}
