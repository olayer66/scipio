import { Component, OnInit} from '@angular/core';
import {SecureService} from "../secure.service";
import {Subscription} from "rxjs";
import {AssignmentsService} from "./assignments.service";
import {APIService} from "../api.service";
import {ActivatedRoute} from "@angular/router";
import {Globals} from "../../globals";
import {DomSanitizer} from "@angular/platform-browser";
import {MatIconRegistry,MatDialog, MatDialogConfig} from "@angular/material";
import {UpgradeAssignmentComponent} from "./upgrade-assignment/upgrade-assignment.component";
import {Assignment} from "../interfaces.model";
import {UploadSubmissionComponent} from "./upload-submission/upload-submission.component";
@Component({
    selector: 'app-assignments',
    templateUrl: './assignments.component.html',
    styleUrls: ['./assignments.component.scss'],
    providers: [SecureService]
})
export class AssignmentsComponent implements OnInit{
     asDataSubscription:Subscription;
     asData:any ={};
     params: any;
     selectedTab = -1;
     userType: string;
    constructor(private ass:AssignmentsService,private route: ActivatedRoute, private api: APIService, private gb:Globals,iconRegistry: MatIconRegistry, sanitizer: DomSanitizer,private dialog: MatDialog) {
        iconRegistry.addSvgIcon('swap-horizontal-bold', sanitizer.bypassSecurityTrustResourceUrl('../../assets/images/svg/swap-horizontal-bold.svg'));
        iconRegistry.addSvgIcon('swap-horizontal-bold-react', sanitizer.bypassSecurityTrustResourceUrl('../../assets/images/svg/swap-horizontal-bold-react.svg'));
        iconRegistry.addSvgIcon('swap-horizontal-bold-forbi', sanitizer.bypassSecurityTrustResourceUrl('../../assets/images/svg/swap-horizontal-bold-forbi.svg'));
        iconRegistry.addSvgIcon('file-document-box-plus-outline', sanitizer.bypassSecurityTrustResourceUrl('../../assets/images/svg/file-document-box-plus-outline.svg'));
        iconRegistry.addSvgIcon('file-document-box-outline', sanitizer.bypassSecurityTrustResourceUrl('../../assets/images/svg/file-document-box-outline.svg'));
        iconRegistry.addSvgIcon('pencil-outline', sanitizer.bypassSecurityTrustResourceUrl('../../assets/images/svg/pencil-outline.svg'));
        iconRegistry.addSvgIcon('plus', sanitizer.bypassSecurityTrustResourceUrl('../../assets/images/svg/plus.svg'));
        iconRegistry.addSvgIcon('bell-ring', sanitizer.bypassSecurityTrustResourceUrl('../../assets/images/svg/bell-ring.svg'));
        this.asDataSubscription= this.ass.getAsData().subscribe(asData=>{
            this.asData=asData;
        });
        this.route.queryParams.subscribe(params => {
            this.params=params;
            this.ass.getQueryParams(params);
        });
    }
    ngOnInit() {
        this.userType=this.gb.userTypes[this.params.user_type];
        this.api.getAssignments({userId:this.params.user_id,userType:this.userType}).subscribe(res=>{
            this.asData=res;
        });
    }

    openUpgradeAssignment(subjectId,pos?:number) {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.autoFocus = true;
        let isNew=false;
        if(pos>=0)
            dialogConfig.data = this.asData.assignments[subjectId][pos];
        else {
            isNew=true;
            dialogConfig.data = new Assignment();
            dialogConfig.data.subject_id=subjectId;
        }
        const dialogRef = this.dialog.open(UpgradeAssignmentComponent, dialogConfig);
        dialogRef.afterClosed().subscribe(data => {
                if(data){
                    if(data.action==='REMOVE') {
                        this.ass.removeAssignment(data.assignment.assignment_id);
                        this.asData.assignments[subjectId][pos].active=false;
                    }else {
                        this.ass.saveAssignment(data.assignment).subscribe(resp=>{
                            data.assignment.assignment_id=resp.assignment_id;
                            if (isNew)
                                this.asData.assignments[subjectId].push(data.assignment);

                        });
                    }
                }
            }
        );
    }
    openUploadSubmission(subjectId,assignmentId,pos) {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.autoFocus = true;
        dialogConfig.data={'subjectId':subjectId, 'assignmentId':assignmentId,'userId':this.params.user_id};
        const dialogRefUpload = this.dialog.open(UploadSubmissionComponent, dialogConfig);
        dialogRefUpload.afterClosed().subscribe(data => {
                if(data.status)
                    this.asData.assignments[subjectId][pos].have_submission=true;
            }
        );
    }
    activateAssignment(subjectId,pos?:number){
        this.ass.activateAssignment(this.asData.assignments[subjectId][pos]);
        this.asData.assignments[subjectId][pos].active=true;
    }
    activatePeerReview(subjectId,assignmentId,pos){
        this.api.activatePeerReview(assignmentId).subscribe(resp=>{
            this.asData.assignments[subjectId][pos].peer_review=1;
        });
    }
    changeStatePeerReview(subjectId,assignmentId,status,pos){
        this.api.changeStatePeerReview(assignmentId,status).subscribe(()=>{
            this.asData.assignments[subjectId][pos].peer_review=status;
        });
    }
}
