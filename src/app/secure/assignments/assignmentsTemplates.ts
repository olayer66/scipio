import {Assignment, Subject} from "../interfaces.model";

export class AssignmentsData{
    subjects:Subject[];
    assignments: Assignment[];
}
export class AssignmentsBody{
    userId:number;
    userType:string;
}
export class ActivatePeerReviewBody {
    assignmentId:number;

}
