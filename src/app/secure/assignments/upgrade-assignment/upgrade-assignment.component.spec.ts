import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpgradeAssignmentComponent } from './upgrade-assignment.component';

describe('UpgradeAssignmentComponent', () => {
  let component: UpgradeAssignmentComponent;
  let fixture: ComponentFixture<UpgradeAssignmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpgradeAssignmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpgradeAssignmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
