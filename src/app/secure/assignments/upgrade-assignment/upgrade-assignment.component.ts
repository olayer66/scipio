import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatIconRegistry} from "@angular/material";
import {FormBuilder, FormControl, FormGroup,Validators} from "@angular/forms";
import {AssignmentsComponent} from "../assignments.component";
import {Assignment} from "../../interfaces.model";
import {Globals} from "../../../globals";
import {DomSanitizer} from "@angular/platform-browser";
@Component({
  selector: 'app-upgrade-assignment',
  templateUrl: './upgrade-assignment.component.html',
  styleUrls: ['./upgrade-assignment.component.scss']
})
export class UpgradeAssignmentComponent implements OnInit {
   form: FormGroup;
   assignment: Assignment;
   today:any;
   isNew:boolean;
  constructor( private fb: FormBuilder,iconRegistry: MatIconRegistry, sanitizer: DomSanitizer,private dialogRef: MatDialogRef<AssignmentsComponent>, @Inject(MAT_DIALOG_DATA) data) {
    iconRegistry.addSvgIcon('calendar-month-outline', sanitizer.bypassSecurityTrustResourceUrl('../../assets/images/svg/calendar-month-outline.svg'));
    this.assignment=data;
    this.today= new Date();
    if (!this.assignment.assignment_id) {
      this.isNew = true;
      this.assignment.end_date=this.today;
    }else
      this.isNew =false;
  }
  ngOnInit() {
    this.form = this.fb.group({
      name:new FormControl(this.assignment.name,[Validators.required,Validators.maxLength(10)]),
      description:new FormControl(this.assignment.description,[Validators.required]),
      end_date:new FormControl(new Date(this.assignment.end_date),[Validators.required])
    });
  }
  close() {
    this.dialogRef.close();
  }
  save() {
    console.log(this.form.value);
    function twoDigits(d: number) {
      if(0 <= d && d < 10) return "0" + d.toString();
      if(-10 < d && d < 0) return "-0" + (-1*d).toString();
      return d.toString();
    }
    this.assignment.name=this.form.value.name;
    this.assignment.description=this.form.value.description;
    this.assignment.end_date = this.form.value.end_date.getFullYear() + "-" + twoDigits(1 + this.form.value.end_date.getMonth()) + "-" + twoDigits(this.form.value.end_date.getDate()) + " " + twoDigits(this.form.value.end_date.getHours()) + ":" + twoDigits(this.form.value.end_date.getMinutes()) + ":" + twoDigits(this.form.value.end_date.getSeconds());
    this.dialogRef.close({action:'UPDATE',assignment:this.assignment});
  }
  delete() {
    this.dialogRef.close({action:'REMOVE',assignment:this.assignment});
  }
}
