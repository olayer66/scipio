import {CommentedLine} from "../interfaces.model";

export class FileNode {
    children: FileNode[];
    filename: string;
    type: any;
    filePath: string;
}
export class FolderTreeBody {
    userId: number;
    userType: string;
    path: string;
}
export class FileData{
    name:string;
    content:string;
    type:string;
    comments:CommentedLine[];
    correctionId:number;
}
export class FileBody {
    userId: number;
    userType: string;
    path: string;
}
export class FileCommentsBody{
    correctionId:number;
    fileName:string;

}
export class SaveFileCommentsBody{
    userId: number;
    userType: string;
    comments:any;
}
