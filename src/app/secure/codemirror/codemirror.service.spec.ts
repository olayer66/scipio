import { TestBed } from '@angular/core/testing';

import { CodemirrorService } from './codemirror.service';

describe('CodemirrorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CodemirrorService = TestBed.get(CodemirrorService);
    expect(service).toBeTruthy();
  });
});
