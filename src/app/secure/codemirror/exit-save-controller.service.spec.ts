import { TestBed } from '@angular/core/testing';

import { ExitSaveControllerService } from './exit-save-controller.service';

describe('ExitSaveControllerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExitSaveControllerService = TestBed.get(ExitSaveControllerService);
    expect(service).toBeTruthy();
  });
});
