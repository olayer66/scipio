import {Injectable} from '@angular/core';
import {Observable,Subject} from 'rxjs';
import {FileNode, FileData} from './codemirrorTemplates';
import {CommentedLine} from "../interfaces.model";
import  {APIService} from "../api.service";
import {Globals} from "../../globals";
@Injectable({
    providedIn: 'root'
})
export class CodemirrorService {
    private params:any;
    private cachedFiles: FileData[]=[];
    private dataBase: FileNode[];
    private fTypes = {};
    private cmUpdate = new Subject<FileData>();
    constructor (private api: APIService, private gb:Globals) {
        this.cmUpdate.asObservable();
        this.api.getFileTypes().subscribe((res)=>{
            for(let i=0;i<res.fileTypes.length;i++){
                this.fTypes[res.fileTypes[i].extension]=res.fileTypes[i].type;
            }
        });
    }
    removeCache(){
        this.cachedFiles=[];
    }
    getQueryParams(params){
        this.params=params;
        console.log(this.params)
    }
    getDataBase() {
        return this.dataBase;
    }
    updateCodeMirror(fileBody) {
        let path=fileBody.path.split("/");
        const fileName =path[path.length-1];
        let ext=fileName.split('.');
        ext=ext[ext.length-1];
        const element = this.cachedFiles.find(file=>file.name===fileName);
        if (element) {
            console.log('cacheado')
            this.cmUpdate.next(element);
        } else {
            console.log('nuevo')
            this.api.getFile(fileBody).subscribe((res)=>{
                let test:FileData={
                    name:'',
                    content:'',
                    type:'',
                    comments:[],
                    correctionId:0
                };
                test.name=fileName;
                test.content=res.content;
                test.type=this.fTypes[ext]?this.fTypes[ext]:'';
                this.api.getFileComments({'correctionId':this.params.correctionId,'fileName':fileName}).subscribe(res2=>{
                    test.comments=res2.comments;
                    test.correctionId=res2.correctionId;
                    this.cachedFiles.push(test);
                    this.cmUpdate.next(test);
                });
            });
        }
    }
    saveFileComments(fileName:string,commentedLines:CommentedLine[]):Observable<any>{
        const element = this.cachedFiles.find(file=>file.name===fileName);
        this.cachedFiles[this.cachedFiles.indexOf(element)].comments = commentedLines;
        return this.api.saveFileComments({'userId': this.params.userId, 'userType': this.gb.userTypes[this.params.user_type], 'comments': commentedLines});
    }
    /*--------------------------------------------OBSERVABLES--------------------------------------------------------*/
    getCodeMirrorUpdate(): Observable<FileData> {
        return this.cmUpdate.asObservable();
    }
}
