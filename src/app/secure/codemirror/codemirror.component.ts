import {Component, OnDestroy, OnInit, AfterViewInit, ViewChild, HostListener, ChangeDetectorRef} from '@angular/core';
import {Subscription,Observable, of as observableOf} from 'rxjs';
import {FormControl, FormGroup} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {Globals} from "../../globals";
import { Location } from '@angular/common';
/*----------------------------------------------------Mode Libs-------------------------------------------------------*/
//Main Langs
import 'codemirror/mode/jsx/jsx';//Java (react)
import 'codemirror/mode/clike/clike';//C,C++,C#,java,Scala,Kotlin y Ceylon
import 'codemirror/mode/coffeescript/coffeescript';
import 'codemirror/mode/python/python';
//Web dev
import 'codemirror/mode/javascript/javascript';
import 'codemirror/mode/htmlmixed/htmlmixed';
import 'codemirror/mode/htmlembedded/htmlembedded';//JSP, ASP.NET
import 'codemirror/mode/css/css';
import 'codemirror/mode/sass/sass';
import 'codemirror/mode/php/php';
import 'codemirror/mode/ruby/ruby';
import 'codemirror/mode/coffeescript/coffeescript';
import 'codemirror/mode/perl/perl';
//Old langs
import 'codemirror/mode/cobol/cobol';
import 'codemirror/mode/pascal/pascal';
import 'codemirror/mode/fortran/fortran';
//latex
import 'codemirror/mode/stex/stex';
//sql
import 'codemirror/mode/sql/sql';
//swift
import 'codemirror/mode/swift/swift';
//visual basic
import 'codemirror/mode/vb/vb';
import 'codemirror/mode/vbscript/vbscript';
/*--------------------------------------------------Mode Libs end-----------------------------------------------------*/
//Treemap
import {NestedTreeControl} from '@angular/cdk/tree';
import {MatTreeNestedDataSource} from '@angular/material/tree';
//Services
import {SecureService} from '../secure.service';
import {APIService} from '../api.service';
//classes
import {FileNode, FolderTreeBody} from './codemirrorTemplates';
import {CommentedLine} from "../interfaces.model";
// JQuery
import * as $ from 'jquery';
import {CodemirrorService} from "./codemirror.service";
import {TranslateService} from "@ngx-translate/core";
import {MediaMatcher} from "@angular/cdk/layout";
import {MatDialog, MatDialogConfig, MatIconRegistry} from "@angular/material";
import {DomSanitizer} from "@angular/platform-browser";
import {commentLineComponent} from "./comment-modal/comment-line.component";
import {NotificationService} from "../../notification.service";



@Component({
    selector: 'app-codemirror, ngbd-popover-basic, ngbd-popover-triggers',
    templateUrl: './codemirror.component.html',
    styleUrls: ['./codemirror.component.scss'],
    providers: [SecureService]
})
export class CodemirrorComponent implements OnInit, OnDestroy, AfterViewInit {
    @ViewChild('codeEditor',{static: false }) private codeEd;
     params: any;
     hcMessage: string;
     userType:string;

    @HostListener('window:beforeunload', ['$event'])
    beforeUnloadHandler(event) {
        if(this.hasChange) {
            let confirmMessage = 'You have unsaved data changes. Are you sure to close the page?';
            event.returnValue = confirmMessage;
            return confirmMessage;
        }
    }
     hasChange:boolean=false;
    //treemap
    nestedTreeControl: NestedTreeControl<FileNode>;
    nestedDataSource: MatTreeNestedDataSource<FileNode>;
    dataBase: any;
    dataBase$ = new Observable<any>();
    dataBaseSuscription: Subscription;
    treeReqBody: FolderTreeBody;

    //codemirror
    options: any = {lineNumbers: true, theme: '', readOnly: true, mode: ''};
     doc: any;
    content: string;
     cmUpdateSubscription: Subscription;
    form: FormGroup;
     selectedLines: any[];
     commentedLines: CommentedLine[];
     fileName:string;
    commentedLine: CommentedLine;
     commentedLineNumber: number;
     haveSelectedLines;
     poMessage: string;
     correctionId: number | number | any;
     smallScreenCm: boolean=false;
      mobileQuery: MediaQueryList;
      isEditable:boolean;
     readonly _mobileQueryListener: () => void;

    constructor(private cms: CodemirrorService,private location: Location,private ns: NotificationService, private api: APIService,private route: ActivatedRoute,private translate: TranslateService, private gb:Globals,changeDetectorRef: ChangeDetectorRef, media: MediaMatcher,iconRegistry: MatIconRegistry, sanitizer: DomSanitizer,private dialog: MatDialog) {
        iconRegistry.addSvgIcon('file-tree', sanitizer.bypassSecurityTrustResourceUrl('../../assets/images/svg/file-tree.svg'));
        iconRegistry.addSvgIcon('theme-light-dark', sanitizer.bypassSecurityTrustResourceUrl('../../assets/images/svg/theme-light-dark.svg'));
        iconRegistry.addSvgIcon('keyboard-return', sanitizer.bypassSecurityTrustResourceUrl('../../assets/images/svg/keyboard-return.svg'));
        this.smallScreenCm = window.innerWidth < gb.minWidth;

        this.mobileQuery = media.matchMedia('(max-width: 600px)');
        this._mobileQueryListener = () => changeDetectorRef.detectChanges();
        this.mobileQuery.addListener(this._mobileQueryListener);

        this.route.queryParams.subscribe(params => {
            this.params=params;
            this.isEditable = this.params.editable == 'true';
            this.cms.getQueryParams(params);
        });
        //treemap
        this.dataBaseSuscription = this.dataBase$.subscribe(db => this.dataBase =  db);
        this.dataBase = cms.getDataBase();
        this.nestedTreeControl = new NestedTreeControl<FileNode>(this._getChildren);
        this.nestedDataSource = new MatTreeNestedDataSource();
        this.nestedDataSource.data = this.dataBase;
        //codemirror
        this.options = {lineNumbers: true, theme: 'eclipse', readOnly: 'nocursor', mode: ''};
        this.cmUpdateSubscription = this.cms.getCodeMirrorUpdate().subscribe(data => {
            if(this.fileName && this.commentedLines)
                this.cms.saveFileComments(this.fileName, this.commentedLines).subscribe(() => {
                    this.updateCodemirror(data);
                });
            else
                this.updateCodemirror(data);
        });
        this.selectedLines = [];
        this.commentedLine = {
            file_name:'',
            line_start: 0,
            line_end: 0,
            correction_id:0,
            message: '',
            code_modified: false,
            original_code: '',
            modified_code: '',
            created_at: '',
            deleted:false
        };
        this.haveSelectedLines = false;
        this.commentedLines = [];
        this.commentedLineNumber = 0;
        this.poMessage = '';
    }
    public hasChanges():boolean{
        return this.hasChange;
    }
    public hasChangesMessage():string{
        return this.hcMessage;
    }
    ngOnInit() {
        this.userType=this.gb.userTypes[this.params.user_type];
        this.treeReqBody = {'userId': this.params.user_id, 'userType': this.gb.userTypes[this.params.user_type], 'path': this.params.path};
        this.api.getFolderTree(this.treeReqBody).subscribe(
            (resp) => {
                this.nestedDataSource.data=resp.children;
            },
            (error) => {
                this.ns.error(false,error.error.error,error.error.code);
                this.location.back();
            }
        );
        this.content = '';
        this.form = new FormGroup({
            thm: new FormControl('eclipse')
        });
    }
    ngOnDestroy() {
        this.cmUpdateSubscription.unsubscribe();
    }
    ngAfterViewInit() {
        this.translate.get('message_are_you_secure_save', {value: 'world'}).subscribe((res: string) => {
            this.hcMessage=res;
            //=> 'hello world'
        });
        const DELAY = 170;
        let clicks = 0;
        let timer = null;
        this.doc = this.codeEd.codeMirror.getDoc();
        this.createLineIds();
        const cmComponent: any = this;
        $(() => {
            let ctrlIsPressed = false;
            $(document).on('keydown',function(event) {
                if ( event.which === 16) {
                    ctrlIsPressed = true;
                }
            });
            $(document).on('keyup',function(event) {
                if (event.which === 16) {
                    ctrlIsPressed = false;
                }
            });
            $(document).on('click',function () {
                $('.selected-menu').hide(100);
                $('.haveComment-menu').hide(100);
                $('.popover').hide(100);
            });
            function addSelected (sLines) {
                sLines.forEach(id => {
                    $('#ln' + id).addClass('selected');
                });
            }
            function removeSelected (sLines) {
                sLines.forEach(function (id) {
                    $('#ln' + id).removeClass('selected');
                });
            }
            function isCommented(sLines, cLines) {
                let commented = false;
                let pos = 0;
                if (sLines.length > cLines.length) {
                    pos++; //  en clines la posición 0 no tiene sentido (No hay una linea 0)
                    while (pos < cLines.length && !commented) {
                        if (cLines[pos] &&  !cLines[pos].deleted && sLines.includes(pos)) { commented = true; }
                        pos++;
                    }
                } else {
                    while (pos < sLines.length && !commented) {
                        if (cLines[sLines[pos]] &&  !cLines[sLines[pos]].deleted) { commented = true; }
                        pos++;
                    }
                }
                return commented;
            }
            // Eventos de click sobre el numero de linea
            $('.CodeMirror-linenumber')
                .on('click', function () {
                    if(cmComponent.params.editable==='true') {
                        cmComponent.setCommentedLineNumber(0);
                        const clicked = this;
                        const cLines = cmComponent.getCommentedLines(); // Lineas con comentarios
                        let sLines = cmComponent.getSelectedLines(); // Lineas seleccionadas
                        clicks++;  // count clicks
                        $('.selected-menu').hide(100);
                        $('.haveComment-menu').hide(100);
                        $('.popover').hide(100);
                        $('#selShow').remove();
                        if (clicks === 1) {
                            timer = setTimeout(function () {
                                removeSelected(sLines);
                                if (ctrlIsPressed && sLines.length) {// Si tenemos lineas seleccionadas y shift seleccionado
                                    const selLine = Number($(clicked).data('lineNumber')); // Linea seleccionada
                                    // Si esta por encima de las seleccionadas
                                    if (selLine <= sLines[0]) {
                                        $('#' + selLine).addClass('selected');
                                        for (let i = selLine; i < sLines[0]; i++) {
                                            if (!sLines.includes(i)) {
                                                sLines.push(i);
                                            }
                                        }
                                    } else if (selLine >= sLines[sLines.length - 1]) {// Si esta por debajo de las seleccionadas
                                        for (let i = sLines[sLines.length - 1] + 1; i <= selLine; i++) {
                                            if (!sLines.includes(i)) {
                                                sLines.push(i);
                                            }
                                            $('#' + i).addClass('selected');
                                        }
                                    } else if (selLine > sLines[0] && selLine < sLines[sLines.length - 1]) {
                                        sLines.splice(sLines.indexOf(selLine) + 1);
                                    }
                                    // Comprobamos la existencia de lineas comentadas en la selección
                                    if (isCommented(sLines, cLines) === false) {// Mostramos y guardamos
                                        addSelected(sLines);
                                        $('#addCommentButton').prop('disabled', false);
                                        cmComponent.setSelectedLines(sLines);
                                    } else {// Si la selección incluye lineas comentadas se anula la selección
                                        removeSelected(sLines);
                                        sLines = [];
                                        cmComponent.setSelectedLines(sLines);
                                    }
                                } else {
                                    sLines = [];
                                    const sLine = Number($(clicked).data('lineNumber'));
                                    if (!cLines[sLine] || cLines[sLine].deleted) {
                                        sLines.push(sLine);
                                        addSelected(sLines);
                                        cmComponent.setSelectedLines(sLines);
                                        $('#addCommentButton').prop('disabled', false);
                                    } else {
                                        $('#addCommentButton').prop('disabled', true);
                                        cmComponent.setCommentedLineNumber(sLine);
                                        $('#showCommentButton').click();
                                    }
                                }
                                clicks = 0;  // after action performed, reset counter
                            }, DELAY);
                        } else {
                            clearTimeout(timer); // prevent single-click action
                            const sLine = Number($(clicked).data('lineNumber'));
                            if (sLines.includes(sLine)) {
                                $('#showCommentButton').click();
                            }
                            clicks = 0;  // after action performed, reset counter
                        }
                    }
                })
                .on('contextmenu', function (event) {
                    // Avoid the real one
                    event.preventDefault();
                    if(cmComponent.params.editable==='true') {
                        const that = $('.selected-menu');
                        const top = (Number(event.currentTarget.id.substr(2)) * 21) + 31;
                        let sLines = cmComponent.getSelectedLines(); // Lineas seleccionadas
                        const sLine = Number(event.currentTarget.id.substr(2));
                        // Hide popover
                        that.hide(100);
                        $('.popover').hide(100);
                        $('.haveComment-menu').hide(100);
                        $('#selShow').remove();
                        if (!sLines.length) {
                            sLines.push(sLine);
                            cmComponent.setSelectedLines(sLines);
                        } else if (sLines.length && !sLines.includes(sLine)) {
                            sLines = [];
                            sLines.push(sLine);
                            cmComponent.setSelectedLines(sLines);
                        }
                        for (let i = 0; i <= sLines.length; i++) {
                            $('#ln' + sLines[i]).addClass('selected');
                        }
                        // Show contextmenu
                        that.finish().toggle(100).// In the right position (the mouse)
                        css({
                            display: 'grid',
                            top: event.pageY - 48,
                            left: '20px'
                        });
                    }
                })
                .on('mousedown', function (e) {
                    // If the clicked element is not the menu
                    if ($(e.target).parents('.selected-menu')) {
                        // Hide it
                        $('.selected-menu').hide(100);
                        const sLines = cmComponent.getSelectedLines();
                        removeSelected(sLines);
                    }
                });
            $('.CodeMirror-linenumber.haveComment')
                .on('mouseenter', function(event) {
                    event.preventDefault();
                    const sLine = Number(event.currentTarget.id.substr(2));
                    const message = cmComponent.getpoMessage(sLine);
                    $('#pomessage').text(message);
                    const top = (Number(event.currentTarget.id.substr(2)) * 21) + 31;
                    // Hide context menu
                    const that = $('.popover');
                    const left=event.pageX - $('#codeEditor').offset().left;
                    that.hide(100);
                    $('.selected-menu').hide(100);
                    $('.haveComment-menu').hide(100);
                    $('#selShow').remove();
                    // Show contextmenu
                    that.finish().toggle(100).
                    // In the right position (the mouse)
                    css({
                        display: 'grid',
                        top: top + 'px',
                        left: left+'px'
                    });
                })
                .on('mouseleave', function() {
                    $('.popover').hide(100);
                })
                .on('contextmenu', function (event) {
                    // Avoid the real one
                    event.preventDefault();
                    let sLines = cmComponent.getSelectedLines();
                    removeSelected(sLines);
                    sLines = [];
                    const sLine = Number(event.currentTarget.id.substr(2));
                    sLines.push(sLine);
                    cmComponent.setSelectedLines(sLines);
                    cmComponent.setCommentedLineNumber(sLine);
                    const top = (Number(event.currentTarget.id.substr(2)) * 21) + 31;
                    // Hide popover
                    const that =  $('.haveComment-menu');
                    that.hide(100);
                    $('.popover').hide(100);
                    $('.selected-menu').hide(100);
                    $('#selShow').remove();
                    // Show contextmenu
                    that.finish().toggle(100).
                    // In the right position (the mouse)
                    css({
                        top: event.pageY-48,
                        left: '20px'
                    });
                })
                .on('mousedown', function (e) {
                    // If the clicked element is not the menu
                    if ($(e.target).parents('.selected-menu')) {
                        // Hide it
                        $('.selected-menu').hide(100);
                        const sLines = cmComponent.getSelectedLines();
                        removeSelected(sLines);
                    }
                });
            $('.haveLineComment').unbind('click')
                .on('click', function(event) {
                    event.preventDefault();
                    $('.popover').hide(100);
                    $('.selected-menu').hide(100);
                    const cLine = Number(event.currentTarget.id.substr(2));
                    const cLines = cmComponent.getCommentedLines();
                    const line = cLines[cLine];
                    const linesShowId = 'selShow' + line.line_start + 'to' + line.line_end;
                    const target = $('#' + linesShowId);
                    if (!target.length && !line.deleted) {
                        $('#lt' + line.line_end).after('<pre id="' + linesShowId + '" class="selShow">' + line.modified_code.replace(/</g,'&lt').replace(/>/g,'&gt')+ '</pre>');
                    } else {
                        target.remove();
                    }
                });
// If the menu element is clicked
            $('.haveComment-menu li').click(function(event) {
                // This is the triggered action name
                switch ($(this).attr('data-action')) {

                    // A case for each action. Your actions here
                    case 'show':
                        $('#showCommentButton').click();
                        break;
                    case 'delete':
                        cmComponent.deleteComment();
                        break;
                }

                // Hide it AFTER the action was triggered
                $('.haveComment-menu').hide(100);
            });
            $('.selected-menu li').click(function() {

                // This is the triggered action name
                switch ($(this).attr('data-action')) {
                    // A case for each action. Your actions here
                    case 'new':
                        $('#showCommentButton').click();
                        break;
                }

                // Hide it AFTER the action was triggered
                $('.haveComment-menu').hide(100);
            });
        });
    }
    getSelectedLines(): any {
        return this.selectedLines;
    }
    setSelectedLines(sLines: any) {
        sLines.sort(function(a, b) {return a - b; });
        this.selectedLines = sLines;
    }
    getCommentedLines(): any {
        return this.commentedLines;
    }
    setCommentedLineNumber(line: number) {
        this.commentedLineNumber = line;
    }
    getpoMessage(line: number) {
        return this.commentedLines[line].message;
    }
    updateCodemirror(data) {
        this.hasChange=false;
        this.correctionId=data.correctionId;
        this.fileName = data.name;
        this.content = data.content;
        this.options.mode = data.type;
        this.commentedLines = data.comments;
        this.createLineIds();
        this.ngAfterViewInit();
    }
    themeChange() {
        if(this.options.theme==='eclipse')
            this.options.theme='darcula';
        else if(this.options.theme==='darcula')
            this.options.theme='eclipse';
        this.createLineIds();
        this.ngAfterViewInit();
    }
    // Crea los id de las lineas al inicio
    createLineIds() {
        const commentedLines = this.commentedLines;
        $(function() {
            $('.CodeMirror-linenumber').each(function () {
                $(this).attr('id', 'ln' + $(this).text());
                $(this).data('lineNumber', $(this).text());
                const line = Number($(this).text());
                if (commentedLines[line] && !commentedLines[line].deleted) {
                    $(this).addClass('haveComment');
                }
            });
            let ln = 1;
            $('.CodeMirror-line').each(function () {
                $(this).attr('id', 'lt' + ln);
                if (commentedLines[ln] && !commentedLines[ln].deleted) {
                    $(this).addClass('haveLineComment');
                }
                $(this).data('lineNumber', ln);
                ln++;
            });
        });
    }
    /*-----------------------------------------------------tree map---------------------------------------------------*/
    hasNestedChild = (_: number, nodeData: FileNode) => nodeData.type==='directory';

    private _getChildren = (node: FileNode) => observableOf(node.children);

    openFile(file: string) {
        this.cms.updateCodeMirror({'userId': 3, 'userType': 'STUDENT', 'path': file});
        this.createLineIds();
        this.ngAfterViewInit();
    }
    /*------------------------------------------------------Modal-----------------------------------------------------*/
    async openCommentedLine() {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.autoFocus = true;
        dialogConfig.minHeight=window.innerHeight>500?'500px':window.innerHeight;
        dialogConfig.minWidth=window.innerWidth>700?'700px':window.innerWidth;
        dialogConfig.maxWidth=window.innerWidth>700?'700px':window.innerWidth;
        if (!this.commentedLines[this.commentedLineNumber]) {
            const start = this.selectedLines[0];
            const end = this.selectedLines[this.selectedLines.length - 1];
            const code: string = await this.doc.getRange({line: start - 1, ch: 0},
                {line: end - 1, ch: this.doc.getLine(end - 1).length });
            this.commentedLine = {file_name:this.fileName,line_start: start, line_end: end, message: '', code_modified: false, original_code: code, modified_code: code,deleted:false, created_at: '',correction_id:this.correctionId};
        } else
            this.commentedLine = this.commentedLines[this.commentedLineNumber];
        this.commentedLineNumber = 0;
        dialogConfig.data={'commentedLine':this.commentedLine,'mode':this.options.mode,'theme':this.options.theme,'editable':this.params.editable};
        const dialogRefUpload = this.dialog.open(commentLineComponent, dialogConfig);
        dialogRefUpload.afterClosed().subscribe(data => {
            if(data) {
                this.commentedLine=data.commentedLine;
                switch (data.action) {
                    case 'DELETE':
                        this.deleteComment();
                        break;
                    case 'UPDATE':
                        this.updateCommentedLines();
                        break;
                    default:
                        console.log('error');
                }
            }
        });
    }
    deleteComment() {
        this.hasChange=true;
        if (this.commentedLine.line_start === 0) {
            this.commentedLine = this.commentedLines[this.commentedLineNumber];
        }
        for (let line = this.commentedLine.line_start; line <= this.commentedLine.line_end; line++) {
            $(function() {
                $('#ln' + line).removeClass('haveComment');
                $('#lt' + line).removeClass('haveLineComment');
            });
            this.commentedLines[line].deleted = true;
        }
        this.selectedLines = [];
    }
    updateCommentedLines() {
        if(this.commentedLine.original_code!==this.commentedLine.modified_code)
            this.commentedLine.code_modified=true;
        for (let line = this.commentedLine.line_start; line <= this.commentedLine.line_end; line++) {
            $(function() {
                const lg = $('#ln' + line);
                if (!lg.hasClass('haveComment')) {
                    lg.removeClass('selected');
                    lg.addClass('haveComment');
                    $('#lt' + line).addClass('haveLineComment');
                }
            });
            this.commentedLines[line] = this.commentedLine;
        }
        this.selectedLines = [];
        this.hasChange=true;
        this.createLineIds();
        this.ngAfterViewInit();
    }
    saveBeforeExit(){
        return this.cms.saveFileComments(this.fileName, this.commentedLines);
    }
    goBack(){
        this.location.back();
    }
    removeCache(){
        this.cms.removeCache();
    }
}
