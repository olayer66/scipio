import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LineModComponent } from './comment-line.component';

describe('LineModComponent', () => {
  let component: LineModComponent;
  let fixture: ComponentFixture<LineModComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LineModComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LineModComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
