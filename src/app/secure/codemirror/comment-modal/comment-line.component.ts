import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {CommentedLine} from "../../interfaces.model";

/*----------------------------------------------------Mode Libs-------------------------------------------------------*/
//Main Langs
import 'codemirror/mode/jsx/jsx';//Java (react)
import 'codemirror/mode/clike/clike';//C,C++,C#,java,Scala,Kotlin y Ceylon
import 'codemirror/mode/coffeescript/coffeescript';
import 'codemirror/mode/python/python';
//Web dev
import 'codemirror/mode/javascript/javascript';
import 'codemirror/mode/htmlmixed/htmlmixed';
import 'codemirror/mode/htmlembedded/htmlembedded';//JSP, ASP.NET
import 'codemirror/mode/css/css';
import 'codemirror/mode/sass/sass';
import 'codemirror/mode/php/php';
import 'codemirror/mode/ruby/ruby';
import 'codemirror/mode/coffeescript/coffeescript';
import 'codemirror/mode/perl/perl';
//Old langs
import 'codemirror/mode/cobol/cobol';
import 'codemirror/mode/pascal/pascal';
import 'codemirror/mode/fortran/fortran';
//latex
import 'codemirror/mode/stex/stex';
//sql
import 'codemirror/mode/sql/sql';
//swift
import 'codemirror/mode/swift/swift';
//visual basic
import 'codemirror/mode/vb/vb';
import 'codemirror/mode/vbscript/vbscript';
import {CodemirrorComponent} from "../codemirror.component";
/*--------------------------------------------------Mode Libs end-----------------------------------------------------*/

@Component({
  selector: 'app-line-mod',
  templateUrl: './comment-modal.component.html',
  styleUrls: ['./comment-modal.component.scss']
})
export class commentLineComponent implements OnInit {
  @ViewChild('orCodeEditor',{static: false }) private ceOrCode;
  @ViewChild('modCodeEditor',{static: false }) private ceModCode;
   commentedLine: CommentedLine;
   hideDeleteButton:boolean=true;
   readonly cmConf: any;
   optionsOrCode: any = {lineNumbers: false, theme: '', readOnly: true, mode: '',viewportMargin:3};
   optionsModCode: any = {lineNumbers: false, theme: '', readOnly: false, mode: ''};
   saveCommentButton: boolean;
   editable: boolean;
  constructor(private dialogRef: MatDialogRef<CodemirrorComponent>, @Inject(MAT_DIALOG_DATA) data) {
    this.commentedLine=data.commentedLine;
    this.optionsModCode.mode=data.mode;
    this.optionsOrCode.mode=data.mode;
    this.optionsModCode.theme=data.theme;
    this.optionsOrCode.theme=data.theme;
    this.editable = data.editable === 'true';
    if(!this.editable)
      this.optionsModCode.readOnly=true;
    if(this.commentedLine.message!==''){
      this.hideDeleteButton=false;
      this.saveCommentButton=false;
    } else{
      this.hideDeleteButton=true;
      this.saveCommentButton=true;
    }
  }

  ngOnInit() {
  }

  close(action?:string) {
    if(action){
      switch (action) {
        case 'DELETE':
          this.dialogRef.close({'action':'DELETE','commentedLine':this.commentedLine});
          break;
        case 'UPDATE':
          this.dialogRef.close({'action':'UPDATE','commentedLine':this.commentedLine});
          break;
        default:
          console.log('error');
      }
    } else
      this.dialogRef.close();
  }

  validateCommentData() {
    this.saveCommentButton = this.commentedLine.message === '';
  }
}
