import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ExitSaveControllerService  implements CanActivate {

  constructor(private router: Router) { }
  canActivate(
      next: ActivatedRouteSnapshot,
      state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean | UrlTree {
    //if(this.authService.isLoggedIn)
      return true;
    //else
     // return this.router.parseUrl("/admin/login");
  }
}
